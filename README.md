Prerequisites:<br/>

Vulkan 1.2+<br/>
CMake 3.19+<br/>
C++20 compiler<br/>

Dependencies:<br/>

asio: 1.19.2<br/>
bullet: 3.17<br/>
concurrentqueue: 1.0.3<br/>
flac: 1.3.3<br/>
glfw: 3.3.4<br/>
glm: 0.9.9.8<br/>
libwebp: 1.2.0<br/>
magic_enum: 0.7.2<br/>
openal-soft: 1.21.1<br/>
rapidjson: 1.1.0<br/>
tetgen: 1.6<br/>
tinyobjloader: 1.0.7<br/>
tinyply: 2.3.2<br/>
xz: 5.2.5<br/>
