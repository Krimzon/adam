#pragma once

/// <summary> Internal network protocol struct to send user inputs </summary>
struct KeyAction
{
	int Key;
	int Mode;
};

/// <summary> Internal network protocol struct to send user mouse inputs </summary>
struct MouseAction
{
	double X, Y;
};