#include "game_object.hpp"
#include "glm/gtc/type_ptr.hpp"

ContactResultCallback::ContactResultCallback()
{}

btScalar ContactResultCallback::addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1)
{
	auto Obj0Ptr = colObj0Wrap->getCollisionObject()->getUserPointer();
	auto Obj1Ptr = colObj1Wrap->getCollisionObject()->getUserPointer();
	if (Obj0Ptr == 0)
		return 0;
	if (Obj1Ptr == 0)
		return 0;
	auto GameObject0 = (GameObject *)Obj0Ptr;
	auto GameObject1 = (GameObject *)Obj1Ptr;
	if (GameObject0->Mass > 0 or GameObject0->Mass == -1)
		GameObject0->Collide(GameObject1->Tag);
	if (GameObject1->Mass > 0 or GameObject1->Mass == -1)
		GameObject1->Collide(GameObject0->Tag);
	return 0;
}

GameObject::GameObject()
{
	Handle = nullptr;
	Tag = GameObjectTag::NONE;
	RenderObject = true;
	RenderObjectID = 0;
	Transform = glm::mat4(1.0f);
	Mass = 0;
	Body = nullptr;
	DeleteFlag = false;
}

GameObject::~GameObject()
{}

void GameObject::Bind(Renderer *pHandle)
{
	Handle = pHandle;
}

std::vector<uint8_t> GameObject::ParseBase(const GameObject *Object)
{
	auto BaseData = GamePacket::Parse(std::tuple(Object->Tag, Object->RenderObject));
	if (Object->RenderObject)
	{
		std::array<float, 16> ArrTransform;
		std::copy((float *)(&(Object->Transform)), (float *)(&(Object->Transform)) + 16, ArrTransform.data());
		auto BaseDataRender = GamePacket::Parse(std::tuple(Object->RenderObjectID, ArrTransform));

		BaseData.insert(BaseData.end(), BaseDataRender.begin(), BaseDataRender.end());
	}

	return BaseData;
}

GameObject *GameObject::DecodeBase(std::vector<uint8_t>::iterator &Iter)
{
	GameObject *Instance = nullptr;

	BaseDataT BaseData = GamePacket::Decode<std::tuple<GameObjectTag, bool>>(Iter);
	BaseDataRenderT BaseDataRender;

	if (std::get<1>(BaseData))
	{
		BaseDataRender = GamePacket::Decode<std::tuple<uint16_t, std::array<float, 16>>>(Iter);
	}

	switch (std::get<0>(BaseData))
	{
		case GameObjectTag::BULLET:
			Instance = new GameObjectBullet();
			break;
		case GameObjectTag::ROOM:
			Instance = new GameObjectRoom();
			break;
		case GameObjectTag::PLAYER:
			Instance = new PlayerGameObject();
			break;

		default:
			throw std::runtime_error("Invalid entity");
	}

	if (std::get<1>(BaseData))
	{
		Instance->RenderObjectID = std::get<0>(BaseDataRender);
		Instance->Transform = glm::make_mat4(std::get<1>(BaseDataRender).data());
	}

	Instance->Tag = std::get<0>(BaseData);
	Instance->RenderObject = std::get<1>(BaseData);

	return Instance;
}

GameObject *GameObject::DecodeBase(const GameObjectTag &Tag, const rapidjson::Value &Json)
{
	GameObject *Instance = nullptr;
	switch (Tag)
	{
		case GameObjectTag::BULLET:
			Instance = new GameObjectBullet();
			Instance->DecodeDerived(Json);
			break;
		case GameObjectTag::ROOM:
			Instance = new GameObjectRoom();
			Instance->DecodeDerived(Json);
			break;
		case GameObjectTag::PLAYER:
			Instance = new PlayerGameObject();
			Instance->DecodeDerived(Json);
			break;
		default:
			throw std::runtime_error("Invalid entity");
	}

	if (Json.HasMember("RenderObject"))
	{
		Instance->RenderObjectID = Json.FindMember("RenderObject")->value.GetUint();
	}
	if (Json.HasMember("Transform"))
	{
		if (Json.FindMember("Transform")->value.GetObject().HasMember("Position"))
		{
			glm::vec3 Translate;
			Translate.x = Json.FindMember("Transform")->value.GetObject().FindMember("Position")->value.GetArray()[0].GetFloat();
			Translate.y = Json.FindMember("Transform")->value.GetObject().FindMember("Position")->value.GetArray()[1].GetFloat();
			Translate.z = Json.FindMember("Transform")->value.GetObject().FindMember("Position")->value.GetArray()[2].GetFloat();
			Instance->Transform = glm::translate(Instance->Transform, Translate);
		}
		if (Json.FindMember("Transform")->value.GetObject().HasMember("Rotation") and Json.FindMember("Transform")->value.GetObject().FindMember("Rotation")->value.GetArray()[0].IsArray())
		{
			for (auto &Entry : Json.FindMember("Transform")->value.GetObject().FindMember("Rotation")->value.GetArray())
			{
				glm::vec3 Axis = {Entry.GetArray()[0].GetFloat(), Entry.GetArray()[1].GetFloat(), Entry.GetArray()[2].GetFloat()};
				float Angle = Entry.GetArray()[3].GetFloat();

				Instance->Transform *= glm::rotate(glm::mat4(1.0f), glm::radians(Angle), Axis);
			}
		}
		else if (Json.FindMember("Transform")->value.GetObject().HasMember("Rotation"))
		{
			const auto &Entry = Json.FindMember("Transform")->value.GetObject().FindMember("Rotation")->value.GetArray();

			glm::vec3 Axis = {Entry[0].GetFloat(), Entry[1].GetFloat(), Entry[2].GetFloat()};
			float Angle = Entry[3].GetFloat();

			Instance->Transform *= glm::rotate(glm::mat4(1.0f), glm::radians(Angle), Axis);
		}
	}

	return Instance;
}

void GameObject::SetBody(btCollisionObject *body)
{
	Body = body;
}

btCollisionObject *GameObject::GetBody()
{
	return Body;
}

GameObjectBullet::GameObjectBullet()
{
	this->Tag = GameObjectTag::BULLET;
	Hitpoints = 1;
	Mass = 10;
}

GameObjectBullet::~GameObjectBullet()
{}

std::vector<uint8_t> GameObjectBullet::ParseDerived()
{
	return GamePacket::Parse(Hitpoints);
}

void GameObjectBullet::DecodeDerived(std::vector<uint8_t>::iterator &Iter)
{
	Hitpoints = GamePacket::Decode<uint8_t>(Iter);
}

void GameObjectBullet::DecodeDerived(const rapidjson::Value &Json)
{
	if (Json.HasMember("Hitpoints"))
	{
		Hitpoints = Json.FindMember("Hitpoints")->value.GetUint();
	}
}

void GameObjectBullet::Tick()
{}

void GameObjectBullet::Collide(GameObjectTag tag)
{
	DeleteFlag = true;
}

GameObjectRoom::GameObjectRoom()
{
	this->Tag = GameObjectTag::ROOM;
	Hitpoints = 1;
	Mass = 0;
}

GameObjectRoom::~GameObjectRoom()
{}

std::vector<uint8_t> GameObjectRoom::ParseDerived()
{
	return GamePacket::Parse(Hitpoints);
}

void GameObjectRoom::DecodeDerived(std::vector<uint8_t>::iterator &Iter)
{
	Hitpoints = GamePacket::Decode<uint8_t>(Iter);
}

void GameObjectRoom::DecodeDerived(const rapidjson::Value &Json)
{
	if (Json.HasMember("Hitpoints"))
	{
		Hitpoints = Json.FindMember("Hitpoints")->value.GetUint();
	}
}

void GameObjectRoom::Tick()
{}

void GameObjectRoom::Collide(GameObjectTag tag)
{}

PlayerGameObject::PlayerGameObject()
{
	this->Tag = GameObjectTag::PLAYER;
	Hitpoints = 100;
	Mass = -1;
}

PlayerGameObject::~PlayerGameObject()
{}

std::vector<uint8_t> PlayerGameObject::ParseDerived()
{
	return GamePacket::Parse(Hitpoints);
}

void PlayerGameObject::DecodeDerived(std::vector<uint8_t>::iterator &Iter)
{
	Hitpoints = GamePacket::Decode<uint8_t>(Iter);
}

void PlayerGameObject::DecodeDerived(const rapidjson::Value &Json)
{
	if (Json.HasMember("Hitpoints"))
	{
		Hitpoints = Json.FindMember("Hitpoints")->value.GetUint();
	}
}

void PlayerGameObject::Tick()
{}

void PlayerGameObject::Collide(GameObjectTag tag)
{
	switch (tag)
	{
		case GameObjectTag::BULLET:
		{
			btTransform trans;
			trans.setIdentity();
			trans.setOrigin({0, 0.5, 0});
			Body->setWorldTransform(trans);
			break;
		}
		default:
			break;
	}
}