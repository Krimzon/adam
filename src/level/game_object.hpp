#pragma once
#define BT_USE_DOUBLE_PRECISION

#include <iostream>
#include <vector>
#include "../render/renderer.hpp"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "rapidjson/document.h"

/// <summary> Struct used in CollisionTest for the callback of collision </summary>
struct ContactResultCallback : public btCollisionWorld::ContactResultCallback
{
	ContactResultCallback();
	/// <summary> Method used after collision </summary>
	btScalar addSingleResult(btManifoldPoint &cp, const btCollisionObjectWrapper *colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper *colObj1Wrap, int partId1, int index1);
};

enum class GameObjectTag : uint8_t
{
	NONE,
	BULLET,
	TRIGGER,
	ROOM,
	PLAYER
};

/// <summary> Class which must be implemented for all objects in the game </summary>
class GameObject
{
	Renderer *Handle;
	std::vector<GameObject *> Connectors; // currently not implemented

  public:
	using Serializable = std::true_type;
	using BaseDataT = std::tuple<GameObjectTag, bool>;
	using BaseDataRenderT = std::tuple<uint16_t, std::array<float, 16>>;

	/// <summary> Tag of type of object </summary>
	GameObjectTag Tag;
	/// <summary> Flag which triggers if the object is visible </summary>
	bool RenderObject;
	uint16_t RenderObjectID;
	glm::mat4 Transform;
	btCollisionObject *Body;
	/// <summary> Mass of object, if 0, object will be static </summary>
	float Mass;
	/// <summary> Flag used to mark objects to delete </summary>
	bool DeleteFlag;
	std::optional<int> Entity;

	GameObject();
	virtual ~GameObject();
	void Bind(Renderer *pHandle);

	/// <summary> Connects to physic engine object </summary>
	void SetBody(btCollisionObject *body);
	/// <summary> Returns a physic engine body </summary>
	btCollisionObject *GetBody();

	static std::vector<uint8_t> ParseBase(const GameObject *Object);
	static GameObject *DecodeBase(std::vector<uint8_t>::iterator &Iter);
	static GameObject *DecodeBase(const GameObjectTag &Tag, const rapidjson::Value &Json);

	virtual std::vector<uint8_t> ParseDerived() = 0;
	virtual void DecodeDerived(std::vector<uint8_t>::iterator &Iter) = 0;
	virtual void DecodeDerived(const rapidjson::Value &Json) = 0;

	/// <summary> Function used with every server tick </summary>
	virtual void Tick() = 0;
	/// <summary> Function used after collision </summary>
	/// <param name="tag"> the tag of the object which was collided with </param>
	virtual void Collide(GameObjectTag tag) = 0;
};

/// <summary> Implementation of the GameObject representing the bullet object </summary>
class GameObjectBullet : public GameObject
{
  private:
	uint8_t Hitpoints;

  public:
	GameObjectBullet();
	~GameObjectBullet();

	std::vector<uint8_t> ParseDerived();
	void DecodeDerived(std::vector<uint8_t>::iterator &Iter);
	void DecodeDerived(const rapidjson::Value &Json);

	void Tick();
	void Collide(GameObjectTag tag);
};

class GameObjectTrigger : public GameObject
{};

/// <summary> Implementation of the GameObject representing the static terrain object </summary>
class GameObjectRoom : public GameObject
{
  private:
	uint8_t Hitpoints;

  public:
	GameObjectRoom();
	~GameObjectRoom();

	std::vector<uint8_t> ParseDerived();
	void DecodeDerived(std::vector<uint8_t>::iterator &Iter);
	void DecodeDerived(const rapidjson::Value &Json);

	void Tick();
	void Collide(GameObjectTag tag);
};

/// <summary> Implementation of the GameObject representing the player </summary>
class PlayerGameObject : public GameObject
{
  private:
	uint8_t Hitpoints;

  public:
	PlayerGameObject();
	~PlayerGameObject();

	std::vector<uint8_t> ParseDerived();
	void DecodeDerived(std::vector<uint8_t>::iterator &Iter);
	void DecodeDerived(const rapidjson::Value &Json);

	void Tick();
	void Collide(GameObjectTag tag);
};
