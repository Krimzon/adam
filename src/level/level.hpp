#pragma once
#define BT_USE_DOUBLE_PRECISION

#include <map>
#include <string>
#include <type_traits>
#include "game_object.hpp"

class Level
{
	std::vector<GameObject *> Objects;

  public:
	using Convertible = std::true_type;
	using ParamT = std::vector<GameObject *>;

	Level();
	Level(const ParamT &Param);

	std::vector<GameObject *> &GetObjects();

	ParamT Convert() const;
};