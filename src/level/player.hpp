#pragma once

#include "../net/protocol.hpp"

struct Player
{
	ProtocolState ClientState;
	std::string Nick;
};