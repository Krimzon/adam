#include "level.hpp"

Level::Level()
{}

Level::Level(const Level::ParamT &Param)
{
	Objects = Param;
}

std::vector<GameObject *> &Level::GetObjects()
{
	return Objects;
}

Level::ParamT Level::Convert() const
{
	return Objects;
}