#pragma once

#include <fstream>
#include "../audio/sound_data.hpp"
#include "../audio/sound_loader.hpp"
#include "../audio/sound_object.hpp"
#include "file_adapter.hpp"
#include "flac/all.h"
#include "game_packet.hpp"
#include "FLAC++/all.h"

template <typename T>
concept FLACAdapterT = std::is_same<typename T::Convertible, std::true_type>::value and std::is_same<typename T::ParamT, std::vector<SoundData>>::value;

template <typename T>
struct FLACAdapter : Adapter<T>
{
	FLACAdapter();
	FLACAdapter(const T &Object);
	FLACAdapter(std::vector<uint8_t>::iterator &Iter);
};

template <FLACAdapterT T>
struct FLACFileAdapter : FileAdapter<T>
{
	T Load(const std::filesystem::path &Path);
};

template <FLACAdapterT T>
T FLACFileAdapter<T>::Load(const std::filesystem::path &Path)
{
	auto s = SoundObject();
	auto *l = new SoundLoader();
	l->LoadSound(&s, Path.string().c_str());
	return s;
}