#pragma once

#include <cstdint>
#include <string>

enum class ProtocolState
{
	DIE,
	HANDSHAKE,
	INIT,
	DOWNLOAD,
	GAME,
};

/// <summary> Internal network protocol struct with client informations </summary>
struct ClientInfo
{
	using Convertible = std::true_type;
	using ParamT = std::string;

	std::string Nick;

	ClientInfo();
	ClientInfo(const ParamT &Param);
	ParamT Convert() const;
};

enum class ClientRequest : uint8_t
{
	PING,
	CONNECT,
	DISCONNECT,
	LEVEL,
	READY,
};

/// <summary> Internal network protocol struct with server informations </summary>
struct ServerInfo
{
	using Convertible = std::true_type;
	using ParamT = std::tuple<std::string, int>;

	std::string Level;
	int PlayerID;

	ServerInfo();
	ServerInfo(const ParamT &Param);
	ParamT Convert() const;
};

enum class ServerResponse : uint8_t
{
	ACCEPT,
	DISCONNECT,
	REJECT,
};

enum class SoundType : uint8_t
{
	THROW,
};