#pragma once

#include <atomic>
#include <chrono>
#include <iostream>
#include <map>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include "asio/asio.hpp"
#include "concurrentqueue/concurrentqueue.h"
#include "game_packet.hpp"

/// <summary> Class for managing the connections, messages of server </summary>
class ServerNetManager
{
	std::string Address;
	std::string Port;
	asio::io_service io_service;
	asio::ip::tcp::acceptor *acceptor;

	bool End;
	bool Listening;

	int SocketCounter;

	moodycamel::ConcurrentQueue<std::pair<GamePacket *, std::vector<int>>> SendQ;
	moodycamel::ConcurrentQueue<std::pair<GamePacket *, int>> RecvQ;
	moodycamel::ConcurrentQueue<int> NewUsr;
	moodycamel::ConcurrentQueue<int> DelUsr;

	std::thread *TListen;
	std::thread *TSend;
	std::thread *TRun;

	std::map<int, asio::ip::tcp::socket *> Sockets;

	std::map<int, std::thread *> ReceiveThreadPool;
	std::map<int, std::atomic<bool>> ThreadPoolFlags;
	std::recursive_mutex ThreadPoolLock;

	void Listen();
	void Send();
	void Run();
	void Receive(int SocketNumber);
	void Close();

  public:
	ServerNetManager(std::string adress, std::string port);
	~ServerNetManager();

	/// <summary> Pushes the packet to send to users </summary>
	/// <param name="Ids"> vector of ids to which sent the packet to </param>
	void Push(GamePacket *Packet, std::vector<int> Ids);
	/// <param name="Ids"> End the connection with user and remove him from the queues </param>
	void DeleteUser(int User);
	/// <summary> Returns a vector of pairs of received packets and ids of user who send them </summary>
	std::vector<std::pair<GamePacket *, int>> Pull();
	/// <summary> Returns a vector of new connections </summary>
	std::vector<int> GetNewUsr();
	/// <summary> Returns a vector of ended connections </summary>
	std::vector<int> GetDelUsr();
};

/// <summary> Class for sending data to and receiving data from server </summary>
class ClientNetManager
{
	asio::io_service io_service;

	asio::ip::tcp::socket *Server;

	moodycamel::ConcurrentQueue<GamePacket *> SendQ;
	moodycamel::ConcurrentQueue<GamePacket *> RecvQ;

	std::thread *TRecv;
	std::thread *TSend;

	bool End;

	void Send();
	void Receive();
	void Close();

  public:
	ClientNetManager(std::string SAddress, std::string SPort);
	~ClientNetManager();

	/// <summary> Pushes the packet to the send queue </summary>
	void Push(GamePacket *Packet);
	/// <summary> Returns a vector of received packets </summary>
	std::vector<GamePacket *> Pull();
};