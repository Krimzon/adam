#pragma once

#define TINYOBJLOADER_IMPLEMENTATION

#include <fstream>
#include "../render/structures.hpp"
#include "../render/vertex.hpp"
#include "file_adapter.hpp"
#include "tinyobjloader/tiny_obj_loader.h"

template <typename T>
concept ObjAdapterT = std::is_same<typename T::Convertible, std::true_type>::value and std::is_same<typename T::ParamT, std::tuple<std::vector<Vertex>, std::vector<uint32_t>>>::value;

template <ObjAdapterT T>
struct ObjFileAdapter : FileAdapter<T>
{
	T Load(const std::filesystem::path &Path) override;

  private:
	std::tuple<std::vector<Vertex>, std::vector<uint32_t>> LoadObj(std::istream *obj_file);
};

template <ObjAdapterT T>
T ObjFileAdapter<T>::Load(const std::filesystem::path &Path)
{
	std::ifstream obj_stream(Path);

	auto obj_data = LoadObj(&obj_stream);
	auto vertices = std::get<0>(obj_data);
	auto indices = std::get<1>(obj_data);

	return T(std::tuple(vertices, indices));
}

/// <summary>
/// tiny obj implementation, parsing data from .obj files
/// </summary>
/// <param name="obj_file">file stream of desired .obj file</param>
/// <returns>tuple of indices and vertices, data for later use in model mesh</returns>
template <ObjAdapterT T>
std::tuple<std::vector<Vertex>, std::vector<uint32_t>> ObjFileAdapter<T>::LoadObj(std::istream *obj_file)
{
	tinyobj::attrib_t vertex_attributes;
	std::vector<Vertex> mesh_vertices;
	std::vector<uint32_t> mesh_indices;

	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	bool is_load_a_success = tinyobj::LoadObj(&vertex_attributes, &shapes, &materials, &warn, &err, obj_file);

	if (!is_load_a_success)
	{
		throw std::runtime_error(warn + err);
	}

	for (const auto &shape : shapes)
	{
		for (const auto &index : shape.mesh.indices)
		{
			Vertex vertex{};

			vertex.Position = {vertex_attributes.vertices[3 * index.vertex_index + 0], vertex_attributes.vertices[3 * index.vertex_index + 1], vertex_attributes.vertices[3 * index.vertex_index + 2]};

			if (index.texcoord_index >= 0)
			{
				vertex.TexCoord = {vertex_attributes.texcoords[2 * index.texcoord_index + 0], vertex_attributes.texcoords[2 * index.texcoord_index + 1]};
			}

			if (index.normal_index >= 0)
			{
				vertex.Normal = {vertex_attributes.normals[3 * index.normal_index + 0], vertex_attributes.normals[3 * index.normal_index + 1], vertex_attributes.normals[3 * index.normal_index + 2]};
			}

			// optional
			vertex.Color = {vertex_attributes.colors[3 * index.vertex_index + 0], vertex_attributes.colors[3 * index.vertex_index + 1], vertex_attributes.colors[3 * index.vertex_index + 2]};

			mesh_vertices.push_back(vertex);
			mesh_indices.push_back(mesh_indices.size());
		}
	}

	return std::tuple(mesh_vertices, mesh_indices);
}