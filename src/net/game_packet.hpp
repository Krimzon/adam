#pragma once

#include <any>
#include <array>
#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <vector>
#include "game_packet_enum.hpp"

template <typename T>
requires std::is_default_constructible<T>::value and std::is_copy_constructible<T>::value
struct Adapter
{
	using IAdapter = std::true_type;
	using ObjectT = T;

	T Object;
	std::vector<uint8_t> Data;
};

template <typename T>
concept Primitive = std::is_trivially_copyable<T>::value and not std::is_pointer<T>::value and not requires(T x)
{
	{ T::Convertible } -> std::same_as<std::true_type>;
};

template <typename T, typename B = typename std::remove_pointer<T>::type>
concept Pointer = std::is_pointer<T>::value and std::is_copy_constructible<B>::value;

template <typename T, typename B = typename std::remove_pointer<T>::type>
concept Abstract = std::is_pointer<T>::value and std::is_abstract<B>::value and std::is_same<typename B::Serializable, std::true_type>::value;

template <typename T>
concept Pair = requires(T x)
{
	typename T::first_type;
	typename T::second_type;
	{
		x.first
	}
	->std::same_as<typename T::first_type &>;
	{
		x.second
	}
	->std::same_as<typename T::second_type &>;
};

template <typename T>
concept DynamicIterable = requires(T x, T y)
{
	x.cbegin() != x.cend();
	x.size();
	y = T(x.begin(), x.end());
};

template <typename T>
concept Tuple = not Pair<T> and not std::is_trivially_copyable<T>::value and requires(T x)
{
	requires std::tuple_size<T>::value > 1;
};

template <typename T>
concept Convertible = std::is_same<typename T::Convertible, std::true_type>::value;

template <typename T>
concept IAdapter = std::is_same<typename T::IAdapter, std::true_type>::value;

template <typename T>
concept AnyInsertable = requires(T a, T b)
{
	a.insert(a.end(), b.begin(), b.end());
};

template <typename T>
concept FrontInsertable = requires(T a, T b)
{
	a.insert(b.begin(), b.end());
};

struct GamePacket
{
	std::vector<uint8_t> Data;
	std::map<GamePacketTag, std::any> Tags;

	GamePacket(size_t Size = 0);
	GamePacket(const std::vector<uint8_t> &Data);
	void Translate();

	template <typename T>
	void Insert(GamePacketTag Tag, const T &Object);

	template <Primitive T>
	static std::vector<uint8_t> Parse(const T &Object);
	template <Pointer T>
	static std::vector<uint8_t> Parse(const T &Object);
	template <Abstract T>
	static std::vector<uint8_t> Parse(const T &Object);
	template <Pair T, typename K = typename T::first_type, typename V = T::second_type>
	static std::vector<uint8_t> Parse(const T &Object);
	template <DynamicIterable T, typename I = typename T::value_type>
	static std::vector<uint8_t> Parse(const T &Object);
	template <Tuple T, size_t I = 0>
	static std::vector<uint8_t> Parse(const T &Object);
	template <Convertible T>
	static std::vector<uint8_t> Parse(const T &Object);
	template <IAdapter T>
	static std::vector<uint8_t> Parse(const T &Object);

	template <Primitive T>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <Pointer T>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <Abstract T>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <Pair T, typename K = typename std::remove_const<typename T::first_type>::type, typename V = std::remove_const<typename T::second_type>::type>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <AnyInsertable T, typename I = typename T::value_type>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <FrontInsertable T, typename I = typename T::value_type>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <Tuple T, size_t I = 0>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <Convertible T, typename I = typename T::ParamT>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
	template <IAdapter T>
	static T Decode(std::vector<uint8_t>::iterator &Iter);
};

template <typename T>
void GamePacket::Insert(GamePacketTag Tag, const T &Object)
{
	std::vector<uint8_t> ParsedTag = Parse(Tag);
	std::vector<uint8_t> ParsedObj = Parse(Object);

	Data.insert(Data.end(), ParsedTag.begin(), ParsedTag.end());
	Data.insert(Data.end(), ParsedObj.begin(), ParsedObj.end());
}

template <Primitive T>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	return std::vector<uint8_t>(reinterpret_cast<uint8_t *>(const_cast<T *>(&Object)), reinterpret_cast<uint8_t *>(const_cast<T *>(&Object)) + sizeof(T));
}

template <Pointer T>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	return Parse(*Object);
}

template <Abstract T>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	std::vector<uint8_t> Result;
	std::vector<uint8_t> Base = std::remove_pointer<T>::type::ParseBase(Object);
	std::vector<uint8_t> Derived = Object->ParseDerived();

	Result.insert(Result.end(), Base.begin(), Base.end());
	Result.insert(Result.end(), Derived.begin(), Derived.end());

	return Result;
}

template <Pair T, typename K, typename V>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	std::vector<uint8_t> Result;
	std::vector<uint8_t> Key = Parse(Object.first);
	std::vector<uint8_t> Val = Parse(Object.second);

	Result.insert(Result.end(), Key.begin(), Key.end());
	Result.insert(Result.end(), Val.begin(), Val.end());

	return Result;
}

template <DynamicIterable T, typename I>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	std::vector<uint8_t> Result;
	Result.reserve(Object.size() * sizeof(I) + sizeof(size_t));
	std::vector<uint8_t> Size = Parse(Object.size());
	Result.insert(Result.end(), Size.begin(), Size.end());

	for (const I &Inner : Object)
	{
		std::vector<uint8_t> Temp = Parse(Inner);
		Result.insert(Result.end(), Temp.begin(), Temp.end());
	}

	return Result;
}

template <Tuple T, size_t I>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	std::vector<uint8_t> Result;

	if constexpr (I + 1 != std::tuple_size<T>{})
	{
		Result = Parse<T, I + 1>(Object);
	}
	std::vector<uint8_t> Temp = Parse(std::get<I>(Object));
	Result.insert(Result.end(), Temp.begin(), Temp.end());

	return Result;
}

template <Convertible T>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	return Parse(Object.Convert());
}

template <IAdapter T>
std::vector<uint8_t> GamePacket::Parse(const T &Object)
{
	return Object.Data;
}

template <Primitive T>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	T Object = *reinterpret_cast<T *>(std::addressof(*Iter));
	Iter += sizeof(T);
	return Object;
}

template <Pointer T>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	return new std::remove_pointer<T>::type(Decode<typename std::remove_pointer<T>::type>(Iter));
}

template <Abstract T>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	T Object = std::remove_pointer<T>::type::DecodeBase(Iter);
	Object->DecodeDerived(Iter);
	return Object;
}

template <Pair T, typename K, typename V>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	K Key = Decode<K>(Iter);
	V Val = Decode<V>(Iter);
	return T(Key, Val);
}

template <AnyInsertable T, typename I>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	T Result;
	size_t Size = Decode<size_t>(Iter);

	for (size_t i = 0; i < Size; i++)
	{
		I Immediate = Decode<I>(Iter);
		Result.insert(Result.end(), &Immediate, &Immediate + 1);
	}

	return Result;
}

template <FrontInsertable T, typename I>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	T Result;
	size_t Size = Decode<size_t>(Iter);

	for (size_t i = 0; i < Size; i++)
	{
		I Immediate = Decode<I>(Iter);
		Result.insert(&Immediate, &Immediate + 1);
	}

	return Result;
}

template <Tuple T, size_t I>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	T Result;

	if constexpr (I + 1 != std::tuple_size<T>{})
	{
		Result = Decode<T, I + 1>(Iter);
	}
	std::get<I>(Result) = Decode<typename std::tuple_element<I, T>::type>(Iter);

	return Result;
}

template <Convertible T, typename I>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	return T(Decode<I>(Iter));
}

template <IAdapter T>
T GamePacket::Decode(std::vector<uint8_t>::iterator &Iter)
{
	return T(Iter);
}