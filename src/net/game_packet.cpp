#include "../audio/sound_object.hpp"
#include "../level/action.hpp"
#include "../level/level.hpp"
#include "../render/freecam.hpp"
#include "../render/shader.hpp"
#include "../render/structures.hpp"
#include "game_packet.hpp"
#include "lzma_adapter.hpp"
#include "protocol.hpp"
#include "webp_adapter.hpp"

GamePacket::GamePacket(size_t Size)
{
	Data.reserve(Size);
}

GamePacket::GamePacket(const std::vector<uint8_t> &Data) : Data(Data)
{
	Translate();
}

void GamePacket::Translate()
{
	for (auto Iter = Data.begin(); Iter != Data.end();)
	{
		GamePacketTag Tag = GamePacket::Decode<GamePacketTag>(Iter);

		switch (Tag)
		{
			case GamePacketTag::AUDIO_BULK:
				Tags[Tag] = Decode<std::map<std::string, SoundObject>>(Iter);
				break;
			case GamePacketTag::BUNDLE:
				Tags[Tag] = Decode<std::vector<uint8_t>>(Iter);
				break;
			case GamePacketTag::GAME_LEVEL:
				Tags[Tag] = Decode<LZMAAdapter<Level>>(Iter);
				break;
			case GamePacketTag::NET_CLIENT_KEY_ACTION:
				Tags[Tag] = Decode<KeyAction>(Iter);
				break;
			case GamePacketTag::NET_CLIENT_MOUSE_ACTION:
				Tags[Tag] = Decode<MouseAction>(Iter);
				break;
			case GamePacketTag::NET_CLIENT_INFO:
				Tags[Tag] = Decode<ClientInfo>(Iter);
				break;
			case GamePacketTag::NET_CLIENT_REQUEST:
				Tags[Tag] = Decode<ClientRequest>(Iter);
				break;
			case GamePacketTag::NET_SERVER_DELTA:
				Tags[Tag] = Decode<std::tuple<std::map<int, FreeCam>, std::map<int, std::vector<double>>>>(Iter);
				break;
			case GamePacketTag::NET_SERVER_INFO:
				Tags[Tag] = Decode<ServerInfo>(Iter);
				break;
			case GamePacketTag::NET_SERVER_RESPONSE:
				Tags[Tag] = Decode<ServerResponse>(Iter);
				break;
			case GamePacketTag::NET_SERVER_STATE:
				Tags[Tag] = Decode<std::tuple<Level, std::map<int, FreeCam>>>(Iter);
				break;
			case GamePacketTag::NET_SERVER_OBJECT_ADD:
				Tags[Tag] = Decode<std::tuple<int, std::vector<double>, int>>(Iter);
				break;
			case GamePacketTag::NET_SERVER_OBJECT_DEL:
				Tags[Tag] = Decode<int>(Iter);
				break;
			case GamePacketTag::NET_SERVER_SOUND:
				Tags[Tag] = Decode<SoundType>(Iter);
				break;
			case GamePacketTag::RENDER_LIGHT_TEMPLATE_BULK:
				Tags[Tag] = Decode<LZMAAdapter<std::vector<Light>>>(Iter);
				break;
			case GamePacketTag::RENDER_MODEL_TEMPLATE_BULK:
				Tags[Tag] = Decode<LZMAAdapter<std::vector<Model>>>(Iter);
				break;
			case GamePacketTag::RENDER_SHADER_BULK:
				Tags[Tag] = Decode<LZMAAdapter<std::multimap<std::string, Shader>>>(Iter);
				break;
			case GamePacketTag::RENDER_TARGET_TEMPLATE_BULK:
				Tags[Tag] = Decode<LZMAAdapter<std::vector<Target>>>(Iter);
				break;
			case GamePacketTag::RENDER_TEXTURE_TEMPLATE_BULK:
				Tags[Tag] = Decode<std::vector<WebPAdapter<Texture>>>(Iter);
				break;
		}
	}
}