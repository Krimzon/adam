#pragma once

#include <stdexcept>
#include "game_packet.hpp"
#include "lzma/lzma.h"

template <typename T>
struct LZMAAdapter : Adapter<T>
{
	LZMAAdapter();
	LZMAAdapter(const T &Object);
	LZMAAdapter(std::vector<uint8_t>::iterator &Iter);
};

template <typename T>
LZMAAdapter<T>::LZMAAdapter()
{}

template <typename T>
LZMAAdapter<T>::LZMAAdapter(const T &Object)
{
	std::vector<uint8_t> In = GamePacket::Parse(Object);
	std::vector<uint8_t> Out(lzma_stream_buffer_bound(In.size()));
	size_t Pos = 0;

	if (lzma_easy_buffer_encode(9 | LZMA_PRESET_EXTREME, LZMA_CHECK_NONE, nullptr, In.data(), In.size(), Out.data(), &Pos, Out.size()) != LZMA_OK)
	{
		throw std::runtime_error("Fatal error encoding LZMA stream");
	}

	this->Data = GamePacket::Parse(std::vector(Out.begin(), Out.begin() + Pos));
}

template <typename T>
LZMAAdapter<T>::LZMAAdapter(std::vector<uint8_t>::iterator &Iter)
{
	std::vector<uint8_t> In = GamePacket::Decode<std::vector<uint8_t>>(Iter);
	std::vector<uint8_t> Out(0x00ffffff);
	size_t InPos = 0;
	size_t OutPos = 0;
	size_t Max = 0xffffffff;

	if (lzma_stream_buffer_decode(&Max, 0, nullptr, In.data(), &InPos, In.size(), Out.data(), &OutPos, Out.size()) != LZMA_OK)
	{
		throw std::runtime_error("Fatal error decoding LZMA stream");
	}

	this->Data = std::vector<uint8_t>(Out.begin(), Out.begin() + OutPos);
	auto Iterator = this->Data.begin();
	this->Object = T(GamePacket::Decode<T>(Iterator));
}