#include "../auxiliary/logger.hpp"
#include "net_manager.hpp"


ServerNetManager::ServerNetManager(std::string address, std::string port) : Address(address), Port(port)
{
	End = false;
	Listening = false;
	SocketCounter = 0;
	TListen = new std::thread([this] { this->Listen(); });
	TSend = new std::thread([this] { this->Send(); });
	TRun = new std::thread([this] { this->Run(); });
	while (!Listening)
		;
}

ServerNetManager::~ServerNetManager()
{
	this->Close();
	delete acceptor;
	delete TListen;
	delete TRun;
	delete TSend;
}

void ServerNetManager::Listen()
{
	asio::ip::address address = asio::ip::address::from_string(Address);
	uint16_t port = std::stoi(Port);
	acceptor = new asio::ip::tcp::acceptor(io_service, asio::ip::tcp::endpoint(address, port));
	Listening = true;
	while (true)
	{
		if (End)
		{
			break;
		}
		auto socket = new asio::ip::tcp::socket(io_service);
		acceptor->accept(*socket);
		NewUsr.enqueue(SocketCounter);
		ThreadPoolLock.lock();
		int tmp = SocketCounter;
		Sockets.emplace(tmp, socket);
		ReceiveThreadPool.emplace(tmp, new std::thread([this, tmp] { this->Receive(tmp); }));
		ThreadPoolFlags.emplace(tmp, false);
		SocketCounter++;
		ThreadPoolLock.unlock();
	}
}

void ServerNetManager::Receive(int SocketNumber)
{
	asio::error_code ec;
	while (true)
	{
		if (ThreadPoolFlags[SocketNumber])
		{
			break;
		}
		std::size_t size = 0;
		auto sizeBuff = asio::buffer(&size, sizeof(std::size_t));
		asio::read(*Sockets[SocketNumber], sizeBuff, ec);
		if (ec == asio::error::eof)
		{
			ThreadPoolLock.lock();
			ThreadPoolFlags[SocketNumber] = true;
			ThreadPoolLock.unlock();
			break;
		}
		else if (ec)
		{
			Log(LogModule::SERVER_NET, LogSeverity::ERR, ec.message());
			break;
		}
		std::vector<uint8_t> in;
		in.resize(size);
		auto buff = asio::buffer(in, size);
		std::size_t read = 0;
		while (read != size)
		{
			read += asio::read(*Sockets[SocketNumber], buff + read, ec);
		}
		if (ec == asio::error::eof)
		{
			ThreadPoolLock.lock();
			ThreadPoolFlags[SocketNumber] = true;
			ThreadPoolLock.unlock();
			break;
		}
		else if (ec)
		{
			Log(LogModule::SERVER_NET, LogSeverity::ERR, ec.message());
			break;
		}
		RecvQ.enqueue({new GamePacket(std::move(in)), SocketNumber});
	}
}

void ServerNetManager::Run()
{
	using namespace std::chrono_literals;
	while (true)
	{
		if (End)
		{
			break;
		}
		std::this_thread::sleep_for(1ms);

		for (auto iter = ThreadPoolFlags.begin(); iter != ThreadPoolFlags.end();)
		{
			if (iter->second)
			{
				DelUsr.enqueue(iter->first);

				Sockets[iter->first]->shutdown(asio::ip::tcp::socket::shutdown_both);
				Sockets[iter->first]->close();
				ReceiveThreadPool[iter->first]->join();
				delete ReceiveThreadPool[iter->first];

				delete Sockets[iter->first];
				Sockets.erase(iter->first);
				ThreadPoolLock.lock();
				ReceiveThreadPool.erase(iter->first);
				iter = ThreadPoolFlags.erase(iter);
				ThreadPoolLock.unlock();

				continue;
			}
			else
				iter++;
		}
	}
}

void ServerNetManager::Send()
{
	using namespace std::chrono_literals;
	while (true)
	{
		std::pair<GamePacket *, std::vector<int>> recv;
		std::this_thread::sleep_for(1ms);
		if (End)
		{
			break;
		}
		if (SendQ.try_dequeue(recv))
		{
			if (recv.second.empty())
			{
				for (auto &[SocketID, Socket] : Sockets)
				{
					auto buff = asio::buffer(recv.first->Data);
					auto size = buff.size();
					auto sizeBuff = asio::buffer(&size, sizeof(std::size_t));
					Socket->send(sizeBuff);
					std::size_t sent = 0;
					while (sent != buff.size())
					{
						sent += Socket->write_some(buff + sent);
					}
				}
			}
			else
			{
				for (auto i : recv.second)
				{
					if (Sockets.find(i) != Sockets.end())
					{
						auto buff = asio::buffer(recv.first->Data);
						auto size = buff.size();
						auto sizeBuff = asio::buffer(&size, sizeof(std::size_t));
						Sockets[i]->send(sizeBuff);
						std::size_t sent = 0;
						while (sent != buff.size())
						{
							sent += Sockets[i]->write_some(buff + sent);
						}
					}
				}
			}
			delete recv.first;
		}
	}
}

void ServerNetManager::Push(GamePacket *Packet, std::vector<int> Ids)
{
	if (Ids.size()>0)
		SendQ.enqueue(std::pair(Packet, Ids));
}

std::vector<std::pair<GamePacket *, int>> ServerNetManager::Pull()
{
	std::vector<std::pair<GamePacket *, int>> BulkData(RecvQ.size_approx());
	size_t ActualSize = RecvQ.try_dequeue_bulk(BulkData.data(), BulkData.size());
	BulkData.resize(ActualSize);
	return BulkData;
}

std::vector<int> ServerNetManager::GetNewUsr()
{
	std::vector<int> BulkData(NewUsr.size_approx());
	size_t ActualSize = NewUsr.try_dequeue_bulk(BulkData.data(), BulkData.size());
	BulkData.resize(ActualSize);
	return BulkData;
}

std::vector<int> ServerNetManager::GetDelUsr()
{
	std::vector<int> BulkData(DelUsr.size_approx());
	size_t ActualSize = DelUsr.try_dequeue_bulk(BulkData.data(), BulkData.size());
	BulkData.resize(ActualSize);
	return BulkData;
}

void ServerNetManager::DeleteUser(int user)
{
	auto ThreadPoolFlag = ThreadPoolFlags.find(user);
	if (ThreadPoolFlag != ThreadPoolFlags.end())
	{
		ThreadPoolLock.lock();
		ThreadPoolFlag->second = true;
		ThreadPoolLock.unlock();
	}
}

void ServerNetManager::Close()
{
	while (SendQ.size_approx())
		;
	End = true;
	auto const address = asio::ip::make_address(Address);
	auto const port = std::atoi(Port.c_str());
	asio::error_code error;
	auto listen = asio::ip::tcp::socket(io_service);
	listen.connect(asio::ip::tcp::endpoint(address, port), error);
	TListen->join();
	listen.shutdown(asio::ip::tcp::socket::shutdown_both);
	listen.close();

	ThreadPoolLock.lock();
	for (auto iter = ThreadPoolFlags.begin(); iter != ThreadPoolFlags.end();)
	{
		iter->second = true;
		iter++;
	}
	ThreadPoolLock.unlock();
	acceptor->close();
	while (acceptor->is_open())
		;
	TRun->join();
	TSend->join();
}


ClientNetManager::ClientNetManager(std::string SAddress, std::string SPort)
{
	End = false;
	auto const address = asio::ip::make_address(SAddress);
	auto const port = std::atoi(SPort.c_str());
	asio::error_code error;
	Server = new asio::ip::tcp::socket(io_service);
	Server->connect(asio::ip::tcp::endpoint(address, port), error);
	if (error)
		throw asio::system_error(error);

	TRecv = new std::thread([this] { this->Receive(); });
	TSend = new std::thread([this] { this->Send(); });
}

ClientNetManager::~ClientNetManager()
{
	this->Close();
	delete Server;
	delete TRecv;
	delete TSend;
}

void ClientNetManager::Close()
{
	while (SendQ.size_approx())
		;
	End = true;
	asio::error_code x;
	Server->shutdown(asio::ip::tcp::socket::shutdown_both, x);
	Server->close();
	while (Server->is_open())
		;
	TSend->join();
	TRecv->join();
}

void ClientNetManager::Send()
{
	using namespace std::chrono_literals;
	GamePacket *Packet;

	while (true)
	{
		std::this_thread::sleep_for(1ms);

		if (SendQ.try_dequeue(Packet))
		{
			auto buff = asio::buffer(Packet->Data);
			auto size = buff.size();
			auto sizeBuff = asio::buffer(&size, sizeof(std::size_t));
			Server->send(sizeBuff);
			std::size_t sent = 0;
			while (sent != buff.size())
			{
				sent += Server->write_some(buff + sent);
			}
			delete Packet;
		}
		if (End)
			break;
	}
}

void ClientNetManager::Receive()
{
	asio::error_code ec;
	while (true)
	{
		if (End)
			break;
		std::size_t size = 0;
		auto sizeBuff = asio::buffer(&size, sizeof(std::size_t));
		asio::read(*Server, sizeBuff, ec);
		if (ec == asio::error::eof)
		{
			End = true;
			break;
		}
		else if (ec)
		{
			Log(LogModule::CLIENT_NET, LogSeverity::ERR, ec.message());
			break;
		}
		std::vector<uint8_t> in;
		in.resize(size);
		auto buff = asio::buffer(in, size);
		std::size_t read = 0;
		while (read != size)
		{
			read += asio::read(*Server, buff + read, ec);
		}
		if (ec == asio::error::eof)
		{
			End = true;
			break;
		}
		else if (ec)
		{
			Log(LogModule::CLIENT_NET, LogSeverity::ERR, ec.message());
			break;
		}
		RecvQ.enqueue(new GamePacket(in));
	}
}

void ClientNetManager::Push(GamePacket *Packet)
{
	SendQ.enqueue(Packet);
}

std::vector<GamePacket *> ClientNetManager::Pull()
{
	std::vector<GamePacket *> BulkData(RecvQ.size_approx());
	size_t ActualSize = RecvQ.try_dequeue_bulk(BulkData.data(), BulkData.size());
	BulkData.resize(ActualSize);
	return BulkData;
}