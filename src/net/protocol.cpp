#include "protocol.hpp"

ClientInfo::ClientInfo()
{}

ClientInfo::ClientInfo(const ClientInfo::ParamT &Param)
{
	Nick = Param;
}

ClientInfo::ParamT ClientInfo::Convert() const
{
	return Nick;
}

ServerInfo::ServerInfo()
{}

ServerInfo::ServerInfo(const ServerInfo::ParamT &Param)
{
	Level = std::get<0>(Param);
	PlayerID = std::get<1>(Param);
}

ServerInfo::ParamT ServerInfo::Convert() const
{
	return {Level, PlayerID};
}