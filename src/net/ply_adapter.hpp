#pragma once

#define TINYPLY_IMPLEMENTATION

#include <fstream>
#include "../render/structures.hpp"
#include "../render/vertex.hpp"
#include "file_adapter.hpp"
#include "tinyply/tinyply.h"

template <typename T>
concept PlyAdapterT = std::is_same<typename T::Convertible, std::true_type>::value and std::is_same<typename T::ParamT, std::tuple<std::vector<Vertex>, std::vector<uint32_t>>>::value;

template <PlyAdapterT T>
struct PlyFileAdapter : FileAdapter<T>
{
	T Load(const std::filesystem::path &filepath) override;

  private:
	inline std::vector<uint8_t> ReadFileBinary(const std::string &filepath);
	std::tuple<std::vector<Vertex>, std::vector<uint32_t>> ReadPly(const std::string &filepath);

	struct memory_buffer : public std::streambuf
	{
		char *p_start{nullptr};
		char *p_end{nullptr};
		size_t size;

		memory_buffer(char const *first_elem, size_t size) : p_start(const_cast<char *>(first_elem)), p_end(p_start + size), size(size)
		{
			setg(p_start, p_start, p_end);
		}

		pos_type seekoff(off_type off, std::ios_base::seekdir dir, std::ios_base::openmode which) override
		{
			if (dir == std::ios_base::cur)
				gbump(static_cast<int>(off));
			else
				setg(p_start, (dir == std::ios_base::beg ? p_start : p_end) + off, p_end);
			return gptr() - p_start;
		}

		pos_type seekpos(pos_type pos, std::ios_base::openmode which) override
		{
			return seekoff(pos, std::ios_base::beg, which);
		}
	};

	struct memory_stream : virtual memory_buffer, public std::istream
	{
		memory_stream(char const *first_elem, size_t size) : memory_buffer(first_elem, size), std::istream(static_cast<std::streambuf *>(this))
		{}
	};
};

template <PlyAdapterT T>
T PlyFileAdapter<T>::Load(const std::filesystem::path &filepath)
{

	auto obj_data = ReadPly(std::string(filepath.string()));
	auto vertices = std::get<0>(obj_data);
	auto indices = std::get<1>(obj_data);

	return T(std::tuple(vertices, indices));
}

template <PlyAdapterT T>
inline std::vector<uint8_t> PlyFileAdapter<T>::ReadFileBinary(const std::string &filepath)
{
	std::ifstream file(filepath, std::ios::binary);
	std::vector<uint8_t> fileBufferBytes;

	if (file.is_open())
	{
		file.seekg(0, std::ios::end);
		size_t sizeBytes = file.tellg();
		file.seekg(0, std::ios::beg);
		fileBufferBytes.resize(sizeBytes);
		if (file.read((char *)fileBufferBytes.data(), sizeBytes))
			return fileBufferBytes;
	}
	else
		throw std::runtime_error("could not open binary ifstream to path " + filepath);
	return fileBufferBytes;
}

/// <summary>
/// tinyply implementation, parsing data from .ply files
/// </summary>
/// <param name="filepath">path to the desired .ply file</param>
/// <returns>tuple of indices and vertices, data for later use in model mesh</returns>
template <PlyAdapterT T>
std::tuple<std::vector<Vertex>, std::vector<uint32_t>> PlyFileAdapter<T>::ReadPly(const std::string &filepath)
{
	std::unique_ptr<std::istream> file_stream;
	std::vector<uint8_t> byte_buffer;

	std::vector<Vertex> mesh_vertices;
	std::vector<uint32_t> mesh_indices;

	try
	{
		// preloading the file
		byte_buffer = ReadFileBinary(filepath);
		file_stream.reset(new memory_stream((char *)byte_buffer.data(), byte_buffer.size()));

		if (!file_stream || file_stream->fail())
			throw std::runtime_error("file_stream failed to open " + filepath);

		file_stream->seekg(0, std::ios::end);
		const float size_mb = file_stream->tellg() * float(1e-6);
		file_stream->seekg(0, std::ios::beg);

		tinyply::PlyFile file;
		file.parse_header(*file_stream);

		// std::cout << "\t[ply_header] Type: " << (file.is_binary_file() ? "binary" : "ascii") << std::endl;

		std::shared_ptr<tinyply::PlyData> vertices, normals, colors, texcoords, faces, tripstrip;
		std::vector<glm::vec3> verts2;

		// The header information can be used to programmatically extract properties on elements
		// known to exist in the header prior to reading the data.
		try
		{
			vertices = file.request_properties_from_element("vertex", {"x", "y", "z"});
		}
		catch (const std::exception &e)
		{
			std::cerr << "tinyply exception: " << e.what() << std::endl;
		}

		try
		{
			normals = file.request_properties_from_element("vertex", {"nx", "ny", "nz"});
		}
		catch (const std::exception &e)
		{
			std::cerr << "tinyply exception: " << e.what() << std::endl;
		}

		try
		{
			colors = file.request_properties_from_element("vertex", {"red", "green", "blue", "alpha"});
		}
		catch (const std::exception &e)
		{
			std::cerr << "tinyply exception: " << e.what() << std::endl;
		}

		// blender saves it as such, sometimes it could be a "u" and "v"
		try
		{
			texcoords = file.request_properties_from_element("vertex", {"s", "t"});
		}
		catch (const std::exception &e)
		{
			std::cerr << "tinyply exception: " << e.what() << std::endl;
		}

		// list size hint. If you have arbitrary ply files, it is best to leave this 0.
		try
		{
			faces = file.request_properties_from_element("face", {"vertex_indices"}, 0);
		}
		catch (const std::exception &e)
		{
			std::cerr << "tinyply exception: " << e.what() << std::endl;
		}

		file.read(*file_stream);

		/* if (vertices)
			std::cout << "\tRead " << vertices->count << " total vertices " << std::endl;
		if (normals)
			std::cout << "\tRead " << normals->count << " total vertex normals " << std::endl;
		if (colors)
			std::cout << "\tRead " << colors->count << " total vertex colors " << std::endl;
		if (texcoords)
			std::cout << "\tRead " << texcoords->count << " total vertex texcoords " << std::endl;
		if (faces)
			std::cout << "\tRead " << faces->count << " total faces (triangles) " << std::endl;
		*/

		if (vertices->t == tinyply::Type::FLOAT32)
		{ // as floats

			float *positions, *normal_array, *texcoord_array, *color_array;

			positions = new float[uint32_t(vertices->count) * 3];
			normal_array = new float[uint32_t(vertices->count) * 3];
			texcoord_array = new float[uint32_t(vertices->count) * 2];
			color_array = new float[uint32_t(vertices->count) * 4];

			// conversion
			std::memcpy(positions, vertices->buffer.get(), vertices->buffer.size_bytes());
			std::memcpy(normal_array, normals->buffer.get(), normals->buffer.size_bytes());
			std::memcpy(texcoord_array, texcoords->buffer.get(), texcoords->buffer.size_bytes());
			std::memcpy(color_array, colors->buffer.get(), colors->buffer.size_bytes());

			for (int i = 0; i < uint32_t(vertices->count); i++)
			{
				Vertex v;
				v.Position = {positions[i * 3 + 0], positions[i * 3 + 1], positions[i * 3 + 2]};
				if (normals)
				{
					v.Normal = {normal_array[i * 3 + 0], normal_array[i * 3 + 1], normal_array[i * 3 + 2]};
				}
				if (texcoords)
				{
					v.TexCoord = {texcoord_array[i * 2 + 0], texcoord_array[i * 2 + 1]};
				}
				if (colors)
				{
					// ignoring alpha
					v.Color = {color_array[i * 4 + 0], color_array[i * 4 + 1], color_array[i * 4 + 2]};
				}

				mesh_vertices.push_back(v);
				mesh_indices.push_back(mesh_indices.size());
			}

			delete[] positions, normal_array, color_array, texcoord_array;
		}
		else
		{
			fprintf(stderr, "Don't know how to convert to floats for vertices attribute");
			throw std::exception();
		}
	}
	catch (const std::exception &e)
	{
		std::cerr << "Caught tinyply exception: " << e.what() << std::endl;
	}

	return std::tuple(mesh_vertices, mesh_indices);
}