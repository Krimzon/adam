#pragma once

#include <fstream>
#include "file_adapter.hpp"
#include "game_packet.hpp"
#include "webp/decode.h"
#include "webp/encode.h"

template <typename T>
concept WebPAdapterT = std::is_same<typename T::Convertible, std::true_type>::value and std::is_same<typename T::ParamT, std::tuple<int32_t, int32_t, std::vector<uint8_t>>>::value;

template <WebPAdapterT T>
struct WebPAdapter : Adapter<T>
{
	WebPAdapter();
	WebPAdapter(const T &Object);
	WebPAdapter(std::vector<uint8_t>::iterator &Iter);

  private:
	WebPConfig *GetConfig();
	WebPPicture *GetPicture(int32_t Width, int32_t Height, uint8_t *Image);
};

template <WebPAdapterT T>
WebPAdapter<T>::WebPAdapter()
{}

template <WebPAdapterT T>
WebPAdapter<T>::WebPAdapter(const T &Object)
{
	auto Params = Object.Convert();

	WebPConfig *Config = GetConfig();
	WebPPicture *Picture = GetPicture(std::get<0>(Params), std::get<1>(Params), std::get<2>(Params).data());
	WebPMemoryWriter *Writer = new WebPMemoryWriter();
	WebPMemoryWriterInit(Writer);

	Picture->writer = WebPMemoryWrite;
	Picture->custom_ptr = Writer;

	if (!WebPEncode(Config, Picture))
		throw std::runtime_error("WebPAdapter: Could not encode image");

	this->Data = GamePacket::Parse(std::vector<uint8_t>(reinterpret_cast<uint8_t *>(Writer->mem), reinterpret_cast<uint8_t *>(Writer->mem) + Writer->size));
}

template <WebPAdapterT T>
WebPAdapter<T>::WebPAdapter(std::vector<uint8_t>::iterator &Iter)
{
	this->Data = GamePacket::Decode<std::vector<uint8_t>>(Iter);

	int32_t Width, Height;
	uint8_t *Result = WebPDecodeRGBA(this->Data.data(), this->Data.size(), &Width, &Height);

	this->Object = T({Width, Height, std::vector<uint8_t>(Result, Result + static_cast<size_t>(Width) * static_cast<size_t>(Height) * 4)});
}

template <WebPAdapterT T>
WebPConfig *WebPAdapter<T>::GetConfig()
{
	WebPConfig *Config = new WebPConfig();
	WebPConfigInit(Config);

	Config->lossless = true;
	Config->exact = true;
	Config->method = 6;
	Config->quality = 100;

	return Config;
}

template <WebPAdapterT T>
WebPPicture *WebPAdapter<T>::GetPicture(int32_t Width, int32_t Height, uint8_t *Image)
{
	WebPPicture *Picture = new WebPPicture();
	WebPPictureInit(Picture);

	Picture->use_argb = true;
	Picture->width = Width;
	Picture->height = Height;

	WebPPictureImportRGBA(Picture, Image, Width * 4);
	return Picture;
}

template <WebPAdapterT T>
struct WebPFileAdapter : FileAdapter<T>
{
	T Load(const std::filesystem::path &Path);
};

template <WebPAdapterT T>
T WebPFileAdapter<T>::Load(const std::filesystem::path &Path)
{
	std::fstream File(Path.string(), std::ios::in | std::ios::binary);
	size_t FileSize = std::filesystem::file_size(Path);

	uint8_t *Buff = new uint8_t[FileSize];
	File.read(reinterpret_cast<char *>(Buff), FileSize);

	int32_t Width, Height;
	uint8_t *Result = WebPDecodeRGBA(Buff, FileSize, &Width, &Height);

	return T({Width, Height, std::vector<uint8_t>(Result, Result + static_cast<size_t>(Width) * static_cast<size_t>(Height) * 4)});
}