#pragma once

#include <filesystem>

/// <summary>
/// Generic one-way file adapter interface. Intended for filesystem reading only.
/// </summary>
/// <typeparam name="T">Any type that will satisfy internal constraints of derived class</typeparam>
template <typename T>
struct FileAdapter
{
	virtual T Load(const std::filesystem::path &Path) = 0;
};