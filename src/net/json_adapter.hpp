#pragma once

#include <fstream>
#include "file_adapter.hpp"
#include "rapidjson/document.h"

template <typename T>
concept JsonAdapterT = std::is_same<typename T::JsonParsable, std::true_type>::value;

template <JsonAdapterT T>
struct JsonFileAdapter : FileAdapter<T>
{
	T Load(const std::filesystem::path &Path);
};

template <JsonAdapterT T>
T JsonFileAdapter<T>::Load(const std::filesystem::path &Path)
{
	std::fstream File(Path.string(), std::ios::in | std::ios::binary);
	size_t FileSize = std::filesystem::file_size(Path);

	std::string Json(FileSize, ' ');
	File.read(Json.data(), Json.size());

	rapidjson::Document Document;
	Document.Parse(Json.data(), Json.size());

	return T(Document);
}