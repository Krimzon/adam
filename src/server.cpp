#include <chrono>
#include <cstdint>
#include <filesystem>
#include <format>
#include <fstream>
#include <map>
#include "auxiliary/character_controller.hpp"
#include "auxiliary/convex_conversion.hpp"
#include "auxiliary/logger.hpp"
#include "auxiliary/physics_helper.hpp"
#include "glfw/glfw3.h"
#include "glm/gtc/type_ptr.hpp"
#include "level/action.hpp"
#include "level/level.hpp"
#include "level/player.hpp"
#include "net/lzma_adapter.hpp"
#include "net/net_manager.hpp"
#include "net/protocol.hpp"
#include "render/freecam.hpp"

void ProcessKeyAction(KeyAction &Action, FreeCam &Camera, CharacterController *Controller)
{
	if (Action.Key == GLFW_KEY_W and (Action.Mode == GLFW_PRESS or Action.Mode == GLFW_REPEAT))
	{
		auto rot = Camera.GetRot();
		Controller->setMovementDirection({rot.x, 0, rot.z});
	}

	if (Action.Key == GLFW_KEY_S and (Action.Mode == GLFW_PRESS or Action.Mode == GLFW_REPEAT))
	{
		auto rot = Camera.GetRot();
		Controller->setMovementDirection({-rot.x, 0, -rot.z});
	}

	if (Action.Key == GLFW_KEY_A and (Action.Mode == GLFW_PRESS or Action.Mode == GLFW_REPEAT))
	{
		auto rot = Camera.GetRot();
		rot = glm::rotate(rot, glm::radians(90.f), glm::vec3(0, 1, 0));

		Controller->setMovementDirection({rot.x, 0, rot.z});
	}

	if (Action.Key == GLFW_KEY_D and (Action.Mode == GLFW_PRESS or Action.Mode == GLFW_REPEAT))
	{
		auto rot = Camera.GetRot();
		rot = glm::rotate(rot, glm::radians(90.f), glm::vec3(0, 1, 0));

		Controller->setMovementDirection({-rot.x, 0, -rot.z});
	}

	if (Action.Mode == GLFW_RELEASE and (Action.Key == GLFW_KEY_W or Action.Key == GLFW_KEY_S or Action.Key == GLFW_KEY_A or Action.Key == GLFW_KEY_D))
		Controller->setMovementDirection({0, 0, 0});

	if (Action.Key == GLFW_KEY_SPACE and (Action.Mode == GLFW_PRESS))
	{
		if (Controller->canJump())
			Controller->jump();
	}
}

void ProcessMouseAction(MouseAction &Action, FreeCam &Camera)
{
	Camera.Rotate(glm::vec2(Action.Y * 0.01, -Action.X * 0.01));
}

int main()
{
	const short int StaticObjectGroup = 0b1000000;
	const short int DynamicObjectGroup = 0b10000000;

	std::fstream Autoexec(std::string(DATA) + "/autoexec.cfg", std::ios::in);
	std::map<std::string, std::string> Params;
	std::string Variable, Value;

	while (Autoexec >> Variable and Autoexec >> Value)
	{
		std::transform(Variable.begin(), Variable.end(), Variable.begin(), ::toupper);
		Params[Variable] = Value;
	}

	std::string IP = Params.contains("IP") ? Params["IP"] : "0.0.0.0";
	std::string Port = Params.contains("PORT") ? Params["PORT"] : "8303";
	std::string Map = Params.contains("MAP") ? Params["MAP"] : "level";
	uint8_t MaxPlayers = Params.contains("MAXPLAYERS") ? std::stoull(Params["MAXPLAYERS"]) : 16;
	uint64_t TickTime = Params.contains("TICKTIME") ? std::stoll(Params["TICKTIME"]) : 15625000;

	std::fstream LevelFile(std::format("{}/{}.bin", std::string(DATA), Map), std::ios::in | std::ios::binary);
	std::vector<uint8_t> LevelData(std::filesystem::file_size(std::format("{}/{}.bin", std::string(DATA), Map)));
	LevelFile.read(reinterpret_cast<char *>(LevelData.data()), LevelData.size());

	auto LevelPacket = GamePacket(LevelData);
	auto &&Models = std::any_cast<LZMAAdapter<std::vector<Model>> &>(LevelPacket.Tags[GamePacketTag::RENDER_MODEL_TEMPLATE_BULK]).Object;
	auto &&Targets = std::any_cast<LZMAAdapter<std::vector<Target>> &>(LevelPacket.Tags[GamePacketTag::RENDER_TARGET_TEMPLATE_BULK]).Object;
	auto &&Instance = std::any_cast<LZMAAdapter<Level> &>(LevelPacket.Tags[GamePacketTag::GAME_LEVEL]).Object;
	auto &&Entities = Instance.GetObjects();
	std::map<int, btCollisionShape *> CollisionShapes;
	const auto PlayerModel = 0;
	const auto SpawnObjectModel = 1;

	auto convModelPlayer = ConvDec(Models[PlayerModel]);
	btCompoundShape *collisionModelPlayer = new btCompoundShape();
	for (auto &tetra : convModelPlayer)
	{
		btBU_Simplex1to4 *tetraShape = new btBU_Simplex1to4(std2bt(tetra[0]), std2bt(tetra[1]), std2bt(tetra[2]), std2bt(tetra[3]));
		btTransform trans;
		trans.setIdentity();
		trans.setOrigin({0, 0, 0});
		collisionModelPlayer->addChildShape(trans, tetraShape);
	}
	CollisionShapes[PlayerModel] = collisionModelPlayer;

	auto convModelBullet = ConvDec(Models[SpawnObjectModel]);
	btCompoundShape *collisionModelBullet = new btCompoundShape();
	for (auto &tetra : convModelBullet)
	{
		btBU_Simplex1to4 *tetraShape = new btBU_Simplex1to4(std2bt(tetra[0]), std2bt(tetra[1]), std2bt(tetra[2]), std2bt(tetra[3]));
		btTransform trans;
		trans.setIdentity();
		trans.setOrigin({0, 0, 0});
		collisionModelBullet->addChildShape(trans, tetraShape);
	}
	CollisionShapes[SpawnObjectModel] = collisionModelBullet;

	Simulation World;
	for (auto Entity : Entities)
	{
		if (Entity->RenderObject)
		{
			auto T = Targets[Entity->RenderObjectID];
			auto transM = glm::value_ptr(Entity->Transform);
			btScalar btTransM[] = {transM[0], transM[1], transM[2], transM[3], transM[4], transM[5], transM[6], transM[7], transM[8], transM[9], transM[10], transM[11], transM[12], transM[13], transM[14], transM[15]};
			btTransform trans;
			trans.setFromOpenGLMatrix(btTransM);
			auto state = new btDefaultMotionState(trans);
			if (Entity->Mass > 0)
			{
				if (!CollisionShapes.contains(T.ModelID))
				{
					auto convModel = ConvDec(Models[T.ModelID]);
					btCompoundShape *collisionModel = new btCompoundShape();
					for (auto &tetra : convModel)
					{
						btBU_Simplex1to4 *tetraShape = new btBU_Simplex1to4(std2bt(tetra[0]), std2bt(tetra[1]), std2bt(tetra[2]), std2bt(tetra[3]));
						btTransform trans;
						trans.setIdentity();
						trans.setOrigin({0, 0, 0});
						collisionModel->addChildShape(trans, tetraShape);
					}
					CollisionShapes[T.ModelID] = collisionModel;
				}
				auto collisionModel = CollisionShapes[T.ModelID];
				btVector3 inertia;
				btScalar mass(Entity->Mass);
				collisionModel->calculateLocalInertia(mass, inertia);
				btRigidBody::btRigidBodyConstructionInfo info(mass, state, collisionModel, inertia);
				auto body = new btRigidBody(info);
				Entity->SetBody(body);
				body->setUserPointer((void *)Entity);
				World.GetCollisionWorld()->addRigidBody(body, DynamicObjectGroup, DynamicObjectGroup | StaticObjectGroup | 0b1);
			}
			else
			{
				btVector3 vertex1, vertex2, vertex3;
				btTriangleMesh *triangleMeshTerrain = new btTriangleMesh();

				for (int i = 0; i < Models[T.ModelID].Indices.size() / 3; i++)
				{
					auto helpVertex1 = Models[T.ModelID].Vertices[Models[T.ModelID].Indices[i * 3]];
					auto helpVertex2 = Models[T.ModelID].Vertices[Models[T.ModelID].Indices[i * 3 + 1]];
					auto helpVertex3 = Models[T.ModelID].Vertices[Models[T.ModelID].Indices[i * 3 + 2]];
					vertex1 = btVector3(helpVertex1.Position.x, helpVertex1.Position.y, helpVertex1.Position.z);
					vertex2 = btVector3(helpVertex2.Position.x, helpVertex2.Position.y, helpVertex2.Position.z);
					vertex3 = btVector3(helpVertex3.Position.x, helpVertex3.Position.y, helpVertex3.Position.z);

					triangleMeshTerrain->addTriangle(vertex1, vertex2, vertex3);
				}

				auto colShape = new btBvhTriangleMeshShape(triangleMeshTerrain, true);

				auto colobj = new btCollisionObject();
				colobj->setCollisionShape(colShape);
				colobj->setWorldTransform(trans);
				Entity->SetBody(colobj);
				colobj->setUserPointer((void *)Entity);
				World.GetCollisionWorld()->addCollisionObject(colobj, StaticObjectGroup, DynamicObjectGroup | 0b1);
			}
		}
	}
	std::map<int, Player> Players;
	std::map<int, FreeCam> PlayersFreeCam;
	std::map<int, CharacterController *> PlayersControllers;

	auto NetManager = new ServerNetManager(IP, Port);
	GamePacket *Response = nullptr;
	auto VirtualTime = std::chrono::high_resolution_clock::now();
	auto VirtualSendTime = std::chrono::high_resolution_clock::now();
	auto StartTime = VirtualTime;

	using namespace std::chrono_literals;
	while (true)
	{
		// Delete old users
		auto DelUser = NetManager->GetDelUsr();
		for (auto User : DelUser)
		{
			Log(LogModule::SERVER_MAIN, LogSeverity::INFO, std::format("Player {} disconnected", User));
		}

		// Add new users
		auto NewUser = NetManager->GetNewUsr();
		for (auto User : NewUser)
		{
			Log(LogModule::SERVER_MAIN, LogSeverity::INFO, std::format("Player {} connected", User));
		}

		// Process incomming packets
		auto Packets = NetManager->Pull();
		for (auto &[Packet, PlayerID] : Packets)
		{
			for (auto &[Tag, Object] : Packet->Tags)
			{
				switch (Tag)
				{
					case GamePacketTag::NET_CLIENT_KEY_ACTION:
					{
						if (Players[PlayerID].ClientState != ProtocolState::GAME)
						{
							break;
						}
						auto Action = std::any_cast<KeyAction &>(Object);
						if (Action.Key == GLFW_KEY_E and (Action.Mode == GLFW_PRESS))
						{
							auto &Camera = PlayersFreeCam[PlayerID];
							Response = new GamePacket();
							auto rot = glm::normalize(Camera.GetRot());
							auto distance = 0.9;
							std::vector<double> pos = {Camera.GetPos().x + rot.x * distance, Camera.GetPos().y + rot.y * distance, Camera.GetPos().z + rot.z * distance};
							std::vector<int> IDs;
							IDs.reserve(Players.size());
							for (auto &[ID, Player] : Players)
							{
								IDs.push_back(ID);
							}
							std::tuple<int, std::vector<double>, int> tuple = {SpawnObjectModel, pos, Entities.size()};
							Response->Insert(GamePacketTag::NET_SERVER_OBJECT_ADD, tuple);
							NetManager->Push(Response, IDs);
							auto SoundMsg = new GamePacket;
							SoundMsg->Insert(GamePacketTag::NET_SERVER_SOUND, SoundType::THROW);
							NetManager->Push(SoundMsg, IDs);
							auto NewObject = new GameObjectBullet();

							auto collisionModel = CollisionShapes[Targets[SpawnObjectModel].ModelID];
							auto transformation = glm::mat4(1.0f);
							transformation = glm::translate(transformation, {pos[0], pos[1], pos[2]});
							NewObject->RenderObject = true;
							NewObject->RenderObjectID = SpawnObjectModel;
							NewObject->Transform = transformation;
							Entities.push_back(NewObject);
							NewObject->Entity = Entities.size() - 1;
							auto transM = glm::value_ptr(transformation);
							btScalar btTransM[] = {transM[0], transM[1], transM[2], transM[3], transM[4], transM[5], transM[6], transM[7], transM[8], transM[9], transM[10], transM[11], transM[12], transM[13], transM[14], transM[15]};
							btTransform trans;
							trans.setFromOpenGLMatrix(btTransM);
							auto state = new btDefaultMotionState(trans);
							btVector3 inertia;
							btScalar mass(NewObject->Mass);
							collisionModel->calculateLocalInertia(mass, inertia);
							btRigidBody::btRigidBodyConstructionInfo info(mass, state, collisionModel, inertia);
							auto body = new btRigidBody(info);
							auto velocity = 6;
							body->setLinearVelocity({velocity * rot.x, velocity * rot.y, velocity * rot.z});
							NewObject->SetBody(body);
							body->setUserPointer((void *)NewObject);
							World.GetCollisionWorld()->addRigidBody(body, DynamicObjectGroup, DynamicObjectGroup | StaticObjectGroup | 0b1);
						}
						else
							ProcessKeyAction(Action, PlayersFreeCam[PlayerID], PlayersControllers[PlayerID]);

						break;
					}
					case GamePacketTag::NET_CLIENT_MOUSE_ACTION:
					{
						if (Players[PlayerID].ClientState != ProtocolState::GAME)
						{
							break;
						}
						ProcessMouseAction(std::any_cast<MouseAction &>(Object), PlayersFreeCam[PlayerID]);

						break;
					}
					case GamePacketTag::NET_CLIENT_INFO:
					{
						Players[PlayerID].Nick = std::any_cast<ClientInfo>(Object).Nick;

						Response = new GamePacket();
						Response->Insert(GamePacketTag::NET_SERVER_INFO, ServerInfo({Map, PlayerID}));
						NetManager->Push(Response, {PlayerID});

						break;
					}
					case GamePacketTag::NET_CLIENT_REQUEST:
					{
						switch (std::any_cast<ClientRequest>(Object))
						{
							case ClientRequest::CONNECT:
							{
								if (Players.size() == MaxPlayers - 1)
								{
									Response = new GamePacket();
									Response->Insert(GamePacketTag::NET_SERVER_RESPONSE, ServerResponse::REJECT);
									NetManager->Push(Response, {PlayerID});
									// NetManager->DeleteUser(PlayerID);
								}
								else
								{
									Response = new GamePacket();
									Response->Insert(GamePacketTag::NET_SERVER_RESPONSE, ServerResponse::ACCEPT);
									NetManager->Push(Response, {PlayerID});

									Players[PlayerID] = Player{.ClientState = ProtocolState::INIT};
									// PlayersFreeCam[PlayerID] = FreeCam(glm::vec3(0.0f, 2.0f, -5.0f), glm::vec2(0.0f, 0.0f));
									btTransform contrTrans;
									contrTrans.setIdentity();
									contrTrans.setOrigin({0, 0.5, 0});
									const auto sphere = new btCapsuleShape(0.5, 0.5);
									auto state = new btDefaultMotionState(contrTrans);
									btVector3 inertia;
									btScalar mass(1);
									sphere->calculateLocalInertia(mass, inertia);
									btRigidBody::btRigidBodyConstructionInfo info(mass, state, sphere, inertia);
									auto rigid = new btRigidBody(info);
									rigid->setGravity(World.GetCollisionWorld()->getGravity());
									PlayersControllers[PlayerID] = new CharacterController(rigid, sphere);
									World.GetCollisionWorld()->addRigidBody(rigid, DynamicObjectGroup, DynamicObjectGroup | StaticObjectGroup | 0b1);
									World.GetCollisionWorld()->addAction(PlayersControllers[PlayerID]);
								}
								break;
							}
							case ClientRequest::DISCONNECT:
							{
								Response = new GamePacket();
								Response->Insert(GamePacketTag::NET_SERVER_RESPONSE, ServerResponse::DISCONNECT);
								NetManager->Push(Response, {PlayerID});

								auto ResponseTwo = new GamePacket();
								std::vector<int> IDs;
								IDs.reserve(Players.size());
								for (auto &[ID, Player] : Players)
								{
									if (ID != PlayerID)
										IDs.push_back(ID);
								}
								ResponseTwo->Insert(GamePacketTag::NET_SERVER_OBJECT_DEL, *PlayersFreeCam[PlayerID].Entity);
								NetManager->Push(ResponseTwo, IDs);

								auto entityNumber = *PlayersFreeCam[PlayerID].Entity;
								auto deletedEntity = Entities.erase(Entities.begin() + entityNumber);
								for (auto i = deletedEntity; i != Entities.end(); i++)
								{
									(*i)->Entity = *(*i)->Entity - 1;
								}
								for (auto &[ID, Cam] : PlayersFreeCam)
								{
									if (*Cam.Entity > entityNumber)
										Cam.Entity = *Cam.Entity - 1;
								}
								Players.erase(PlayerID);
								PlayersFreeCam.erase(PlayerID);
								btCollisionObject *obj = PlayersControllers[PlayerID]->mRigidBody;
								World.GetCollisionWorld()->removeAction(PlayersControllers[PlayerID]);
								delete PlayersControllers[PlayerID];
								PlayersControllers.erase(PlayerID);
								btRigidBody *body = btRigidBody::upcast(obj);
								if (body && body->getMotionState())
								{
									delete body->getMotionState();
								}
								World.GetCollisionWorld()->removeCollisionObject(obj);
								delete obj;

								// NetManager->DeleteUser(PlayerID);
								break;
							}
							case ClientRequest::LEVEL:
							{
								Response = new GamePacket();
								Response->Insert(GamePacketTag::BUNDLE, LevelData);
								NetManager->Push(Response, {PlayerID});

								Players[PlayerID].ClientState = ProtocolState::DOWNLOAD;
								break;
							}
							case ClientRequest::READY:
							{
								PlayersFreeCam[PlayerID] = FreeCam(glm::vec3(0.0f, 1.0f, -5.0f), glm::vec2(0.0f, 0.0f));
								Response = new GamePacket();
								Response->Insert(GamePacketTag::NET_SERVER_STATE, std::tuple{Instance, PlayersFreeCam});
								NetManager->Push(Response, {PlayerID});

								Players[PlayerID].ClientState = ProtocolState::GAME;

								auto ResponseTwo = new GamePacket();
								std::vector<int> IDs;
								IDs.reserve(Players.size());
								for (auto &[ID, Player] : Players)
								{
									IDs.push_back(ID);
								}
								std::vector<double> pos = {PlayersFreeCam[PlayerID].GetPos().x, PlayersFreeCam[PlayerID].GetPos().y - 1, PlayersFreeCam[PlayerID].GetPos().z};
								auto NewObject = new PlayerGameObject();

								auto transformation = glm::mat4(1.0f);
								transformation = glm::translate(transformation, {pos[0], pos[1], pos[2]});
								NewObject->RenderObject = true;
								NewObject->RenderObjectID = PlayerModel;
								NewObject->Transform = transformation;
								Entities.push_back(NewObject);
								NewObject->Entity = Entities.size() - 1;
								PlayersFreeCam[PlayerID].Entity = Entities.size() - 1;
								PlayersControllers[PlayerID]->mRigidBody->setUserPointer((void *)NewObject);
								NewObject->Body = PlayersControllers[PlayerID]->mRigidBody;

								std::tuple<int, std::vector<double>, int> tuple = {PlayerModel, pos, Entities.size() - 1};
								ResponseTwo->Insert(GamePacketTag::NET_SERVER_OBJECT_ADD, tuple);
								NetManager->Push(ResponseTwo, IDs);
								break;
							}
						}
						break;
					}
				}
			}
			delete Packet;
		}
		// Send outbound packets (deltas)
		if (!(std::chrono::high_resolution_clock::now() - VirtualSendTime < 0ns))
		{
			if (not Players.empty())
			{
				std::map<int, std::vector<double>> positions;
				int Counter = 0;
				for (auto &i : Entities)
				{
					if (i->RenderObject)
					{
						if (i->Mass <= 0)
						{
							Counter++;
							continue;
						}
						auto body = btRigidBody::upcast(i->GetBody());
						double trans[16];
						btTransform transform;
						body->getMotionState()->getWorldTransform(transform);
						transform.getOpenGLMatrix(trans);
						if (i->Entity)
							positions[*i->Entity] = std::move(btScalar2double(trans));
						else
							positions[Counter] = std::move(btScalar2double(trans));
						Counter++;
					}
				}
				Response = new GamePacket();
				std::vector<int> IDs;
				IDs.reserve(Players.size());
				for (auto &[ID, Player] : Players)
				{
					IDs.push_back(ID);
					btTransform trans;
					PlayersControllers[ID]->getBody()->getMotionState()->getWorldTransform(trans);
					auto origin = trans.getOrigin();
					auto camPos = btVector3{origin.x(), origin.y(), origin.z()} + btVector3{0, 0.5, 0};
					PlayersFreeCam[ID].SetPosition({camPos.x(), camPos.y(), camPos.z()});
					if (PlayersFreeCam[ID].Entity)
					{
						auto transM = glm::value_ptr(Entities[*PlayersFreeCam[ID].Entity]->Transform);
						positions[*PlayersFreeCam[ID].Entity] = {transM[0], transM[1], transM[2],  transM[3],  transM[4],  transM[5],		 transM[6],	 transM[7],
																 transM[8], transM[9], transM[10], transM[11], camPos.x(), camPos.y() - 0.5, camPos.z(), transM[15]};
					}
				}
				std::tuple tmp{PlayersFreeCam, positions};
				Response->Insert(GamePacketTag::NET_SERVER_DELTA, ServerDelta{tmp});
				NetManager->Push(Response, IDs); // broadcast
			}
			VirtualSendTime += 2 * std::chrono::high_resolution_clock::duration(TickTime);
		}

		VirtualTime += std::chrono::high_resolution_clock::duration(TickTime);
		while (std::chrono::high_resolution_clock::now() - VirtualTime < 0ns)
			;

		World.GetCollisionWorld()->stepSimulation({TickTime / 1000000000.}, 1, {TickTime / 1000000000.});
		// CollisionCallbacks
		ContactResultCallback callback;
		for (int i = 0; i < World.GetCollisionWorld()->getNumCollisionObjects(); i++)
		{
			if (!World.GetCollisionWorld()->getCollisionObjectArray()[i]->isStaticObject())
				World.GetCollisionWorld()->contactTest(World.GetCollisionWorld()->getCollisionObjectArray()[i], callback);
		}
		// Remove Gameobjects
		for (auto Entity = Entities.begin(); Entity != Entities.end();)
		{
			if ((*Entity)->DeleteFlag)
			{
				auto DelPacket = new GamePacket();
				std::vector<int> IDs;
				IDs.reserve(Players.size());
				for (auto &[ID, Player] : Players)
				{
					IDs.push_back(ID);
				}
				DelPacket->Insert(GamePacketTag::NET_SERVER_OBJECT_DEL, *(*Entity)->Entity);
				NetManager->Push(DelPacket, IDs);

				btCollisionObject *obj = (*Entity)->GetBody();
				btRigidBody *body = btRigidBody::upcast(obj);
				if (body && body->getMotionState())
				{
					delete body->getMotionState();
				}
				World.GetCollisionWorld()->removeCollisionObject(obj);
				delete obj;

				auto entityNumber = *(*Entity)->Entity;
				Entity = Entities.erase(Entity);

				for (auto i = Entity; i != Entities.end(); i++)
				{
					(*i)->Entity = *(*i)->Entity - 1;
				}
				for (auto &[ID, Cam] : PlayersFreeCam)
				{
					if (*Cam.Entity > entityNumber)
						Cam.Entity = *Cam.Entity - 1;
				}
			}
			else
				Entity++;
		}
	}

	delete NetManager;
	return 0;
}
