#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include "audio/sound_object.hpp"
#include "level/level.hpp"
#include "magic_enum/magic_enum.hpp"
#include "net/flac_adapter.hpp"
#include "net/game_packet.hpp"
#include "net/json_adapter.hpp"
#include "net/lzma_adapter.hpp"
#include "net/obj_adapter.hpp"
#include "net/ply_adapter.hpp"
#include "net/webp_adapter.hpp"
#include "rapidjson/rapidjson.h"
#include "render/shader.hpp"
#include "render/structures.hpp"
#include "shaderc/shaderc.hpp"
#include "vulkan/vulkan.hpp"

int main(int argc, char **argv)
{
	std::string LevelPath;
	std::string DataPath;

	if (argc == 2)
	{
		LevelPath = std::string(argv[1]);
		DataPath = "./";
	}
	else
	{
		LevelPath = std::string(DATASRC);
		DataPath = std::string(DATA);
	}

	std::map<std::string, uint16_t> ModelIDMapping;
	std::map<std::string, uint16_t> TextureIDMapping;
	std::map<std::string, uint16_t> TargetIDMapping;

	std::vector<Model> Models;
	std::vector<WebPAdapter<Texture>> Textures;
	std::vector<Light> Lights;
	std::vector<Target> Targets;
	std::multimap<std::string, Shader> Shaders;
	std::map<std::string, SoundObject> Sounds;
	Level Instance;

	auto ObjAdapterI = ObjFileAdapter<Model>();
	auto PlyAdapterI = PlyFileAdapter<Model>();
	for (auto &Entry : std::filesystem::directory_iterator(LevelPath + "/models"))
	{
		ModelIDMapping[Entry.path().stem().string()] = static_cast<uint16_t>(Models.size());

		if (Entry.path().extension() == ".obj")
			Models.push_back(ObjAdapterI.Load(Entry));

		if (Entry.path().extension() == ".ply")
		{
			Models.push_back(PlyAdapterI.Load(Entry));
		}
	}

	std::cout << "Loaded " << Models.size() << " models" << std::endl;

	auto WebPAdapterI = WebPFileAdapter<Texture>();
	for (auto &Entry : std::filesystem::directory_iterator(LevelPath + "/textures"))
	{
		TextureIDMapping[Entry.path().stem().string()] = static_cast<uint16_t>(Textures.size());
		Textures.push_back(WebPAdapter(WebPAdapterI.Load(Entry)));
	}

	std::cout << "Loaded " << Textures.size() << " textures" << std::endl;

	for (auto &Entry : std::filesystem::directory_iterator(LevelPath + "/shaders"))
	{
		std::fstream File(Entry.path().string(), std::ios::in | std::ios::binary);
		std::string ShaderCode(Entry.file_size(), ' ');
		File.read(ShaderCode.data(), Entry.file_size());

		shaderc_shader_kind Stage = shaderc_shader_kind::shaderc_vertex_shader;

		if (Entry.path().extension() == ".vert")
			Stage = shaderc_shader_kind::shaderc_vertex_shader;

		if (Entry.path().extension() == ".frag")
			Stage = shaderc_shader_kind::shaderc_fragment_shader;

		shaderc::CompileOptions Options;
		Options.SetTargetSpirv(shaderc_spirv_version::shaderc_spirv_version_1_5);
		Options.SetTargetEnvironment(shaderc_target_env::shaderc_target_env_vulkan, shaderc_env_version::shaderc_env_version_vulkan_1_2);
		Options.SetOptimizationLevel(shaderc_optimization_level::shaderc_optimization_level_performance);

		shaderc::Compiler Compiler;
		auto Result = Compiler.CompileGlslToSpv(ShaderCode, Stage, Entry.path().string().c_str(), "main", Options);
		if (not Result.GetCompilationStatus() == shaderc_compilation_status::shaderc_compilation_status_success)
			std::cout << Result.GetErrorMessage() << std::endl;

		Shaders.insert({Entry.path().stem().string(), Shader(Stage, std::vector<uint32_t>(Result.begin(), Result.end()))});
	}

	std::cout << "Loaded " << Shaders.size() << " shaders" << std::endl;

	for (auto &Entry : std::filesystem::directory_iterator(LevelPath + "/scripts"))
	{
		std::fstream File(Entry.path().string(), std::ios::in | std::ios::binary);
		std::string Data(Entry.file_size(), ' ');
		File.read(Data.data(), Data.size());

		rapidjson::Document Document;
		Document.Parse(Data.data(), Data.size());

		if (Entry.path().stem().string() == "level")
		{
			for (auto &Entry : Document.GetObject().FindMember("RenderTemplates")->value.GetObject())
			{
				TargetIDMapping[Entry.name.GetString()] = static_cast<uint16_t>(TargetIDMapping.size());

				uint16_t ModelID = ModelIDMapping[Entry.value.FindMember("Model")->value.GetString()];
				uint16_t TextureID = TextureIDMapping[Entry.value.FindMember("Texture")->value.GetString()];

				std::vector<uint16_t> LightID;
				if (Entry.value.HasMember("Lights"))
				{
					for (auto &LightEntry : Entry.value.FindMember("Lights")->value.GetArray())
					{
						glm::vec3 Position(0.0f, 0.0f, 0.0f);
						glm::vec4 Color(1.0f, 1.0f, 1.0f, 1.0f);

						if (LightEntry.GetObject().HasMember("Position"))
						{
							Position.x = LightEntry.GetObject().FindMember("Position")->value.GetArray()[0].GetFloat();
							Position.y = LightEntry.GetObject().FindMember("Position")->value.GetArray()[1].GetFloat();
							Position.z = LightEntry.GetObject().FindMember("Position")->value.GetArray()[2].GetFloat();
						}
						if (LightEntry.GetObject().HasMember("Color"))
						{
							Color.x = LightEntry.GetObject().FindMember("Color")->value.GetArray()[0].GetFloat();
							Color.y = LightEntry.GetObject().FindMember("Color")->value.GetArray()[1].GetFloat();
							Color.z = LightEntry.GetObject().FindMember("Color")->value.GetArray()[2].GetFloat();
							Color.w = LightEntry.GetObject().FindMember("Color")->value.GetArray()[3].GetFloat();
						}

						LightID.push_back(static_cast<uint16_t>(Lights.size()));
						Lights.push_back(Light(Position, Color));
					}
				}

				Targets.push_back(Target(ModelID, TextureID, LightID));
			}

			std::cout << "Loaded " << Lights.size() << " lights" << std::endl;
			std::cout << "Loaded " << Targets.size() << " render templates" << std::endl;

			std::vector<GameObject *> Entities;
			for (auto &Entry : Document.GetObject().FindMember("Entities")->value.GetArray())
			{
				if (not TargetIDMapping.contains(Entry.GetObject().FindMember("RenderObject")->value.GetString()))
					throw std::runtime_error("Invalid template argument");

				Entry.GetObject().FindMember("RenderObject")->value.SetUint(TargetIDMapping[Entry.GetObject().FindMember("RenderObject")->value.GetString()]);
				std::string Entity = Entry.GetObject().FindMember("GameObject")->value.GetString();
				std::transform(Entity.begin(), Entity.end(), Entity.begin(), ::toupper);

				Entities.push_back(GameObject::DecodeBase(magic_enum::enum_cast<GameObjectTag>(Entity).value(), Entry));
			}

			std::cout << "Loaded " << Entities.size() << " entities" << std::endl;
			Instance = Level(Entities);
		}
	}

	auto FLACAdapterI = FLACFileAdapter<SoundObject>();
	for (auto &Entry : std::filesystem::directory_iterator(std::string(DATASRC) + "/audio"))
	{
		Sounds[Entry.path().stem().string()] = FLACAdapterI.Load(Entry);
	}

	std::cout << "Loaded " << Sounds.size() << " sounds" << std::endl;

	GamePacket Packet;
	Packet.Insert(GamePacketTag::RENDER_MODEL_TEMPLATE_BULK, LZMAAdapter(Models));
	Packet.Insert(GamePacketTag::RENDER_TEXTURE_TEMPLATE_BULK, Textures);
	Packet.Insert(GamePacketTag::RENDER_LIGHT_TEMPLATE_BULK, LZMAAdapter(Lights));
	Packet.Insert(GamePacketTag::RENDER_TARGET_TEMPLATE_BULK, LZMAAdapter(Targets));
	Packet.Insert(GamePacketTag::RENDER_SHADER_BULK, LZMAAdapter(Shaders));
	Packet.Insert(GamePacketTag::GAME_LEVEL, LZMAAdapter(Instance));
	Packet.Insert(GamePacketTag::AUDIO_BULK, Sounds);

	std::cout << "Total level size: " << Packet.Data.size() << std::endl;

	std::fstream Stream(DataPath + "/level.bin", std::ios::out | std::ios::binary);
	Stream.write(reinterpret_cast<char *>(Packet.Data.data()), Packet.Data.size());
	Stream.close();
}