#include <fstream>
#include <iostream>
#include <string>
#include "auxiliary/physics_helper.hpp"
#include "glfw/glfw3.h"
#include "glm/gtc/matrix_transform.hpp"
#include "level/action.hpp"
#include "level/level.hpp"
#include "net/game_packet.hpp"
#include "net/lzma_adapter.hpp"
#include "net/net_manager.hpp"
#include "net/protocol.hpp"
#include "net/webp_adapter.hpp"
#include "render/freecam.hpp"
#include "render/renderer.hpp"
#include "render/shader.hpp"
#include <glm/gtc/type_ptr.hpp>
#include "auxiliary/physics_helper.hpp"
#include "audio/audio_object.hpp"
#include "audio/sound_object.hpp"

struct WindowHelper
{
	GamePacket *Request = nullptr;
	double X = 0, Y = 0, accDeltaX = 0, accDeltaY = 0;
	bool Initial = true;
};

void KeyCallback(GLFWwindow *Window, int Key, int Scancode, int Action, int Mods)
{
	WindowHelper *Helper = reinterpret_cast<WindowHelper *>(glfwGetWindowUserPointer(Window));
	Helper->Request = new GamePacket();
	Helper->Request->Insert(GamePacketTag::NET_CLIENT_KEY_ACTION, KeyAction{.Key = Key, .Mode = Action});

	if (Key == GLFW_KEY_ESCAPE)
		glfwSetWindowShouldClose(Window, true);
}

void CursorCallback(GLFWwindow *Window, double X, double Y)
{
	WindowHelper *Helper = reinterpret_cast<WindowHelper *>(glfwGetWindowUserPointer(Window));
	if (Helper->Initial)
	{
		glfwSetCursorPos(Window, 0, 0);
		Helper->Initial = false;
		return;
	}
	double DeltaX = X - Helper->X;
	double DeltaY = Y - Helper->Y;

	Helper->accDeltaX += DeltaX;
	Helper->accDeltaY += DeltaY;

	Helper->X = X;
	Helper->Y = Y;
}

int main(int argc, char **argv)
{
	using namespace std::chrono_literals;
	uint64_t TickTime = 34000000;
	auto VirtualTime = std::chrono::high_resolution_clock::now() + std::chrono::high_resolution_clock::duration(TickTime);

	std::string IP, Port, Nick;
	std::vector<ServerDelta> previousDeltas;
	int myModel = -1;
	bool gotMyModel = false;

	initialize_context();
	float listenerOri[] = { .0f, .0f, 1.f, .0f, 1.f, .0f };
	auto* listenerPos = new float[3]{ 0, 0, 4 };
	float listenerVel[] = { 0.02, 0.005, -0.02 };
	set_listener(listenerPos, listenerVel, listenerOri);
	AudioObject ThrowPlayer;

	if (argc > 1)
	{
		Nick = std::string(argv[1]);
	}
	else
	{
		Nick = "nameless tee";
	}
	if (argc > 3)
	{
		IP = std::string(argv[2]);
		Port = std::string(argv[3]);
	}
	else
	{
		IP = "127.0.0.1";
		Port = "8303";
	}

	auto NetManager = new ClientNetManager(IP, Port);
	auto *Request = new GamePacket();
	Request->Insert(GamePacketTag::NET_CLIENT_REQUEST, ClientRequest::CONNECT);
	NetManager->Push(Request);

	glfwInit();
	int MonitorCount;
	auto Monitors = glfwGetMonitors(&MonitorCount);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow *pWindow = glfwCreateWindow(800, 600, "vktest", nullptr, nullptr);

	WindowHelper Helper;
	glfwSetWindowUserPointer(pWindow, &Helper);
	glfwSetKeyCallback(pWindow, KeyCallback);
	glfwSetInputMode(pWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(pWindow, CursorCallback);

	Renderer Rend(pWindow, false);
	FreeCam Camera;
	Rend.BindCamera(&Camera);

	std::string LevelName;
	std::vector<Model> Models;
	std::vector<WebPAdapter<Texture>> Textures;
	std::vector<Light> Lights;
	std::vector<Target> Targets;
	std::multimap<std::string, Shader> Shaders;
	std::map<std::string, SoundObject> Sounds;
	Level Instance;

	std::vector<GamePacket *> Packets;
	ProtocolState State = ProtocolState::HANDSHAKE;
	bool LevelReady = false;
	int PlayerID = -1;
	size_t Counter = 0;

	while (State not_eq ProtocolState::DIE)
	{
		// Process incoming packets
		auto Packets = NetManager->Pull();
		for (auto &Packet : Packets)
		{
			for (auto &[Tag, Object] : Packet->Tags)
			{
				switch (Tag)
				{
					case GamePacketTag::BUNDLE:
					{
						if (State == ProtocolState::DOWNLOAD)
						{
							auto LevelData = std::any_cast<std::vector<uint8_t>>(Object);
							auto LevelFile = std::fstream(std::format("{}/{}.bin", std::string(DATA), LevelName), std::ios::out | std::ios::binary);
							LevelFile.write(reinterpret_cast<char *>(LevelData.data()), LevelData.size());
							LevelFile.close();
							LevelReady = true;
						}
						break;
					}
					case GamePacketTag::NET_SERVER_DELTA:
					{
						auto recv = std::any_cast<ServerDelta::ParamT>(Object);
						ServerDelta Delta{recv};
						if (State != ProtocolState::GAME)
						{
							previousDeltas.push_back(Delta);
							break;
						}
						Camera = Delta.cams[PlayerID];
						for (auto &[id, pos] : Delta.pos)
						{
							if (id == myModel)
								continue;
							glm::mat4 transform{glm::make_mat4(pos.data())};
							Rend.UpdateTarget(id, transform);
						}
						break;
					}
					case GamePacketTag::NET_SERVER_INFO:
					{
						LevelName = std::any_cast<ServerInfo>(Object).Level;
						PlayerID = std::any_cast<ServerInfo>(Object).PlayerID;
						if (std::filesystem::exists(std::format("{}/{}.bin", std::string(DATA), LevelName)))
						{
							LevelReady = true;
						}
						else
						{
							Request = new GamePacket();
							Request->Insert(GamePacketTag::NET_CLIENT_REQUEST, ClientRequest::LEVEL);
							NetManager->Push(Request);
							State = ProtocolState::DOWNLOAD;
						}
						break;
					}
					case GamePacketTag::NET_SERVER_RESPONSE:
					{
						switch (State)
						{
							case ProtocolState::HANDSHAKE:
							{
								if (std::any_cast<ServerResponse>(Object) == ServerResponse::ACCEPT)
								{
									Request = new GamePacket();
									Request->Insert(GamePacketTag::NET_CLIENT_INFO, Nick);
									NetManager->Push(Request);
									State = ProtocolState::INIT;
								}
								if (std::any_cast<ServerResponse>(Object) == ServerResponse::REJECT)
								{
									return 0;
								}
								break;
							}
						}
						break;
					}
					case GamePacketTag::NET_SERVER_STATE:
					{
						Instance = std::get<0>(std::any_cast<std::tuple<Level, std::map<int, FreeCam>>>(Object));
						Camera = std::get<1>(std::any_cast<std::tuple<Level, std::map<int, FreeCam>>>(Object))[PlayerID];

						Counter = 0;
						for (auto &Entity : Instance.GetObjects())
						{
							if (Entity->RenderObject)
							{
								Target T(Targets[Entity->RenderObjectID]);
								T.Transform = Entity->Transform;
								Rend.CreateTarget(Counter, T);
								Counter++;
							}
						}
						Rend.Draw();
						State = ProtocolState::GAME;
						for (auto Delta : previousDeltas)
						{
							Camera = Delta.cams[PlayerID];
							for (auto &[id, pos] : Delta.pos)
							{
								if (id == myModel)
									continue;
								glm::mat4 transform{glm::make_mat4(pos.data())};
								Rend.UpdateTarget(id, transform);
							}
						}
						break;
					}
					case GamePacketTag::NET_SERVER_OBJECT_ADD:
					{
						auto newObjectId = std::get<0>(std::any_cast<std::tuple<int, std::vector<double>, int>>(Object));
						auto Pos = std::get<1>(std::any_cast<std::tuple<int, std::vector<double>, int>>(Object));
						auto Id = std::get<2>(std::any_cast<std::tuple<int, std::vector<double>, int>>(Object));
						glm::mat4 transform = glm::mat4(1.0f);
						if (!gotMyModel)
						{
							gotMyModel = true;
							myModel = Id;
							transform = glm::translate(transform, {10000, -10000, 10000});
						}
						else
						{
							transform = glm::translate(transform, {Pos[0], Pos[1], Pos[2]});
						}
						Target T(Targets[newObjectId]);
						T.Transform = transform;
						Rend.CreateTarget(Id, T);
						break;
					}
					case GamePacketTag::NET_SERVER_OBJECT_DEL:
					{
						auto ID = std::any_cast<int>(Object);
						if (ID < myModel)
							myModel--;
						Rend.DeleteTarget(ID);
						break;
					}
					case GamePacketTag::NET_SERVER_SOUND:
						auto Type = std::any_cast<SoundType>(Object);
						switch (Type)
						{
						case SoundType::THROW:
							ThrowPlayer.Play();
						}

				}
			}

			delete Packet;
		}

		// Send outbound packets
		if (Helper.Request)
		{
			NetManager->Push(Helper.Request);
			Helper.Request = nullptr;
		}

		if (!(std::chrono::high_resolution_clock::now() - VirtualTime < 0ns) and (Helper.accDeltaX != 0 or Helper.accDeltaY != 0))
		{
			auto MousePacket = new GamePacket;
			MousePacket->Insert(GamePacketTag::NET_CLIENT_MOUSE_ACTION, MouseAction{.X = Helper.accDeltaX, .Y = Helper.accDeltaY});
			NetManager->Push(MousePacket);
			Helper.accDeltaX = 0;
			Helper.accDeltaY = 0;
			VirtualTime += std::chrono::high_resolution_clock::duration(TickTime);
		}

		if (LevelReady)
		{
			LevelReady = false;

			std::fstream Data(std::format("{}/{}.bin", std::string(DATA), LevelName), std::ios::in | std::ios::binary);
			std::vector<uint8_t> Buffer(std::filesystem::file_size(std::format("{}/{}.bin", std::string(DATA), LevelName)));
			Data.read(reinterpret_cast<char *>(Buffer.data()), Buffer.size());
			GamePacket Packet(Buffer);
			auto &Tags = Packet.Tags;

			Models = std::any_cast<LZMAAdapter<std::vector<Model>> &>(Tags[GamePacketTag::RENDER_MODEL_TEMPLATE_BULK]).Object;
			Textures = std::any_cast<std::vector<WebPAdapter<Texture>> &>(Tags[GamePacketTag::RENDER_TEXTURE_TEMPLATE_BULK]);
			Lights = std::any_cast<LZMAAdapter<std::vector<Light>> &>(Tags[GamePacketTag::RENDER_LIGHT_TEMPLATE_BULK]).Object;
			Targets = std::any_cast<LZMAAdapter<std::vector<Target>> &>(Tags[GamePacketTag::RENDER_TARGET_TEMPLATE_BULK]).Object;
			Shaders = std::any_cast<LZMAAdapter<std::multimap<std::string, Shader>> &>(Tags[GamePacketTag::RENDER_SHADER_BULK]).Object;
			Instance = std::any_cast<LZMAAdapter<Level> &>(Tags[GamePacketTag::GAME_LEVEL]).Object;
			Sounds = std::any_cast<std::map<std::string, SoundObject>&>(Tags[GamePacketTag::AUDIO_BULK]);
			
			ThrowPlayer.QueueBuffers(&Sounds["throw"]);

			for (size_t i = 0; i < Models.size(); i++)
			{
				Rend.CreateModel(i, Models[i]);
			}
			for (size_t i = 0; i < Textures.size(); i++)
			{
				Rend.CreateTexture(i, Textures[i].Object);
			}
			for (size_t i = 0; i < Lights.size(); i++)
			{
				Rend.CreateLight(i, Lights[i]);
			}

			Request = new GamePacket();
			Request->Insert(GamePacketTag::NET_CLIENT_REQUEST, ClientRequest::READY);
			NetManager->Push(Request);

			// State = ProtocolState::GAME;
		}
		if (State == ProtocolState::GAME)
			Rend.Draw();
		if (glfwWindowShouldClose(pWindow))
		{
			State = ProtocolState::DIE;
		}
		glfwPollEvents();
	}

	GamePacket *Disconnect = new GamePacket();
	Disconnect->Insert(GamePacketTag::NET_CLIENT_REQUEST, ClientRequest::DISCONNECT);
	NetManager->Push(Disconnect);

	bool Ready = false;
	do
	{
		auto Packets = NetManager->Pull();
		for (auto &Packet : Packets)
		{
			for (auto &[Tag, Object] : Packet->Tags)
			{
				if (Tag == GamePacketTag::NET_SERVER_RESPONSE and std::any_cast<ServerResponse>(Object) == ServerResponse::DISCONNECT)
				{
					Ready = true;
				}
			}
		}

	} while (not Ready);

	delete NetManager;
	return 0;
}