#pragma once
#define AL_LIBTYPE_STATIC

#include <vector>
#include "flac_audio_decoder.hpp"
#include "openal/al.h"
#include "sound_data.hpp"

/// <summary> Stores sounds </summary>
class SoundObject
{
  private:
	/// <summary> Generates OpenAl Soft buffers with data from soundData </summary>
	void GenerateBuffers();
	uint8_t *GetArray8(std::vector<int> val);
	int16_t *GetArray16(std::vector<int> val);

  protected:
	/// <summary> vector of ids of sound buffers in OpenAL Soft context </summary>
	std::vector<ALuint> buffers;
	/// <summary>
	/// Sound from file as a vector of SoundData structs
	/// </summary>
	std::vector<SoundData> soundData;

  public:
	using Convertible = std::true_type;
	using ParamT = std::vector<SoundData>;

	SoundObject();
	SoundObject(ParamT v_);

	/// <summary> Adds buffer id to queue </summary>
	/// <param name="buffer_"> buffer id </param>
	void addBuffer(ALuint buffer_);

	/// <summary> Adds prepared data of single frame to queue </summary>
	/// <param name="frameData"> frame data </param>
	void addFrame(const SoundData &frameData);

	/// <summary> gets vector of buffer id </summary>
	/// <remarks> If vector of id is empty, buffers are generated. </remarks>
	std::vector<ALuint> getSound();

	ParamT Convert() const;
};
