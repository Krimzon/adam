#pragma once

#include "share/compat.h"
#include "sound_data.hpp"
#include "sound_object.hpp"
#include "flac++/decoder.h"

class SoundObject;

/// <summary> Decodes audio data from .flac files </summary>
class FLACAudioDecoder : public FLAC::Decoder::File
{
  private:
	FLACAudioDecoder(const FLACAudioDecoder &);
	// FLACAudioDecoder &operator=(const FLACAudioDecoder &);
	int _format{};
	SoundObject *soundObject{};

	std::vector<int> Get16Stereo(const FLAC__int32 *const *buffer, int samples);
	std::vector<int> Get8Stereo(const FLAC__int32 *const buffer[], int samples);
	std::vector<int> Get8StereoFrom12(const FLAC__int32 *const buffer[], int samples);
	std::vector<int> Get16StereoFrom20(const FLAC__int32 *const *buffer, uint32_t samples);
	std::vector<int> Get16StereoFrom24(const FLAC__int32 *const *buffer, uint32_t samples);

	std::vector<int> Get16Mono(const FLAC__int32 *const buffer[], int samples);
	std::vector<int> Get8Mono(const FLAC__int32 *const buffer[], int samples);
	std::vector<int> Get8MonoFrom12(const FLAC__int32 *const buffer[], int samples);
	std::vector<int> Get16MonoFrom20(const FLAC__int32 *const *buffer, uint32_t samples);
	std::vector<int> Get16MonoFrom24(const FLAC__int32 *const *buffer, uint32_t samples);

  protected:
	int sample_rate{};

	::FLAC__StreamDecoderWriteStatus write_callback(const ::FLAC__Frame *frame, const FLAC__int32 *const buffer[]) override;
	void metadata_callback(const ::FLAC__StreamMetadata *metadata) override;
	void error_callback(::FLAC__StreamDecoderErrorStatus status) override;

  public:
	/// <summary> constructor </summary>
	explicit FLACAudioDecoder() : FLAC::Decoder::File(){};

	/// <summary> Sets <see cref="SoundObject">sound object</see> to which sound data will be decoded </summary>
	/// <param name="soundObject_"> <see cref="SoundObject">sound object</see> to which sound data will be decoded </param>
	void ConnectSoundObject(SoundObject *soundObject_);
};
