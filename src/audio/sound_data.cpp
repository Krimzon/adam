#include "sound_data.hpp"

SoundData::SoundData()
{
	type = 0;
	sampleCount = 0;
	frequencyValue = 0;
}

SoundData::SoundData(const ParamT &Param)
{
	this->type = std::get<0>(Param);
	this->sampleCount = std::get<1>(Param);
	this->frequencyValue = std::get<2>(Param);
	this->soundValues = std::get<3>(Param);
}

SoundData::ParamT SoundData::Convert() const
{
	return {this->type, this->sampleCount, this->frequencyValue, this->soundValues};
}
