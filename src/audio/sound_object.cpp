#include <iostream>
#include <utility>
#include "sound_object.hpp"

void SoundObject::addBuffer(ALuint buffer_)
{
	this->buffers.push_back(buffer_);
}
std::vector<ALuint> SoundObject::getSound()
{
	if (this->buffers.empty())
	{
		GenerateBuffers();
	}
	std::cerr << this->buffers.size() << " " << this->soundData.size();
	return this->buffers;
}
void SoundObject::addFrame(const SoundData &frameData)
{
	if (soundData.empty())
	{
		soundData.push_back(frameData);
		return;
	}
	soundData.push_back(frameData);
}
void SoundObject::GenerateBuffers()
{
	ALuint buf;
	uint8_t *buf8;
	int16_t *buf16;
	int s = this->soundData.size();
	for (int i = 0; i < s; i++)
	{
		alGenBuffers(ALuint(1), &buf);
		alGetError();
		switch (soundData[i].type)
		{
			case AL_FORMAT_MONO8:
			case AL_FORMAT_STEREO8:
				buf8 = GetArray8(soundData[i].soundValues);
				alBufferData(buf, soundData[i].type, buf8, soundData[i].sampleCount, soundData[i].frequencyValue);
				alGetError();
				break;
			case AL_FORMAT_MONO16:
			case AL_FORMAT_STEREO16:
				buf16 = GetArray16(soundData[i].soundValues);
				alBufferData(buf, soundData[i].type, buf16, soundData[i].sampleCount, soundData[i].frequencyValue);
				alGetError();
				break;
		}
		this->buffers.push_back(buf);
	}
}
uint8_t *SoundObject::GetArray8(std::vector<int> val)
{
	auto *res = new uint8_t[val.size()];
	for (int i = 0; i < val.size(); i++)
	{
		res[i] = val[i];
	}
	return res;
}
int16_t *SoundObject::GetArray16(std::vector<int> val)
{
	auto *res = new int16_t[val.size()];
	for (int i = 0; i < val.size(); i++)
	{
		res[i] = val[i];
	}
	return res;
}
SoundObject::SoundObject() : soundData(std::vector<SoundData>())
{}
SoundObject::SoundObject(SoundObject::ParamT _v)
{
	this->soundData = std::move(_v);
}
SoundObject::ParamT SoundObject::Convert() const
{
	return this->soundData;
}
