#pragma once
#define AL_LIBTYPE_STATIC

#include <algorithm>
#include <iterator>
#include "openal/al.h"
#include "openal/alc.h"
#include "sound_object.hpp"

/// <summary> initializes OpeoAL-Soft audio context -> used in sample </summary>
void initialize_context();

/// <summary> sets listener parameters -> used in sample </summary>
/// <param name="listener_position"> array of 3 floats </param>
/// <param name="listener_velocity"> array of 3 floats </param>
/// <param name="listener_orientation"> array of 6 floats </param>
void set_listener(float* listener_position, float* listener_velocity, float* listener_orientation);

/// <summary> Source of sound in the audio context </summary>
class AudioObject
{
  private:
  	/// <value> default value for parameters </value>
	constexpr static float defaultArray[3] = {0, 0, 0};

  protected:
	/// <value> float - pitch </value>
	float pitch;
	/// <value id="AudioObject.pitch"> float - gain </value>
	float gain;
	/// <value> array of 3 floats - position </value>
	float position[3];
	/// <value> array of 3 floats - velocity </value>
	float velocity[3];
	/// <value> bool - decides if the sound is looping </value>
	bool looping;
	/// <value> ALuint - Id of the sound source in the OpenAL Soft context </value>
	ALuint objectId;

  public:
	/// <summary>constructor</summary>
	/// <param name="pitch"> audio source pitch </param>
	/// <param name="gain"> audio source gain </param>
	/// <param name="position"> audio source position </param>
	/// <param name="velocity"> audio source velocity </param>
	/// <param name="looping"> set true if audio source should loop sound </param>
	AudioObject(float pitch = 1, float gain = 1, const float position[3] = defaultArray, const float velocity[3] = defaultArray, bool looping = false);

	/// <summary> Add sound to queue </summary>
	/// <param name="soundSource"> <see cref=SoundObject/> </param>
	void QueueBuffers(SoundObject *soundSource);

	/// <summary> Start planing sound </summary>
	void Play();

	/// <summary> Get state of sound source </summary>
	ALint GetState();

	/// <summary> Get number of processed buffers in queue </summary>
	ALint GetBuffersProcessed();
};
