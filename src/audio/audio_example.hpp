#pragma once

#define AL_LIBTYPE_STATIC

#include <array>
#include <cstdio>
#include <iostream>
#include "audio_object.hpp"
#include "openal/al.h"
#include "openal/alc.h"
#include "sound_loader.hpp"

/// <summary> launches audio test </summary>
void audio_sample_test();

/// <summary> gets new position of listener -> used in sample </summary>
/// <param name="listener_position"> array of 3 floats - current listener position </param>
/// <param name"listener_velocity"> array of 3 floats - velocity of listener - does not change velocity of listener </param>
/// <return> array of 3 floats - new position </return>
float *newPos(float *listener_position, float *listener_velocity);