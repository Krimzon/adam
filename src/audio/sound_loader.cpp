#include <iostream>
#include "sound_loader.hpp"
void SoundLoader::LoadSound(SoundObject *soundObject, const char *f_)
{
	auto *flacAudioDecoder = new FLACAudioDecoder();
	flacAudioDecoder->ConnectSoundObject(soundObject);
	(void)flacAudioDecoder->set_md5_checking(true);
	FLAC__StreamDecoderInitStatus init_status = flacAudioDecoder->init(f_);
	if (init_status != FLAC__STREAM_DECODER_INIT_STATUS_OK)
	{
		throw(std::exception("ERROR: initializing decoder"));
	}
	auto val = flacAudioDecoder->process_until_end_of_stream();
	if (!val)
	{
		throw std::runtime_error("Error processing audio");
	}
}
