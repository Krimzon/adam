#pragma once

#include <tuple>
#include <vector>

/// <summary> struct that stores sound data from a single frame of .flac file </summary>
struct SoundData
{
	using Convertible = std::true_type;
	using ParamT = std::tuple<unsigned int, unsigned int, int, std::vector<int>>;

	/// <summary>
	/// Format of sound data.</br>
	/// <see href="https://www.openal.org/documentation/openal-1.1-specification.pdf#page=48">formats</see>
	/// </summary>
	unsigned int type;
	/// <summary> number of samples </summary>
	unsigned int sampleCount;
	/// <summary> frequency </summary>
	int frequencyValue;
	/// <summary> values of samples </summary>
	std::vector<int> soundValues;

	SoundData();
	SoundData(const ParamT &Param);

	ParamT Convert() const;
};