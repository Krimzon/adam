#pragma once
#include "flac_audio_decoder.hpp"
#include "sound_object.hpp"

/// <summary> Loads sound from file to SoundObject </summary>
class SoundLoader
{
  public:
	/// <summary> Loads sound from file to SoundObject </summary>
	/// <param name="soundObject"> where to load sound into </param>
	/// <param name="f_"> filename </param>
	void LoadSound(SoundObject *soundObject, const char *f_);
};
