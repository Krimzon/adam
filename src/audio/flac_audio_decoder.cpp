#include <iostream>
#include "flac_audio_decoder.hpp"

::FLAC__StreamDecoderWriteStatus FLACAudioDecoder::write_callback(const ::FLAC__Frame *frame, const FLAC__int32 *const buffer[])
{
	const auto total_size = (FLAC__uint32)(frame->header.blocksize * frame->header.channels * (frame->header.bits_per_sample / 8));
	std::vector<int> buf;
	SoundData frameData;

	if (total_size == 0)
	{
		return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
	}

	if (frame->header.channels == 1)
	{
		switch (frame->header.bits_per_sample)
		{
			case 8:
				buf = Get8Mono(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_MONO8;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = total_size;
				frameData.soundValues = buf;
				break;
			case 12:
				buf = Get8MonoFrom12(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_MONO8;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = frame->header.blocksize;
				frameData.soundValues = buf;
				break;
			case 16:
				buf = Get16Mono(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_MONO16;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = total_size;
				frameData.soundValues = buf;
				break;
			case 20:
				buf = Get16MonoFrom20(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_MONO16;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = frame->header.blocksize * 2;
				frameData.soundValues = buf;
				break;
			case 24:
				buf = Get16MonoFrom24(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_MONO16;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = frame->header.blocksize * 2;
				frameData.soundValues = buf;
				break;
			default:
				return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
		}
	}
	else if (frame->header.channels == 2)
	{
		switch (frame->header.bits_per_sample)
		{
			case 8:
				buf = Get8Stereo(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_STEREO8;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = total_size;
				frameData.soundValues = buf;
				break;
			case 12:
				buf = Get8StereoFrom12(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_STEREO8;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = frame->header.blocksize * 2;
				frameData.soundValues = buf;
				break;
			case 16:
				buf = Get16Stereo(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_STEREO16;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = total_size;
				frameData.soundValues = buf;
				break;
			case 20:
				buf = Get16StereoFrom20(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_STEREO16;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = frame->header.blocksize * 4;
				frameData.soundValues = buf;
				break;
			case 24:
				buf = Get16StereoFrom24(buffer, frame->header.blocksize);
				frameData.type = AL_FORMAT_STEREO16;
				frameData.frequencyValue = sample_rate;
				frameData.sampleCount = frame->header.blocksize * 4;
				frameData.soundValues = buf;
				break;
			default:
				return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
		}
	}
	else
	{
		return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
	}

	soundObject->addFrame(frameData);

	return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}
void FLACAudioDecoder::metadata_callback(const ::FLAC__StreamMetadata *metadata)
{
	Stream::metadata_callback(metadata);
	this->sample_rate = metadata->data.stream_info.sample_rate;
}
void FLACAudioDecoder::error_callback(::FLAC__StreamDecoderErrorStatus status)
{}
void FLACAudioDecoder::ConnectSoundObject(SoundObject *soundObject_)
{
	this->soundObject = soundObject_;
}
std::vector<int> FLACAudioDecoder::Get16Stereo(const FLAC__int32 *const *buffer, int samples)
{
	std::vector<int> res;
	res.reserve(samples * 2);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i]);
		res.push_back(buffer[1][i]);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get8Stereo(const FLAC__int32 *const *buffer, int samples)
{
	std::vector<int> res;
	res.reserve(samples * 2);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i] + 128);
		res.push_back(buffer[1][i] + 128);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get8StereoFrom12(const FLAC__int32 *const *buffer, int samples)
{
	std::vector<int> res;
	res.reserve(samples * 2);
	for (int i = 0; i < samples; i++)
	{
		res.push_back((buffer[0][i] >> 4) + 128);
		res.push_back((buffer[1][i] >> 4) + 128);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get16StereoFrom20(const FLAC__int32 *const *buffer, uint32_t samples)
{
	std::vector<int> res;
	res.reserve(samples * 2);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i] >> 4);
		res.push_back(buffer[1][i] >> 4);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get16StereoFrom24(const FLAC__int32 *const *buffer, uint32_t samples)
{
	std::vector<int> res;
	res.reserve(samples * 2);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i] >> 8);
		res.push_back(buffer[1][i] >> 8);
	}
	return res;
}

std::vector<int> FLACAudioDecoder::Get16Mono(const FLAC__int32 *const *buffer, int samples)
{
	std::vector<int> res;
	res.reserve(samples);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i]);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get8Mono(const FLAC__int32 *const *buffer, int samples)
{
	std::vector<int> res;
	res.reserve(samples);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i] + 128);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get8MonoFrom12(const FLAC__int32 *const *buffer, int samples)
{
	std::vector<int> res;
	res.reserve(samples);
	for (int i = 0; i < samples; i++)
	{
		res.push_back((buffer[0][i] >> 4) + 128);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get16MonoFrom20(const FLAC__int32 *const *buffer, uint32_t samples)
{
	std::vector<int> res;
	res.reserve(samples);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i] >> 4);
	}
	return res;
}
std::vector<int> FLACAudioDecoder::Get16MonoFrom24(const FLAC__int32 *const *buffer, uint32_t samples)
{
	std::vector<int> res;
	res.reserve(samples);
	for (int i = 0; i < samples; i++)
	{
		res.push_back(buffer[0][i] >> 8);
	}
	return res;
}