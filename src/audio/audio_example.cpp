#include "audio_example.hpp"

void audio_sample_test()
{
	std::cout << "audio sample begin" << std::endl;

	initialize_context();

	float listenerOri[] = {.0f, .0f, 1.f, .0f, 1.f, .0f};
	auto *listenerPos = new float[3]{0, 0, 4};
	float listenerVel[] = {0.02, 0.005, -0.02};

	set_listener(listenerPos, listenerVel, listenerOri);
	auto *sampleSound = new SoundObject();
	auto *loader = new SoundLoader();
	auto *sampleSource = new AudioObject();
	char sampleFile[] = "../../datasrc/audio/sample3mono.flac";
	loader->LoadSound(sampleSound, sampleFile);
	sampleSource->QueueBuffers(sampleSound);
	sampleSource->Play();
	ALint state = sampleSource->GetState();
	int buffers_processed;
	while (AL_PLAYING == state)
	{
		buffers_processed = sampleSource->GetBuffersProcessed();
		std::cout << buffers_processed << std::endl;
		Sleep(200);
		state = sampleSource->GetState();
		listenerPos = newPos(listenerPos, listenerVel);
		alListener3f(AL_POSITION, listenerPos[0], listenerPos[1], listenerPos[2]);
	}

	std::cout << "audio sample end" << std::endl;
}

float *newPos(float *listener_position, float *listener_velocity)
{
	for (int i = 0; i < 3; i++)
	{
		listener_position[i] += listener_velocity[i];
	}
	return listener_position;
}
