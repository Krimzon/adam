#include <iostream>
#include "audio_object.hpp"

void initialize_context()
{
	ALCdevice* device;
	device = alcOpenDevice(NULL);
	if (!device)
	{
		throw(std::exception("Missing audio device"));
	}
	ALCcontext* context;
	context = alcCreateContext(device, NULL);
	if (!alcMakeContextCurrent(context))
	{
		throw(std::exception("Failed to make current context"));
	}
}

void set_listener(float* listener_position, float* listener_velocity, float* listener_orientation)
{
	alListener3f(AL_POSITION, listener_position[0], listener_position[1], listener_position[2]);
	alListener3f(AL_VELOCITY, listener_velocity[0], listener_velocity[1], listener_velocity[2]);
	alListenerfv(AL_ORIENTATION, listener_orientation);
}

AudioObject::AudioObject(float pitch, float gain, const float position[3], const float velocity[3], bool looping)
{
	this->pitch = pitch;
	this->gain = gain;
	this->looping = looping;
	for (int i = 0; i < 3; i++)
	{
		this->velocity[i] = velocity[i];
		this->position[i] = position[i];
	}
	alGenSources((ALuint)1, &(this->objectId));
	alSourcef(this->objectId, AL_PITCH, this->pitch);
	alSourcef(this->objectId, AL_GAIN, this->gain);
	alSourcefv(this->objectId, AL_POSITION, this->position);
	alSourcefv(this->objectId, AL_VELOCITY, this->velocity);
	alSourcei(this->objectId, AL_LOOPING, this->looping ? AL_TRUE : AL_FALSE);
}
void AudioObject::QueueBuffers(SoundObject *soundSource)
{
	std::vector<ALuint> buffers = soundSource->getSound();
	for (auto buf : buffers)
	{
		alSourceQueueBuffers(this->objectId, 1, &buf);
	}
}
void AudioObject::Play()
{
	alSourcePlay(this->objectId);
}
ALint AudioObject::GetState()
{
	ALint state;
	alGetSourcei(this->objectId, AL_SOURCE_STATE, &state);
	return state;
}
ALint AudioObject::GetBuffersProcessed()
{
	ALint buff;
	alGetSourcei(this->objectId, AL_BUFFERS_PROCESSED, &buff);
	return buff;
}
