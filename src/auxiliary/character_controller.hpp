#pragma once
#define BT_USE_DOUBLE_PRECISION
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

/// <summary> Controller for easier management of player movements </summary>
class CharacterController : public btActionInterface
{
  public:
	CharacterController(btRigidBody *body, const btCapsuleShape *shape);
	virtual ~CharacterController() = default;

	/// <summary> Implementation of updateAction from btActionInterFace, used by DynamicWorld </summary>
	void updateAction(btCollisionWorld *collisionWorld, btScalar deltaTimeStep);
	/// <summary> Required by Bullet </summary>
	void debugDraw(btIDebugDraw *debugDrawer);
	/// <summary> Method to set direction of player and make him move </summary>
	void setMovementDirection(const btVector3 &walkDirection);
	/// <summary> Returns the current direction of movement </summary>
	const btVector3 &getMovementDirection() const;
	/// <summary> Resets movement </summary>
	void resetStatus();
	/// <summary> Returns if player is on ground and can jump </summary>
	bool canJump() const;
	/// <summary> Makes the player jump </summary>
	/// <param name="dir"> direction of jump </param>
	void jump(const btVector3 &dir = btVector3(0, 0, 0));
	/// <summary> Returns btRigidBody of player, used to simulate physics </summary>
	const btRigidBody *getBody() const;
	/// <summary> The maximum velocity of player </summary>
	btScalar mMaxLinearVelocity2 = pow(15 / 3.6, 2);
	/// <summary> Acceleration of player </summary>
	btScalar mWalkAccel = 25.0;
	/// <summary> Speed of jumping </summary>
	btScalar mJumpSpeed = 4.5;
	/// <summary> The factor of dampening the speed </summary>
	btScalar mSpeedDamping = 0.1;
	/// <summary> Radius of the shape </summary>
	btScalar mShapeRadius;
	/// <summary> Half of the player shape height </summary>
	btScalar mShapeHalfHeight;
	/// <summary> Vector of gravity </summary>
	btVector3 mGravity;
	/// <summary> The max value of cos of angle between player and the ground </summary>
	btScalar mMaxCosGround = -SIMDSQRT12;
	/// <summary> Threshold of detecting the ground </summary>
	btScalar mRadiusThreshold = 1e-2;
	/// <summary> btRigidBody of player, used to simulate physics </summary>
	btRigidBody* mRigidBody;

  protected:
	CharacterController();
	/// <summary> Setups all the internal functions </summary>
	void setupBody();
	/// <summary> Updates the velocity after the time dt </summary>
	void updateVelocity(float dt);
	/// <summary> Vector of movement direction </summary>
	btVector3 mMoveDirection;
	/// <summary> Flag tells if the player is jumping </summary>
	bool mJump;
	/// <summary> Vector of direction of the jump </summary>
	btVector3 mJumpDir;
	/// <summary> Flag telling if the player is on ground </summary>
	bool mOnGround = false;
	/// <summary> Point of collision with the ground </summary>
	btVector3 mGroundPoint;
};
