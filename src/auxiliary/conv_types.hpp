#pragma once

#include <array>
#include <vector>

/// <summary> X, Y, Z of point in space </summary>
using point = std::array<double, 3>;

/// <summary> indexes of vertexes of the face </summary>
using face = std::vector<int32_t>;

/// <summary> 4 points </summary>
using tetrahedra = std::array<point, 4>;
