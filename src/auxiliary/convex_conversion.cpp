#include "convex_conversion.hpp"

namespace
{
std::vector<point> test_vertexes{{.0, .0, .0}, {2., .0, .0}, {2., 2., 0.}, {.0, 2., .0}, {.0, .0, 2.}, {3., 0., 5.}, {3., 4., 4.}, {.0, 3., 2.}};
std::vector<face> test_faces{{0, 1, 2, 3}, {4, 5, 6, 7}, {0, 4, 5, 1}, {1, 5, 6, 2}, {2, 6, 7, 3}, {3, 7, 4, 0}};
std::vector<uint32_t> test_model_indices{0, 1, 2, 2, 3, 0, 1, 5, 6, 6, 2, 1, 7, 6, 5, 5, 4, 7, 4, 0, 3, 3, 7, 4, 4, 5, 1, 1, 0, 4, 3, 2, 6, 6, 7, 3};
void convDecTest()
{
	std::vector<tetrahedra> res = ConvDec(test_faces, test_vertexes);
	/*
	for (tetrahedra tet : res)
	{
		for (point pt : tet)
		{
			for (double var : pt)
			{
				std::cout << " " << var;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
	 /**/
	std::cout << "==========================================" << std::endl;
	std::vector<Vertex> model_test_vertex;

	Vertex v{};
	for (point p : test_vertexes)
	{
		v.Position.x = p[0];
		v.Position.y = p[1];
		v.Position.z = p[2];
		model_test_vertex.push_back(v);
	}
	Model *test_model = new Model(model_test_vertex, test_model_indices);
	res = ConvDec(*test_model);
	/*
	for (tetrahedra tet : res)
	{
		for (point pt : tet)
		{
			for (double var : pt)
			{
				std::cout << " " << var;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
	 /**/
}

void tetrahedraGen(tetgenio *in, tetgenio *out, const std::vector<face> &faces, const std::vector<point> &vertexes)
{
	tetgenio::facet *f;
	tetgenio::polygon *p;
	int i;

	in->firstnumber = 0;
	in->numberofpoints = vertexes.size();
	in->pointlist = new REAL[in->numberofpoints * 3];
	for (i = 0; i < in->numberofpoints; i++)
	{
		in->pointlist[3 * i] = vertexes[i][0];
		in->pointlist[3 * i + 1] = vertexes[i][1];
		in->pointlist[3 * i + 2] = vertexes[i][2];
	}

	in->numberoffacets = faces.size();
	in->facetlist = new tetgenio::facet[in->numberoffacets];
	in->facetmarkerlist = new int[in->numberoffacets];
	for (i = 0; i < in->numberoffacets; i++)
	{
		f = &in->facetlist[i];
		f->numberofpolygons = 1;
		f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
		f->numberofholes = 0;
		f->holelist = nullptr;
		p = &f->polygonlist[0];
		p->numberofvertices = faces[i].size();
		p->vertexlist = new int[p->numberofvertices];
		std::copy(faces[i].begin(), faces[i].end(), p->vertexlist);
	}

	for (i = 0; i < in->numberoffacets; i++)
	{
		in->facetmarkerlist[i] = 0;
	}

	tetgenbehavior tetgenbehavior;
	tetgenbehavior.parse_commandline((char *)"pq2.0a1.5");
	tetrahedralize(&tetgenbehavior, in, out);
}

std::vector<tetrahedra> getDataVector(tetgenio *out)
{
	std::vector<tetrahedra> vector;
	tetrahedra t;
	point pt;
	for (int i = 0; i < out->numberoftetrahedra; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			pt[0] = out->pointlist[out->tetrahedronlist[i * 4 + j]];
			pt[1] = out->pointlist[out->tetrahedronlist[i * 4 + j] + 1];
			pt[2] = out->pointlist[out->tetrahedronlist[i * 4 + j] + 2];
			t[j] = pt;
		}
		vector.push_back(t);
	}
	return vector;
}
} // namespace

std::vector<tetrahedra> ConvDec(const std::vector<face> &faces, const std::vector<point> &vertices)
{
	tetgenio in, out;
	int i;

	tetrahedraGen(&in, &out, faces, vertices);

	return getDataVector(&out);
}

void TestConvDec()
{
	// printing test data
	std::cout << "Test points:" << std::endl;
	for (int i = 0; i < 8; i++)
	{
		std::cout << i << ":";
		for (int j = 0; j < 3; j++)
		{
			std::cout << " " << test_vertexes[i][j];
		}
		std::cout << std::endl;
	}
	std::cout << std::endl << "Test faces:" << std::endl;
	for (int i = 0; i < 6; i++)
	{
		std::cout << i << ":";
		for (int j = 0; j < 4; j++)
		{
			std::cout << " " << test_faces[i][j];
		}
		std::cout << std::endl;
	}
	convDecTest();
}

std::vector<tetrahedra> ConvDec(const Model &model)
{
	std::vector<face> faces;
	std::vector<point> points;
	face f;
	for (int i = 0; i < model.Indices.size(); i += 3)
	{
		f.push_back(model.Indices.at(i));
		f.push_back(model.Indices.at(i + 1));
		f.push_back(model.Indices.at(i + 2));
		faces.push_back(f);
		f.clear();
	}
	point p;
	for (Vertex v : model.Vertices)
	{
		p[0] = v.Position.x;
		p[1] = v.Position.y;
		p[2] = v.Position.z;
		points.push_back(p);
	}
	return ConvDec(faces, points);
}
