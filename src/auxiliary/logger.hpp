#pragma once

#include <format>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include "magic_enum/magic_enum.hpp"

enum class LogModule
{
	CLIENT_NET,
	CLIENT_RENDER,
	SERVER_NET,
	SERVER_MAIN,
	PATCHER,
};

enum class LogSeverity
{
	INFO,
	WARNING,
	ERR,
};

static void Log(LogModule Module, LogSeverity Severity, std::string Message)
{
	std::cout << std::format("[{:x}][{}][{}]\t{}\n", std::hash<std::thread::id>{}(std::this_thread::get_id()), magic_enum::enum_name(Module), magic_enum::enum_name(Severity), Message);
}