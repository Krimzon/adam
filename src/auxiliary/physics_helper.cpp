#include "physics_helper.hpp"

btVector3 std2bt(const point p)
{
	return {p[0], p[1], p[2]};
}

glm::mat4 btScalar2mat4(btScalar *matrix)
{
	return glm::mat4(matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], matrix[6], matrix[7], matrix[8], matrix[9], matrix[10], matrix[11], matrix[12], matrix[13], matrix[14], matrix[15]);
}

std::vector<double> btScalar2double(btScalar *matrix)
{
	return {matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], matrix[6], matrix[7], matrix[8], matrix[9], matrix[10], matrix[11], matrix[12], matrix[13], matrix[14], matrix[15]};
}

Simulation::Simulation()
{
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	overlappingPairCache = new btDbvtBroadphase();
	overlappingPairCache->resetPool(dispatcher);
	solver = new btSequentialImpulseConstraintSolver;

	dynamicsWorldMt = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
	dynamicsWorldMt->setGravity(btVector3(0, -10, 0));
}

Simulation::~Simulation()
{
	for (auto i = dynamicsWorldMt->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject *obj = dynamicsWorldMt->getCollisionObjectArray()[i];
		btRigidBody *body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		dynamicsWorldMt->removeCollisionObject(obj);
		delete obj;
	}
	delete dynamicsWorldMt;
	delete overlappingPairCache;
	delete dispatcher;
	delete collisionConfiguration;
}

btDynamicsWorld *Simulation::GetCollisionWorld()
{
	return dynamicsWorldMt;
}

ServerDelta::ServerDelta()
{}

ServerDelta::ServerDelta(ParamT &in)
{
	cams = std::get<0>(in);
	pos = std::get<1>(in);
}

ServerDelta::ParamT ServerDelta::Convert() const
{
	std::tuple tmp{cams, pos};
	return tmp;
}
