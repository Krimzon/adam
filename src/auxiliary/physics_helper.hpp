#pragma once
#define BT_USE_DOUBLE_PRECISION

#include <map>
#include "../render/freecam.hpp"
#include "BulletCollision/CollisionDispatch/btCollisionDispatcherMt.h"
#include "BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolverMt.h"
#include "BulletDynamics/Dynamics/btDiscreteDynamicsWorldMt.h"
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "conv_types.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/rotate_vector.hpp"

/// <summary> Internal network protocol struct to send updates to user </summary>
struct ServerDelta
{
	using Convertible = std::true_type;
	using ParamT = std::tuple<std::map<int, FreeCam>, std::map<int, std::vector<double>>>;
	ServerDelta();
	ServerDelta(ParamT &in);

	std::map<int, FreeCam> cams;
	std::map<int, std::vector<double>> pos;
	ParamT Convert() const;
};

btVector3 std2bt(const point p);

glm::mat4 btScalar2mat4(btScalar *matrix);

std::vector<double> btScalar2double(btScalar *matrix);

class Simulation
{
	btDefaultCollisionConfiguration *collisionConfiguration;
	btBroadphaseInterface *overlappingPairCache;
	btCollisionDispatcher *dispatcher;
	btSequentialImpulseConstraintSolver *solver;
	btDynamicsWorld *dynamicsWorldMt;

  public:
	Simulation();
	~Simulation();
	btDynamicsWorld *GetCollisionWorld();
};