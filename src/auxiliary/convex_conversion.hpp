#pragma once

#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>
#include "../render/structures.hpp"
#include "conv_types.hpp"
#include "tetgen/tetgen.h"

/// <summary> Converts faces and vertices </summary>
/// <param name="faces"> := Vector of faces </param>
/// <param name="vertices"> := Vector of vertices </param>
/// @return Vector of tetrahedra
std::vector<tetrahedra> ConvDec(const std::vector<face> &faces, const std::vector<point> &vertices);

/// <summary> Converts Model class into a vector of tetrahedra </summary>
/// <param name="model"> := model class </param>
/// @return Vector of tetrahedra
std::vector<tetrahedra> ConvDec(const Model &model);

/// <summary> Tests if convex converter works </summary>
void TestConvDec();