#include <cassert>
#include "character_controller.hpp"


namespace
{
	struct FindGroundHelp : public btCollisionWorld::ContactResultCallback
	{
	public:
		FindGroundHelp(const CharacterController* controller, const btCollisionWorld* world) : mController(controller), mWorld(world), ContactResultCallback()
		{}

		btScalar addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0, int partId0, int index0, const btCollisionObjectWrapper* colObj1, int partId1, int index1);

		bool mHaveGround = false;
		btVector3 mGroundPoint;

	private:
		void checkGround(const btManifoldPoint& cp);

		const CharacterController* mController;
		const btCollisionWorld* mWorld;
	};

	btScalar FindGroundHelp::addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0, int partId0, int index0, const btCollisionObjectWrapper* colObj1, int partId1, int index1)
	{
		if (colObj1->m_collisionObject == mController->getBody() or colObj0->m_collisionObject == mController->getBody())
		{
			checkGround(cp);
		}

		return 0;
	}

	void FindGroundHelp::checkGround(const btManifoldPoint& cp)
	{
		if (mHaveGround)
		{
			return;
		}

		btTransform inverse = mController->getBody()->getWorldTransform().inverse();
		btVector3 localPoint = inverse(cp.m_positionWorldOnB);
		localPoint[1] += mController->mShapeHalfHeight;

		float r = localPoint.length();
		float cosTheta = localPoint[1] / r;

		if (fabs(r - mController->mShapeRadius) <= mController->mRadiusThreshold && cosTheta < mController->mMaxCosGround)
		{
			mHaveGround = true;
			mGroundPoint = cp.m_positionWorldOnB;
		}
	}
} //namespace

CharacterController::CharacterController(btRigidBody *body, const btCapsuleShape *shape)
{
	mRigidBody = body;

	mShapeRadius = shape->getRadius();
	mShapeHalfHeight = shape->getHalfHeight();

	setupBody();
	resetStatus();
}

CharacterController::CharacterController()
{
	mRigidBody = nullptr;
	resetStatus();
}

void CharacterController::setupBody()
{
	assert(mRigidBody);
	mRigidBody->setSleepingThresholds(0.0, 0.0);
	mRigidBody->setAngularFactor(0.0);
	mGravity = mRigidBody->getGravity();
}

void CharacterController::updateAction(btCollisionWorld *collisionWorld, btScalar deltaTimeStep)
{
	FindGroundHelp groundSteps(this, collisionWorld);
	collisionWorld->contactTest(mRigidBody, groundSteps);
	mOnGround = groundSteps.mHaveGround;
	mGroundPoint = groundSteps.mGroundPoint;

	updateVelocity(deltaTimeStep);
	if (mOnGround)
	{
		mRigidBody->setGravity(btVector3{0, 0, 0});
	}
	else
	{
		mRigidBody->setGravity(mGravity);
	}
}

void CharacterController::updateVelocity(float dt)
{
	btTransform transform;
	mRigidBody->getMotionState()->getWorldTransform(transform);
	btMatrix3x3 &basis = transform.getBasis();
	btMatrix3x3 inv = basis.transpose();

	btVector3 linearVelocity = inv * mRigidBody->getLinearVelocity();

	if (mMoveDirection.fuzzyZero() && mOnGround)
	{
		linearVelocity *= mSpeedDamping;
	}
	else if (mOnGround || linearVelocity[2] > 0)
	{
		btVector3 dv = mMoveDirection * (mWalkAccel * dt);
		linearVelocity += dv;

		btScalar speed2 = pow(linearVelocity.x(), 2) + pow(linearVelocity.z(), 2);
		if (speed2 > mMaxLinearVelocity2)
		{
			btScalar correction = sqrt(mMaxLinearVelocity2 / speed2);
			linearVelocity[0] *= correction;
			linearVelocity[2] *= correction;
		}
	}

	if (mJump)
	{
		linearVelocity += mJumpSpeed * mJumpDir;
		mJump = false;
	}

	mRigidBody->setLinearVelocity(basis * linearVelocity);
}

void CharacterController::debugDraw(btIDebugDraw *debugDrawer)
{
	debugDrawer->drawContactPoint(mGroundPoint, {0, 0, 1}, 0, 1000, {0, 0.3, 1});
}

void CharacterController::setMovementDirection(const btVector3 &walkDirection)
{
	mMoveDirection = walkDirection;
	mMoveDirection.setY(0);
	if (!mMoveDirection.fuzzyZero())
	{
		mMoveDirection.normalize();
	}
}

const btVector3 &CharacterController::getMovementDirection() const
{
	return mMoveDirection;
}

void CharacterController::resetStatus()
{
	mMoveDirection.setValue(0, 0, 0);
	mJump = false;
	mOnGround = false;
}

bool CharacterController::canJump() const
{
	return mOnGround;
}

void CharacterController::jump(const btVector3 &dir)
{
	if (!canJump())
	{
		return;
	}

	mJump = true;

	mJumpDir = dir;
	if (dir.fuzzyZero())
	{
		mJumpDir.setValue(0, 1, 0);
	}
	mJumpDir.normalize();
}

const btRigidBody *CharacterController::getBody() const
{
	return mRigidBody;
}