#version 460
#extension GL_ARB_separate_shader_objects : enable

struct Light
{
	vec4 Position;
	vec4 Color;
};

layout(set = 0, binding = 1) uniform sampler2D texSampler;
layout(set = 0, binding = 2) uniform UniformBufferLight
{
	mat4 NormalMatrix;
	vec4 CameraPosition;
	Light Lights[16];
	uvec4 LightsNum;
} UBL;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec2 fragTexCoord;
layout(location = 3) in vec3 fragCoord;

layout(location = 0) out vec4 outColor;

void main()
{
	vec4 color = vec4(0.0025, 0.0025, 0.0025, 1.0) * texture(texSampler, fragTexCoord);
	vec3 normal = normalize(mat3(UBL.NormalMatrix) * fragNormal);
	vec3 ViewDir = normalize(UBL.CameraPosition.xyz - fragCoord);

	for(int i = 0; i < UBL.LightsNum.x; i++)
	{
		vec3 LightDir = normalize(UBL.Lights[i].Position.xyz - fragCoord);  
		vec3 ReflectDir = reflect(-LightDir, normal);
		float Dist = distance(fragCoord, UBL.Lights[i].Position.xyz);
	
		float diff = max(dot(normal, LightDir), 0.0);
		float spec = pow(max(dot(ViewDir, ReflectDir), 0.0), 32);

		vec3 Diffuse = UBL.Lights[i].Color.xyz * diff * (UBL.Lights[i].Color.w / Dist) * (UBL.Lights[i].Color.w / Dist);
		vec3 Specular = 0.5 * spec * UBL.Lights[i].Color.xyz * (UBL.Lights[i].Color.w / Dist) * (UBL.Lights[i].Color.w / Dist);

		color += vec4(Diffuse + Specular, 1.0) * texture(texSampler, fragTexCoord);
	}
	
	outColor = color * vec4(fragColor, 1.0);
}