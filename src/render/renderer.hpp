#pragma once

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLFW_INCLUDE_VULKAN

#include <iostream>
#include <map>
#include <optional>
#include <queue>
#include <set>
#include <vector>
#include "camera.hpp"
#include "glfw/glfw3.h"
#include "shader.hpp"
#include "structures.hpp"
#include "vulkan/vulkan.hpp"

class Renderer
{
  private:
	GLFWwindow *Window{};

	vk::Instance Instance{};
	vk::DebugUtilsMessengerEXT DebugMessenger{};
	vk::SurfaceKHR Surface{};
	vk::PhysicalDevice PhysicalDevice{};
	vk::Device Device{};
	vk::Queue PresentQueue{};
	vk::Queue GraphicsQueue{};
	vk::SwapchainKHR SwapChain{};
	vk::Format SwapChainImageFormat{};
	vk::Extent2D SwapChainExtent{};
	vk::RenderPass RenderPass{};
	vk::DescriptorSetLayout DescriptorSetLayout{};
	vk::PipelineLayout PipelineLayout{};
	vk::Pipeline Pipeline{};
	vk::CommandPool CommandPool{};
	vk::DescriptorPool DescriptorPool{};
	vk::Buffer VertexBuffer{};
	vk::Buffer IndexBuffer{};
	vk::Image DepthImage{};
	vk::ImageView DepthImageView{};
	vk::Sampler TextureSampler{};
	vk::DeviceMemory VertexBufferMemory{};
	vk::DeviceMemory IndexBufferMemory{};
	vk::DeviceMemory DepthImageMemory{};
	vk::Buffer BLASBuffer{};
	vk::DeviceMemory BLASMemory{};
	vk::AccelerationStructureKHR BLASAccel{};
	vk::Buffer TLASBuffer{};
	vk::DeviceMemory TLASMemory{};
	vk::AccelerationStructureKHR TLASAccel{};
	vk::DescriptorPool RTDescriptorPool{};
	vk::DescriptorSetLayout RTDescriptorSetLayout{};
	vk::Buffer CameraBuffer{};
	vk::DeviceMemory CameraBufferMemory{};
	vk::Buffer TextureMappingBuffer{};
	vk::DeviceMemory TextureMappingBufferMemory{};

	std::vector<vk::Image> SwapChainImages{};
	std::vector<vk::ImageView> SwapChainImageViews{};
	std::vector<vk::Framebuffer> SwapChainFramebuffers{};
	std::vector<vk::CommandBuffer> CommandBuffers{};
	std::vector<vk::Semaphore> ImageAvailableSem{};
	std::vector<vk::Semaphore> RenderFinishedSem{};
	std::vector<vk::Fence> InFlightFences{};
	std::vector<vk::Fence> ImageFences{};
	std::vector<vk::AccelerationStructureInstanceKHR> TLASInstances{};
	std::vector<vk::DescriptorSet> RTDescriptorSets{};
	std::vector<vk::Image> RTOffscreenImages{};
	std::vector<vk::DeviceMemory> RTOffscreenImageMemories{};
	std::vector<vk::ImageView> RTOffscreenImageViews{};

	std::map<uint16_t, Model> Models{};
	std::map<uint16_t, Texture> Textures{};
	std::map<uint16_t, Light> Lights{};
	std::map<uint16_t, Target> Targets{};

	std::map<uint16_t, uint16_t> LightBind{};
	std::vector<Light> LightTemp{};

	std::deque<std::pair<uint16_t, Model>> ModelsCreate{};
	std::deque<std::pair<uint16_t, Texture>> TexturesCreate{};
	std::deque<std::pair<uint16_t, Light>> LightsCreate{};
	std::deque<std::pair<uint16_t, Target>> TargetsCreate{};

	std::deque<uint16_t> ModelsDelete{};
	std::deque<uint16_t> TexturesDelete{};
	std::deque<uint16_t> LightsDelete{};
	std::deque<uint16_t> TargetsDelete{};

	std::multimap<std::string, Shader> Shaders;

	bool PauseDraw = false;
	bool Raytracing = false;

	uint32_t MaxIndexOffset = 0;
	uint32_t VertexBufferSize = 0;
	uint32_t IndexBufferSize = 0;
	uint32_t TextureMappingBufferSize = 0;

	std::vector<const char *> ValidationLayers = {"VK_LAYER_KHRONOS_validation"};
	std::vector<const char *> DeviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

	vk::PhysicalDeviceRayTracingPipelinePropertiesKHR RTProperties{};

	const size_t FramesInFlight = 2;
	size_t CurrentFrame = 0;

	glm::mat4 ProjectionT{};
	ICamera *Camera;

	struct QueueFamilyIndices
	{
		std::optional<uint32_t> PresentFamily{};
		std::optional<uint32_t> GraphicsFamily{};

		bool Complete();
		bool Concurrent();
		std::set<uint32_t> GetUnique();
		std::array<uint32_t, 2> GetConcurrentFamilies();
	};

	struct UniformBufferObject
	{
		alignas(16) glm::mat4 Model;
		alignas(16) glm::mat4 View;
		alignas(16) glm::mat4 Proj;
	};

	struct CameraObject
	{
		alignas(16) glm::mat4 View;
		alignas(16) glm::mat4 Proj;
	};

	struct UniformBufferLights
	{
		glm::mat4 NormalMatrix;
		glm::vec4 CameraPosition;
		Light Lights[16];
		glm::uvec4 LightsNum;
	};

	static vk::Bool32 DebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT Severity, VkDebugUtilsMessageTypeFlagsEXT Type, const VkDebugUtilsMessengerCallbackDataEXT *CallbackData, void *UserData)
	{
		if (Severity > VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
			std::cerr << "Validation layer: " << CallbackData->pMessage << std::endl;
		return VK_FALSE;
	}

	uint32_t FindMemoryType(uint32_t Filter, vk::MemoryPropertyFlags Properties);
	void CreateBuffer(vk::DeviceSize Size, vk::BufferUsageFlags Usage, vk::MemoryPropertyFlags Properties, vk::Buffer &Buffer, vk::DeviceMemory &Memory);
	void CopyBuffer(vk::Buffer Src, vk::Buffer Dst, vk::DeviceSize Size);
	std::vector<const char *> GetRequiredExtensions();
	void CreateInstance();
	void CreateDebugMessenger();
	void CreateSurface();
	void PickPhysicalDevice();
	uint64_t PhysicalDeviceRating(vk::PhysicalDevice &Device);
	QueueFamilyIndices GetQueueFamilies(vk::PhysicalDevice &Device);
	void CreateLogicalDevice();
	void CreateSwapChain();
	void CreateImageViews();
	void CreateRenderPass();
	void CreateDescriptorSetLayout();
	void CreateGraphicsPipeline();
	void CreateFramebuffers();
	void CreateCommandPool();
	void CreateDescriptorPool();
	void CreateCommandBuffers();
	void CreateSyncObjects();
	vk::ImageView CreateImageView(vk::Image Image, vk::Format Format, vk::ImageAspectFlagBits Flags);
	void CleanSwapChain();
	void RebuildSwapChain();
	void UpdateUniformBuffer(uint32_t Image);
	void UpdateSceneDescription();
	void RebuildBuffers();
	void CreateVertexBuffer();
	void CreateIndexBuffer();
	void CreateImage(uint32_t Width, uint32_t Height, vk::Format Format, vk::ImageTiling Tiling, vk::ImageUsageFlags Usage, vk::MemoryPropertyFlags Properties, vk::Image &Image, vk::DeviceMemory &Memory);
	void TransitionImageLayout(vk::Image Image, vk::Format Format, vk::ImageLayout OldLayout, vk::ImageLayout NewLayout);
	void CopyBufferToImage(vk::Buffer Buffer, vk::Image Image, uint32_t Width, uint32_t Height);
	void CreateOffscreenTexture();
	void CreateTextureSampler();
	void CreateUniformBuffers(Target &Target);
	void CreateDescriptorSets(Target &Target);
	void CreateDescriptorSetsRT();
	vk::CommandBuffer BeginSingleTimeCommands();
	void EndSingleTimeCommands(vk::CommandBuffer Buffer);
	void CreateDepthResources();
	void BuildTLAS();
	void CreateModels();
	void CreateTextures();
	void CreateLights();
	void CreateTargets();
	void UpdateLights();
	void DeleteModels();
	void DeleteTextures();
	void DeleteLights();
	void DeleteTargets();

  public:
	Renderer(GLFWwindow *pWindow, bool Raytracing = false, const std::multimap<std::string, Shader> &Shaders = {});
	Renderer(Renderer &other) = delete;
	~Renderer();

	void Draw();
	void CreateModel(uint16_t ID, const Model &Model);
	void CreateTexture(uint16_t ID, const Texture &Texture);
	void CreateLight(uint16_t ID, const Light &Light);
	void CreateTarget(uint16_t ID, Target Target);
	void DeleteModel(uint16_t ID);
	void DeleteTexture(uint16_t ID);
	void DeleteLight(uint16_t ID);
	void DeleteTarget(uint16_t ID);
	Light &UpdateLight(uint16_t ID);
	void UpdateTarget(uint16_t ID, const glm::mat4 &Transform);
	void FlushObjects();
	void BindCamera(ICamera *Camera);
};