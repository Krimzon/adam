#include "freecam.hpp"
#include "glm/gtx/rotate_vector.hpp"

FreeCam::FreeCam()
{
	Pos = glm::vec3(0.0f, 0.0f, 0.0f);
	Rot = glm::vec2(0.0f, 0.0f);
}

FreeCam::FreeCam(const FreeCam::ParamT &Param)
{
	Pos = std::get<0>(Param);
	Rot = std::get<1>(Param);
	Entity = std::get<2>(Param);
}

FreeCam::FreeCam(glm::vec3 Position, glm::vec2 Rotation) : Pos(Position), Rot(Rotation)
{}

void FreeCam::Rotate(const glm::vec2 &Angle)
{
	Rot += Angle;

	if (Rot.x >= glm::pi<float>() / 2)
		Rot.x = glm::pi<float>() / 2 - 0.001;

	if (Rot.x <= -glm::pi<float>() / 2)
		Rot.x = -glm::pi<float>() / 2 + 0.001;
}

void FreeCam::Move(float Distance)
{
	glm::vec3 Pivot = glm::rotate(glm::rotate(glm::vec3(0, 0, 1), Rot.x, glm::vec3(1, 0, 0)), Rot.y, glm::vec3(0, 1, 0));
	Pos += Pivot * Distance;
}

void FreeCam::Strafe(float Distance)
{
	glm::vec3 Pivot = glm::rotate(glm::rotate(glm::vec3(1, 0, 0), Rot.x, glm::vec3(1, 0, 0)), Rot.y, glm::vec3(0, 1, 0));
	Pos += Pivot * Distance;
}

void FreeCam::Ascend(float Distance)
{
	Pos.y += Distance;
}

void FreeCam::SetPosition(const glm::vec3 &Position)
{
	Pos = Position;
}

void FreeCam::SetRotation(const glm::vec3 &Rotation)
{
	Rot = Rotation;
}

glm::vec4 FreeCam::GetPos()
{
	return glm::vec4(Pos, 1.0f);
}

glm::mat4 FreeCam::GetView()
{
	glm::vec3 Pivot = glm::rotate(glm::rotate(glm::vec3(0, 0, 1), Rot.x, glm::vec3(1, 0, 0)), Rot.y, glm::vec3(0, 1, 0));
	return glm::lookAt(Pos, Pos + Pivot, glm::vec3(0.0f, 1.0f, 0.0f));
}

glm::vec3 FreeCam::GetRot()
{
	glm::vec3 Pivot = glm::rotate(glm::rotate(glm::vec3(0, 0, 1), Rot.x, glm::vec3(1, 0, 0)), Rot.y, glm::vec3(0, 1, 0));
	return Pivot;
}

FreeCam::ParamT FreeCam::Convert() const
{
	return {Pos, Rot, Entity.value_or(0)};
}