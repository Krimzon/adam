#pragma once

#include <vector>
#include "shaderc/shaderc.hpp"

struct Shader
{
	using Convertible = std::true_type;
	using ParamT = std::tuple<shaderc_shader_kind, std::vector<uint32_t>>;

	shaderc_shader_kind Stage;
	std::vector<uint32_t> Code;

	Shader();
	Shader(shaderc_shader_kind Stage, const std::vector<uint32_t> &Code);
	Shader(const ParamT &Param);
	ParamT Convert() const;
};