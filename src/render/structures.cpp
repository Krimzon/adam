#include "glm/gtc/type_ptr.hpp"
#include "structures.hpp"

Model::Model()
{
	VertexOffset = 0;
	IndexOffset = 0;
}

Model::Model(const Model::ParamT &Param)
{
	Vertices = std::get<0>(Param);
	Indices = std::get<1>(Param);
}

Model::Model(const std::vector<Vertex> &Vertices, const std::vector<uint32_t> &Indices) : Vertices(Vertices), Indices(Indices)
{
	VertexOffset = 0;
	IndexOffset = 0;
}

Model::ParamT Model::Convert() const
{
	return {Vertices, Indices};
}

Texture::Texture() : Width(0), Height(0)
{}

Texture::Texture(const Texture::ParamT &Param)
{
	Width = std::get<0>(Param);
	Height = std::get<1>(Param);
	Pixels = std::get<2>(Param);
}

Texture::Texture(int32_t Width, int32_t Height) : Width(Width), Height(Height)
{}

Texture::ParamT Texture::Convert() const
{
	return {Width, Height, Pixels};
}

Light::Light()
{
	Position = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	Color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
}

Light::Light(glm::vec3 Position, glm::vec4 Color) : Position(glm::vec4(Position, 1.0f)), Color(Color)
{}

Target::Target()
{
	ModelID = 0;
	TextureID = 0;
	Transform = glm::mat4(1.0f);
}

Target::Target(const Target::ParamT &Params)
{
	ModelID = std::get<0>(Params);
	TextureID = std::get<1>(Params);
	LightID = std::get<2>(Params);
	Transform = glm::mat4();
}

Target::Target(uint16_t ModelID, uint16_t TextureID) : ModelID(ModelID), TextureID(TextureID)
{
	Transform = glm::mat4(1.0f);
}

Target::Target(uint16_t ModelID, uint16_t TextureID, std::vector<uint16_t> LightID) : ModelID(ModelID), TextureID(TextureID), LightID(LightID)
{
	Transform = glm::mat4(1.0f);
}

Target::Target(uint16_t ModelID, uint16_t TextureID, std::vector<uint16_t> LightID, glm::mat4 Transform) : ModelID(ModelID), TextureID(TextureID), LightID(LightID), Transform(Transform)
{}

Target::ParamT Target::Convert() const
{
	return {ModelID, TextureID, LightID};
}