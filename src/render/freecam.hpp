#pragma once

#include <tuple>
#include <optional>
#include "camera.hpp"

/// <summary> Free Cam, can be used instead of <see cref="SoundObject">Character Controller</see></summary>
class FreeCam : public ICamera
{
	glm::vec3 Pos;
	glm::vec2 Rot;

  public:
	std::optional<int> Entity;

	using Convertible = std::true_type;
	using ParamT = std::tuple<glm::vec3, glm::vec2, int>;

	FreeCam();
	FreeCam(const ParamT &Param);
	FreeCam(glm::vec3 Position, glm::vec2 Rotation);

	/// <summary> Rotates the camera </summary>
	void Rotate(const glm::vec2 &Angle);
	/// <summary> Moves the camera </summary>
	void Move(float Distance);
	/// <summary> Strafes the camera </summary>
	void Strafe(float Distance);
	/// <summary> Ascends (or descends) the camera </summary>
	void Ascend(float Distance);
	void SetPosition(const glm::vec3 &Position);
	void SetRotation(const glm::vec3 &Angle);

	glm::vec4 GetPos();
	glm::mat4 GetView();
	glm::vec3 GetRot();

	ParamT Convert() const;
};