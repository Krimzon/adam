#pragma once

#include "glm/gtc/matrix_transform.hpp"

struct ICamera
{
	virtual glm::vec4 GetPos() = 0;
	virtual glm::mat4 GetView() = 0;
};