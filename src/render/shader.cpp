#include "shader.hpp"

Shader::Shader()
{
	Stage = shaderc_shader_kind::shaderc_vertex_shader;
}

Shader::Shader(shaderc_shader_kind Stage, const std::vector<uint32_t> &Code) : Stage(Stage), Code(Code)
{}

Shader::Shader(const Shader::ParamT &Param)
{
	Stage = std::get<0>(Param);
	Code = std::get<1>(Param);
}

Shader::ParamT Shader::Convert() const
{
	return {Stage, Code};
}