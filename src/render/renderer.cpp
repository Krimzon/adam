#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include "glm/gtc/matrix_inverse.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "renderer.hpp"
#include "vertex.hpp"

#ifdef NDEBUG
const bool VulkanDebug = false;
#else
const bool VulkanDebug = true;
#endif

bool Renderer::QueueFamilyIndices::Complete()
{
	return PresentFamily.has_value() and GraphicsFamily.has_value();
}

bool Renderer::QueueFamilyIndices::Concurrent()
{
	return PresentFamily != GraphicsFamily;
}

std::set<uint32_t> Renderer::QueueFamilyIndices::GetUnique()
{
	return std::set<uint32_t>{PresentFamily.value(), GraphicsFamily.value()};
}

std::array<uint32_t, 2> Renderer::QueueFamilyIndices::GetConcurrentFamilies()
{
	return std::array<uint32_t, 2>{GraphicsFamily.value(), PresentFamily.value()};
}

uint32_t Renderer::FindMemoryType(uint32_t Filter, vk::MemoryPropertyFlags Properties)
{
	auto MemoryProperties = PhysicalDevice.getMemoryProperties();
	for (uint32_t i = 0; i < MemoryProperties.memoryTypeCount; i++)
		if (Filter & (1 << i) and (MemoryProperties.memoryTypes[i].propertyFlags & Properties) == Properties)
			return i;

	throw std::runtime_error("Failed to find suitable memory type");
}

void Renderer::CreateBuffer(vk::DeviceSize Size, vk::BufferUsageFlags Usage, vk::MemoryPropertyFlags Properties, vk::Buffer &Buffer, vk::DeviceMemory &Memory)
{
	Buffer = Device.createBuffer(vk::BufferCreateInfo{
		.size = Size,
		.usage = Usage,
		.sharingMode = vk::SharingMode::eExclusive,
	});

	auto Requirements = Device.getBufferMemoryRequirements(Buffer);

	Memory = Device.allocateMemory(vk::MemoryAllocateInfo{
		.allocationSize = Requirements.size,
		.memoryTypeIndex = FindMemoryType(Requirements.memoryTypeBits, Properties),
	});

	Device.bindBufferMemory(Buffer, Memory, 0);
}

void Renderer::CopyBuffer(vk::Buffer Src, vk::Buffer Dst, vk::DeviceSize Size)
{
	auto Command = BeginSingleTimeCommands();
	Command.copyBuffer(Src, Dst, vk::BufferCopy{.size = Size});
	EndSingleTimeCommands(Command);
}

std::vector<const char *> Renderer::GetRequiredExtensions()
{
	uint32_t GlfwExtCount;
	const char **GlfwExtensions = glfwGetRequiredInstanceExtensions(&GlfwExtCount);
	std::vector<const char *> Extensions(GlfwExtensions, GlfwExtensions + GlfwExtCount);

	if (VulkanDebug)
		Extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

	return Extensions;
}

void Renderer::CreateInstance()
{
	if (Raytracing)
	{
		DeviceExtensions.push_back(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME);
		DeviceExtensions.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
		DeviceExtensions.push_back(VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME);
	}

	vk::ApplicationInfo ApplicationInfo{
		.apiVersion = VK_API_VERSION_1_2,
	};

	std::vector<const char *> RequiredExtensions = GetRequiredExtensions();

	vk::InstanceCreateInfo InstanceCreateInfo{
		.pApplicationInfo = &ApplicationInfo,
		.enabledExtensionCount = static_cast<uint32_t>(RequiredExtensions.size()),
		.ppEnabledExtensionNames = RequiredExtensions.data(),
	};

	vk::DebugUtilsMessengerCreateInfoEXT DebugCreateInfo{
		.messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
		.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
		.pfnUserCallback = DebugCallback,
		.pUserData = nullptr,
	};

	if (VulkanDebug)
	{
		InstanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(ValidationLayers.size());
		InstanceCreateInfo.ppEnabledLayerNames = ValidationLayers.data();
		InstanceCreateInfo.pNext = &DebugCreateInfo;
	}

	Instance = vk::createInstance(InstanceCreateInfo);
}

void Renderer::CreateDebugMessenger()
{
	if (VulkanDebug)
	{
		DebugMessenger = Instance.createDebugUtilsMessengerEXT(
			vk::DebugUtilsMessengerCreateInfoEXT{
				.messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
				.messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
				.pfnUserCallback = DebugCallback,
				.pUserData = nullptr,
			},
			nullptr, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));
	}
}

void Renderer::CreateSurface()
{
	VkSurfaceKHR TempSurface = {};

	if (glfwCreateWindowSurface(Instance, Window, nullptr, &TempSurface) != VK_SUCCESS)
		throw std::runtime_error("Failed to create a surface");

	Surface = vk::SurfaceKHR(TempSurface);
}

void Renderer::PickPhysicalDevice()
{
	auto Devices = Instance.enumeratePhysicalDevices();
	if (!Devices.size())
		throw std::runtime_error("No physical devices found");

	uint64_t Rating = 0;
	vk::PhysicalDevice Dev{};
	for (auto &Device : Devices)
	{
		uint64_t TempRating = PhysicalDeviceRating(Device);
		if (TempRating > Rating)
		{
			Rating = TempRating;
			Dev = Device;
		}
	}

	if (Rating == 0)
		throw std::runtime_error("No eligible physical devices found");

	PhysicalDevice = Dev;
}

uint64_t Renderer::PhysicalDeviceRating(vk::PhysicalDevice &Device)
{
	vk::PhysicalDeviceProperties DeviceProperties = Device.getProperties();
	vk::PhysicalDeviceProperties2 DevProp2{.pNext = &RTProperties};
	Device.getProperties2(&DevProp2);

	if (DeviceProperties.apiVersion < VK_API_VERSION_1_2)
		return 0;

	if (not GetQueueFamilies(Device).Complete())
		return 0;

	vk::PhysicalDeviceFeatures DeviceFeatures = Device.getFeatures();

	if (not DeviceFeatures.samplerAnisotropy)
		return 0;

	auto ExtensionProperties = Device.enumerateDeviceExtensionProperties();
	std::set<std::string> RequiredExtensions(DeviceExtensions.begin(), DeviceExtensions.end());

	for (auto &Extension : ExtensionProperties)
		RequiredExtensions.erase(Extension.extensionName);

	if (!RequiredExtensions.empty())
		return 0;

	auto SurfaceFormats = Device.getSurfaceFormatsKHR(Surface);
	auto SurfacePresentModes = Device.getSurfacePresentModesKHR(Surface);

	if (SurfaceFormats.empty() or SurfacePresentModes.empty())
		return 0;

	uint64_t Rating = 1;

	if (DeviceProperties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu)
		Rating += 5;

	return Rating;
}

Renderer::QueueFamilyIndices Renderer::GetQueueFamilies(vk::PhysicalDevice &Device)
{
	auto QueueFamilyProperties = Device.getQueueFamilyProperties();
	QueueFamilyIndices Indices{};

	for (size_t i = 0; i < QueueFamilyProperties.size(); i++)
	{
		if (Device.getSurfaceSupportKHR(i, Surface))
			Indices.PresentFamily = static_cast<uint32_t>(i);

		if (QueueFamilyProperties[i].queueFlags & vk::QueueFlagBits::eGraphics)
			Indices.GraphicsFamily = static_cast<uint32_t>(i);
	}

	return Indices;
}

void Renderer::CreateLogicalDevice()
{
	QueueFamilyIndices Indices = GetQueueFamilies(PhysicalDevice);
	std::vector<vk::DeviceQueueCreateInfo> VecQueueCreateInfo;

	float QueuePriority = 1;
	for (auto &QueueFamily : Indices.GetUnique())
	{
		VecQueueCreateInfo.push_back(vk::DeviceQueueCreateInfo{
			.queueFamilyIndex = QueueFamily,
			.queueCount = 1,
			.pQueuePriorities = &QueuePriority,
		});
	}

	vk::PhysicalDeviceFeatures DeviceFeatures{};
	DeviceFeatures.samplerAnisotropy = VK_TRUE;

	if (Raytracing)
	{
		vk::PhysicalDeviceAccelerationStructureFeaturesKHR AccelFeature{
			.accelerationStructure = true,
		};
		vk::PhysicalDeviceRayTracingPipelineFeaturesKHR RTPipelineFeature{
			.rayTracingPipeline = true,
		};
		vk::PhysicalDeviceBufferDeviceAddressFeaturesEXT BufferDeviceAccessFeature{
			.bufferDeviceAddress = true,
		};

		void *pNext = &AccelFeature;
		AccelFeature.pNext = &RTPipelineFeature;
		RTPipelineFeature.pNext = &BufferDeviceAccessFeature;

		vk::DeviceCreateInfo DeviceCreateInfo{
			.pNext = pNext,
			.queueCreateInfoCount = static_cast<uint32_t>(VecQueueCreateInfo.size()),
			.pQueueCreateInfos = VecQueueCreateInfo.data(),
			.enabledLayerCount = 0,
			.ppEnabledLayerNames = nullptr,
			.enabledExtensionCount = static_cast<uint32_t>(DeviceExtensions.size()),
			.ppEnabledExtensionNames = DeviceExtensions.data(),
			.pEnabledFeatures = &DeviceFeatures,
		};

		Device = PhysicalDevice.createDevice(DeviceCreateInfo);
		PresentQueue = Device.getQueue(Indices.PresentFamily.value(), 0);
		GraphicsQueue = Device.getQueue(Indices.GraphicsFamily.value(), 0);
	}
	else
	{
		vk::DeviceCreateInfo DeviceCreateInfo{
			.pNext = nullptr,
			.queueCreateInfoCount = static_cast<uint32_t>(VecQueueCreateInfo.size()),
			.pQueueCreateInfos = VecQueueCreateInfo.data(),
			.enabledLayerCount = 0,
			.ppEnabledLayerNames = nullptr,
			.enabledExtensionCount = static_cast<uint32_t>(DeviceExtensions.size()),
			.ppEnabledExtensionNames = DeviceExtensions.data(),
			.pEnabledFeatures = &DeviceFeatures,
		};

		Device = PhysicalDevice.createDevice(DeviceCreateInfo);
		PresentQueue = Device.getQueue(Indices.PresentFamily.value(), 0);
		GraphicsQueue = Device.getQueue(Indices.GraphicsFamily.value(), 0);
	}
}

void Renderer::CreateSwapChain()
{
	vk::SurfaceFormatKHR SurfaceFormat = [](auto Formats)
	{
		for (auto &Format : Formats)
			if (Format.format == vk::Format::eB8G8R8A8Srgb and Format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
				return Format;

		return Formats[0];
	}(PhysicalDevice.getSurfaceFormatsKHR(Surface));

	vk::PresentModeKHR PresentMode = [](auto Modes)
	{
		for (auto &Mode : Modes)
			if (Mode == vk::PresentModeKHR::eMailbox)
				return Mode;

		return vk::PresentModeKHR::eFifo;
	}(PhysicalDevice.getSurfacePresentModesKHR(Surface));

	vk::Extent2D Extent = [this](auto Capabilities)
	{
		if (Capabilities.currentExtent.width != UINT32_MAX)
			return Capabilities.currentExtent;

		int Width, Height;
		glfwGetFramebufferSize(Window, &Width, &Height);
		vk::Extent2D ActualExtent = {static_cast<uint32_t>(Width), static_cast<uint32_t>(Height)};

		ActualExtent.width = std::max(Capabilities.minImageExtent.width, std::min(Capabilities.maxImageExtent.width, ActualExtent.width));

		ActualExtent.height = std::max(Capabilities.minImageExtent.height, std::min(Capabilities.maxImageExtent.height, ActualExtent.height));

		return ActualExtent;
	}(PhysicalDevice.getSurfaceCapabilitiesKHR(Surface));

	uint32_t ImageCount = PhysicalDevice.getSurfaceCapabilitiesKHR(Surface).minImageCount + 1;
	QueueFamilyIndices Indices = GetQueueFamilies(PhysicalDevice);

	vk::SwapchainCreateInfoKHR SwapChainInfo{
		.surface = Surface,
		.minImageCount = ImageCount,
		.imageFormat = SurfaceFormat.format,
		.imageColorSpace = SurfaceFormat.colorSpace,
		.imageExtent = Extent,
		.imageArrayLayers = 1,
		.imageUsage = vk::ImageUsageFlagBits::eColorAttachment,
		.imageSharingMode = Indices.Concurrent() ? vk::SharingMode::eConcurrent : vk::SharingMode::eExclusive,
		.queueFamilyIndexCount = static_cast<uint32_t>(Indices.Concurrent() ? 2 : 0),
		.pQueueFamilyIndices = Indices.Concurrent() ? Indices.GetConcurrentFamilies().data() : nullptr,
		.preTransform = PhysicalDevice.getSurfaceCapabilitiesKHR(Surface).currentTransform,
		.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque,
		.presentMode = PresentMode,
		.clipped = true,
	};

	SwapChain = Device.createSwapchainKHR(SwapChainInfo);
	SwapChainImages = Device.getSwapchainImagesKHR(SwapChain);
	SwapChainImageFormat = SurfaceFormat.format;
	SwapChainExtent = Extent;
}

void Renderer::CreateImageViews()
{
	for (auto &Image : SwapChainImages)
	{
		SwapChainImageViews.push_back(CreateImageView(Image, SwapChainImageFormat, vk::ImageAspectFlagBits::eColor));
	}
}

void Renderer::CreateTextureSampler()
{
	vk::SamplerCreateInfo CreateInfo{
		.magFilter = vk::Filter::eLinear,
		.minFilter = vk::Filter::eLinear,
		.mipmapMode = vk::SamplerMipmapMode::eLinear,
		.addressModeU = vk::SamplerAddressMode::eRepeat,
		.addressModeV = vk::SamplerAddressMode::eRepeat,
		.addressModeW = vk::SamplerAddressMode::eRepeat,
		.mipLodBias = 0.0f,
		.anisotropyEnable = VK_TRUE,
		.maxAnisotropy = 16,
		.compareEnable = VK_FALSE,
		.compareOp = vk::CompareOp::eAlways,
		.minLod = 0.0f,
		.maxLod = 0.0f,
		.borderColor = vk::BorderColor::eIntOpaqueBlack,
		.unnormalizedCoordinates = VK_FALSE,
	};

	TextureSampler = Device.createSampler(CreateInfo);
}

void Renderer::CreateRenderPass()
{
	vk::AttachmentDescription ColorAttachment{
		.format = SwapChainImageFormat,
		.samples = vk::SampleCountFlagBits::e1,
		.loadOp = vk::AttachmentLoadOp::eClear,
		.storeOp = vk::AttachmentStoreOp::eStore,
		.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
		.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
		.initialLayout = vk::ImageLayout::eUndefined,
		.finalLayout = vk::ImageLayout::ePresentSrcKHR,
	};

	vk::AttachmentDescription DepthAttachment{
		.format = vk::Format::eD32Sfloat,
		.samples = vk::SampleCountFlagBits::e1,
		.loadOp = vk::AttachmentLoadOp::eClear,
		.storeOp = vk::AttachmentStoreOp::eDontCare,
		.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
		.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
		.initialLayout = vk::ImageLayout::eUndefined,
		.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal,
	};

	vk::AttachmentReference ColorAttachmentReference{
		.attachment = 0,
		.layout = vk::ImageLayout::eColorAttachmentOptimal,
	};

	vk::AttachmentReference DepthAttachmentReference{
		.attachment = 1,
		.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal,
	};

	vk::SubpassDescription SubpassDescription{
		.pipelineBindPoint = vk::PipelineBindPoint::eGraphics,
		.inputAttachmentCount = 0,
		.pInputAttachments = nullptr,
		.colorAttachmentCount = 1,
		.pColorAttachments = &ColorAttachmentReference,
		.pResolveAttachments = nullptr,
		.pDepthStencilAttachment = &DepthAttachmentReference,
		.preserveAttachmentCount = 0,
		.pPreserveAttachments = nullptr,
	};

	vk::SubpassDependency SubpassDependency{
		.srcSubpass = VK_SUBPASS_EXTERNAL,
		.dstSubpass = 0,
		.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
		.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput,
		.srcAccessMask = vk::AccessFlagBits::eNoneKHR,
		.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite,
		.dependencyFlags = {},
	};

	std::array<vk::AttachmentDescription, 2> Attachments{ColorAttachment, DepthAttachment};

	vk::RenderPassCreateInfo RenderPassCreateInfo{
		.attachmentCount = Attachments.size(),
		.pAttachments = Attachments.data(),
		.subpassCount = 1,
		.pSubpasses = &SubpassDescription,
		.dependencyCount = 1,
		.pDependencies = &SubpassDependency,
	};

	RenderPass = Device.createRenderPass(RenderPassCreateInfo);
}

void Renderer::CreateDescriptorSetLayout()
{
	std::array<vk::DescriptorSetLayoutBinding, 3> Bindings{

		vk::DescriptorSetLayoutBinding{
			.binding = 0,
			.descriptorType = vk::DescriptorType::eUniformBuffer,
			.descriptorCount = 1,
			.stageFlags = vk::ShaderStageFlagBits::eVertex,
			.pImmutableSamplers = nullptr,
		},
		vk::DescriptorSetLayoutBinding{
			.binding = 1,
			.descriptorType = vk::DescriptorType::eCombinedImageSampler,
			.descriptorCount = 1,
			.stageFlags = vk::ShaderStageFlagBits::eFragment,
			.pImmutableSamplers = nullptr,
		},
		vk::DescriptorSetLayoutBinding{
			.binding = 2,
			.descriptorType = vk::DescriptorType::eUniformBuffer,
			.descriptorCount = 1,
			.stageFlags = vk::ShaderStageFlagBits::eFragment,
			.pImmutableSamplers = nullptr,
		},
	};

	vk::DescriptorSetLayoutCreateInfo Layout{
		.bindingCount = static_cast<uint32_t>(Bindings.size()),
		.pBindings = Bindings.data(),
	};

	DescriptorSetLayout = Device.createDescriptorSetLayout(Layout);

	if (Raytracing)
	{
		std::array<vk::DescriptorSetLayoutBinding, 5> Bindings{
			vk::DescriptorSetLayoutBinding{
				.binding = 0,
				.descriptorType = vk::DescriptorType::eAccelerationStructureKHR,
				.descriptorCount = 1,
				.stageFlags = vk::ShaderStageFlagBits::eRaygenKHR,
			},
			vk::DescriptorSetLayoutBinding{
				.binding = 1,
				.descriptorType = vk::DescriptorType::eStorageImage,
				.descriptorCount = 1,
				.stageFlags = vk::ShaderStageFlagBits::eRaygenKHR,
			},
			vk::DescriptorSetLayoutBinding{
				.binding = 2,
				.descriptorType = vk::DescriptorType::eUniformBuffer,
				.descriptorCount = 1,
				.stageFlags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eRaygenKHR,
			},
			vk::DescriptorSetLayoutBinding{
				.binding = 3,
				.descriptorType = vk::DescriptorType::eStorageBuffer,
				.descriptorCount = 3,
				.stageFlags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eClosestHitKHR,
			},
			vk::DescriptorSetLayoutBinding{
				.binding = 4,
				.descriptorType = vk::DescriptorType::eCombinedImageSampler,
				.descriptorCount = 16,
				.stageFlags = vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eClosestHitKHR,
			},
		};

		vk::DescriptorSetLayoutCreateInfo Layout{
			.bindingCount = static_cast<uint32_t>(Bindings.size()),
			.pBindings = Bindings.data(),
		};

		RTDescriptorSetLayout = Device.createDescriptorSetLayout(Layout);
	}
}

void Renderer::CreateGraphicsPipeline()
{
	auto ShaderModules = [this](std::vector<std::string> Filenames)
	{
		std::map<std::string, vk::ShaderModule> Shaders;

		for (auto &Filename : Filenames)
		{
			std::fstream File(Filename, std::ios::in | std::ios::binary);
			std::vector<char> ShaderCode(std::filesystem::file_size(Filename));
			File.read(ShaderCode.data(), std::filesystem::file_size(Filename));

			vk::ShaderModuleCreateInfo CreateInfo{
				.codeSize = ShaderCode.size(),
				.pCode = reinterpret_cast<uint32_t *>(ShaderCode.data()),
			};

			Shaders[std::filesystem::path(Filename).stem().string()] = Device.createShaderModule(CreateInfo);
		}

		return Shaders;
	}({"../../data/default/vertex.spv", "../../data/default/fragment.spv"});

	for (auto &&Iter = Shaders.equal_range("default").first; Iter != Shaders.equal_range("default").second; Iter++)
	{
		vk::ShaderModuleCreateInfo CreateInfo{
			.codeSize = Iter->second.Code.size() * sizeof(uint32_t),
			.pCode = Iter->second.Code.data(),
		};

		if (Iter->second.Stage == shaderc_shader_kind::shaderc_vertex_shader)
		{
			Device.destroyShaderModule(ShaderModules["vertex"]);
			ShaderModules["vertex"] = Device.createShaderModule(CreateInfo);
		}

		if (Iter->second.Stage == shaderc_shader_kind::shaderc_fragment_shader)
		{
			Device.destroyShaderModule(ShaderModules["fragment"]);
			ShaderModules["fragment"] = Device.createShaderModule(CreateInfo);
		}
	}

	vk::PipelineShaderStageCreateInfo ShaderStages[] = {
		vk::PipelineShaderStageCreateInfo{
			.stage = vk::ShaderStageFlagBits::eVertex,
			.module = ShaderModules["vertex"],
			.pName = "main",
			.pSpecializationInfo = {},
		},
		vk::PipelineShaderStageCreateInfo{
			.stage = vk::ShaderStageFlagBits::eFragment,
			.module = ShaderModules["fragment"],
			.pName = "main",
			.pSpecializationInfo = {},
		},
	};

	auto BindingDescription = Vertex::GetBindingDescription();
	auto AttributeDescriptions = Vertex::GetAttributeDescriptions();

	vk::PipelineVertexInputStateCreateInfo VertexInputState{
		.vertexBindingDescriptionCount = 1,
		.pVertexBindingDescriptions = &BindingDescription,
		.vertexAttributeDescriptionCount = static_cast<uint32_t>(AttributeDescriptions.size()),
		.pVertexAttributeDescriptions = AttributeDescriptions.data(),
	};

	vk::PipelineInputAssemblyStateCreateInfo InputAssembly{
		.topology = vk::PrimitiveTopology::eTriangleList,
		.primitiveRestartEnable = false,
	};

	vk::Viewport Viewport{
		.x = 0,
		.y = 0,
		.width = static_cast<float>(SwapChainExtent.width),
		.height = static_cast<float>(SwapChainExtent.height),
		.minDepth = 0,
		.maxDepth = 1,
	};

	vk::Rect2D Scissor{
		.offset = {0, 0},
		.extent = SwapChainExtent,
	};

	vk::PipelineViewportStateCreateInfo ViewportState{
		.viewportCount = 1,
		.pViewports = &Viewport,
		.scissorCount = 1,
		.pScissors = &Scissor,
	};

	vk::PipelineRasterizationStateCreateInfo Rasterizer{
		.depthClampEnable = false,
		.rasterizerDiscardEnable = false,
		.polygonMode = vk::PolygonMode::eFill,
		.cullMode = vk::CullModeFlagBits::eNone,
		.frontFace = vk::FrontFace::eClockwise,
		.depthBiasEnable = false,
		.lineWidth = 1,
	};

	vk::PipelineMultisampleStateCreateInfo Multisampling{
		.rasterizationSamples = vk::SampleCountFlagBits::e1,
		.sampleShadingEnable = false,
	};

	vk::PipelineColorBlendAttachmentState ColorBlendAttachment{
		.blendEnable = true,
		.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha,
		.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha,
		.colorBlendOp = vk::BlendOp::eAdd,
		.srcAlphaBlendFactor = vk::BlendFactor::eOne,
		.dstAlphaBlendFactor = vk::BlendFactor::eZero,
		.alphaBlendOp = vk::BlendOp::eAdd,
		.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
	};

	vk::PipelineColorBlendStateCreateInfo ColorBlend{
		.logicOpEnable = false,
		.attachmentCount = 1,
		.pAttachments = &ColorBlendAttachment,
	};

	vk::PipelineDepthStencilStateCreateInfo DepthStencil{
		.depthTestEnable = true,
		.depthWriteEnable = true,
		.depthCompareOp = vk::CompareOp::eLess,
		.depthBoundsTestEnable = false,
		.stencilTestEnable = false,
		.front = {},
		.back = {},
		.minDepthBounds = 0.0f,
		.maxDepthBounds = 1.0f,
	};

	vk::PipelineLayoutCreateInfo LayoutInfo{
		.setLayoutCount = 1,
		.pSetLayouts = &DescriptorSetLayout,
		.pushConstantRangeCount = 0,
		.pPushConstantRanges = nullptr,
	};

	PipelineLayout = Device.createPipelineLayout(LayoutInfo);

	vk::GraphicsPipelineCreateInfo GraphicsPipeline{
		.stageCount = 2,
		.pStages = ShaderStages,
		.pVertexInputState = &VertexInputState,
		.pInputAssemblyState = &InputAssembly,
		.pTessellationState = nullptr,
		.pViewportState = &ViewportState,
		.pRasterizationState = &Rasterizer,
		.pMultisampleState = &Multisampling,
		.pDepthStencilState = &DepthStencil,
		.pColorBlendState = &ColorBlend,
		.pDynamicState = nullptr,
		.layout = PipelineLayout,
		.renderPass = RenderPass,
		.subpass = 0,
	};

	auto Result = Device.createGraphicsPipeline({}, GraphicsPipeline);
	if (Result.result == vk::Result::eSuccess)
		Pipeline = Result.value;
	else
		throw std::runtime_error("Error creating pipeline");

	for (auto &Shader : ShaderModules)
		Device.destroyShaderModule(Shader.second);
}

void Renderer::CreateFramebuffers()
{
	for (size_t i = 0; i < SwapChainImageViews.size(); i++)
	{
		std::array<vk::ImageView, 2> Attachments{SwapChainImageViews[i], DepthImageView};
		vk::FramebufferCreateInfo FramebufferInfo{
			.renderPass = RenderPass,
			.attachmentCount = static_cast<uint32_t>(Attachments.size()),
			.pAttachments = Attachments.data(),
			.width = SwapChainExtent.width,
			.height = SwapChainExtent.height,
			.layers = 1,
		};
		SwapChainFramebuffers.push_back(Device.createFramebuffer(FramebufferInfo));
	}
	if (Raytracing)
	{
		CreateOffscreenTexture();
	}
}

void Renderer::CreateCommandPool()
{
	CommandPool = Device.createCommandPool(vk::CommandPoolCreateInfo{
		.queueFamilyIndex = GetQueueFamilies(PhysicalDevice).GraphicsFamily.value(),
	});
}

void Renderer::CreateDescriptorPool()
{
	std::array<vk::DescriptorPoolSize, 3> PoolSizes{
		vk::DescriptorPoolSize{
			.type = vk::DescriptorType::eUniformBuffer,
			.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),
		},
		vk::DescriptorPoolSize{
			.type = vk::DescriptorType::eCombinedImageSampler,
			.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),
		},
		vk::DescriptorPoolSize{
			.type = vk::DescriptorType::eUniformBuffer,
			.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),
		},
	};

	vk::DescriptorPoolCreateInfo PoolCreateInfo{
		.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
		.maxSets = UINT16_MAX,
		.poolSizeCount = PoolSizes.size(),
		.pPoolSizes = PoolSizes.data(),
	};

	DescriptorPool = Device.createDescriptorPool(PoolCreateInfo);

	if (Raytracing)
	{
		std::array<vk::DescriptorPoolSize, 5> PoolSizes{
			vk::DescriptorPoolSize{
				.type = vk::DescriptorType::eAccelerationStructureKHR,
				.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),
			},
			vk::DescriptorPoolSize{
				.type = vk::DescriptorType::eStorageImage,
				.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),
			},
			vk::DescriptorPoolSize{
				.type = vk::DescriptorType::eUniformBuffer,
				.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),

			},
			vk::DescriptorPoolSize{
				.type = vk::DescriptorType::eStorageBuffer,
				.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),
			},
			vk::DescriptorPoolSize{
				.type = vk::DescriptorType::eCombinedImageSampler,
				.descriptorCount = static_cast<uint32_t>(SwapChainImages.size()),
			},
		};

		vk::DescriptorPoolCreateInfo PoolCreateInfo{
			.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
			.maxSets = UINT16_MAX,
			.poolSizeCount = PoolSizes.size(),
			.pPoolSizes = PoolSizes.data(),
		};

		RTDescriptorPool = Device.createDescriptorPool(PoolCreateInfo);
	}
}

void Renderer::CreateCommandBuffers()
{
	CommandBuffers = Device.allocateCommandBuffers(vk::CommandBufferAllocateInfo{
		.commandPool = CommandPool,
		.level = vk::CommandBufferLevel::ePrimary,
		.commandBufferCount = static_cast<uint32_t>(SwapChainFramebuffers.size()),
	});

	for (size_t i = 0; i < CommandBuffers.size(); i++)
	{
		vk::CommandBufferBeginInfo CommandBegin{};
		CommandBuffers[i].begin(CommandBegin);

		std::array<vk::ClearValue, 2> ClearValues{vk::ClearValue(vk::ClearColorValue(std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f})), vk::ClearValue(vk::ClearDepthStencilValue({1.0f, 0}))};
		vk::RenderPassBeginInfo CommandRenderPass{
			.renderPass = RenderPass,
			.framebuffer = SwapChainFramebuffers[i],
			.renderArea = {.offset = {0, 0}, .extent = SwapChainExtent},
			.clearValueCount = ClearValues.size(),
			.pClearValues = ClearValues.data(),
		};

		CommandBuffers[i].beginRenderPass(CommandRenderPass, vk::SubpassContents::eInline);
		CommandBuffers[i].bindPipeline(vk::PipelineBindPoint::eGraphics, Pipeline);
		if (not Models.empty())
		{
			CommandBuffers[i].bindVertexBuffers(0, VertexBuffer, static_cast<vk::DeviceSize>(0));
			CommandBuffers[i].bindIndexBuffer(IndexBuffer, static_cast<vk::DeviceSize>(0), vk::IndexType::eUint32);
		}
		for (auto &Target : Targets)
		{
			CommandBuffers[i].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, PipelineLayout, 0, Target.second.UniformDescriptorSet[i], {});
			CommandBuffers[i].drawIndexed(static_cast<uint32_t>(Models[Target.second.ModelID].Indices.size()), 1, Models[Target.second.ModelID].IndexOffset, Models[Target.second.ModelID].VertexOffset, 0);
		}
		CommandBuffers[i].endRenderPass();
		CommandBuffers[i].end();
	}
}

void Renderer::CreateSyncObjects()
{
	ImageFences.resize(SwapChainImages.size(), {});
	for (size_t i = 0; i < FramesInFlight; i++)
	{
		ImageAvailableSem.push_back(Device.createSemaphore(vk::SemaphoreCreateInfo{}));
		RenderFinishedSem.push_back(Device.createSemaphore(vk::SemaphoreCreateInfo{}));
		InFlightFences.push_back(Device.createFence(vk::FenceCreateInfo{
			.flags = vk::FenceCreateFlagBits::eSignaled,
		}));
	}
}

vk::ImageView Renderer::CreateImageView(vk::Image Image, vk::Format Format, vk::ImageAspectFlagBits Flags)
{
	vk::ImageViewCreateInfo ViewCreateInfo{
		.image = Image,
		.viewType = vk::ImageViewType::e2D,
		.format = Format,
		.components =
			{
				.r = vk::ComponentSwizzle::eIdentity,
				.g = vk::ComponentSwizzle::eIdentity,
				.b = vk::ComponentSwizzle::eIdentity,
				.a = vk::ComponentSwizzle::eIdentity,
			},
		.subresourceRange =
			{
				.aspectMask = Flags,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
	};

	return Device.createImageView(ViewCreateInfo);
}

void Renderer::CleanSwapChain()
{
	Device.destroyImageView(DepthImageView);
	Device.destroyImage(DepthImage);
	Device.freeMemory(DepthImageMemory);

	Device.freeCommandBuffers(CommandPool, CommandBuffers);
	Device.destroyDescriptorPool(DescriptorPool);

	for (auto &Framebuffer : SwapChainFramebuffers)
		Device.destroyFramebuffer(Framebuffer);
	SwapChainFramebuffers.clear();

	Device.destroyPipeline(Pipeline);
	Device.destroyPipelineLayout(PipelineLayout);
	Device.destroyRenderPass(RenderPass);

	for (auto &View : SwapChainImageViews)
		Device.destroyImageView(View);
	SwapChainImageViews.clear();

	Device.destroySwapchainKHR(SwapChain);
}

void Renderer::RebuildSwapChain()
{
	int Width, Height;
	glfwGetFramebufferSize(Window, &Width, &Height);
	if (Width == 0 or Height == 0)
	{
		PauseDraw = true;
		return;
	}
	else
		PauseDraw = false;

	Device.waitIdle();
	CleanSwapChain();

	CreateSwapChain();
	CreateImageViews();
	CreateRenderPass();
	CreateGraphicsPipeline();
	CreateDepthResources();
	CreateFramebuffers();
	CreateDescriptorPool();

	for (auto &Target : Targets)
		CreateDescriptorSets(Target.second);

	CreateCommandBuffers();
}

void Renderer::UpdateUniformBuffer(uint32_t Image)
{
	ProjectionT = glm::perspective(glm::radians(45.0f), static_cast<float>(SwapChainExtent.width) / static_cast<float>(SwapChainExtent.height), 0.1f, 100.0f);
	ProjectionT[1][1] *= -1;

	for (auto &Target : Targets)
	{
		UniformBufferObject UBO{
			.Model = Target.second.Transform,
			.View = Camera->GetView(),
			.Proj = ProjectionT,
		};

		void *Data = Device.mapMemory(Target.second.UniformDeviceMemory[Image], 0, sizeof(UBO));
		std::memcpy(Data, &UBO, sizeof(UBO));
		Device.unmapMemory(Target.second.UniformDeviceMemory[Image]);

		UniformBufferLights UBL{
			.NormalMatrix = glm::inverseTranspose(Target.second.Transform),
			.CameraPosition = Camera->GetPos(),
			.LightsNum = glm::uvec4(static_cast<uint32_t>(LightTemp.size()), 0, 0, 0),
		};
		std::memcpy(UBL.Lights, LightTemp.data(), sizeof(Light) * LightTemp.size());

		Data = Device.mapMemory(Target.second.UniformDeviceMemoryLights[Image], 0, sizeof(UBL));
		std::memcpy(Data, &UBL, sizeof(UBL));
		Device.unmapMemory(Target.second.UniformDeviceMemoryLights[Image]);
	}
}

void Renderer::UpdateSceneDescription()
{}

void Renderer::RebuildBuffers()
{
	if (VertexBuffer)
	{
		Device.destroyBuffer(VertexBuffer);
		Device.freeMemory(VertexBufferMemory);
		VertexBuffer = vk::Buffer{};
	}
	if (IndexBuffer)
	{
		Device.destroyBuffer(IndexBuffer);
		Device.freeMemory(IndexBufferMemory);
		IndexBuffer = vk::Buffer{};
	}
	if (Models.empty())
		return;

	CreateVertexBuffer();
	CreateIndexBuffer();

	if (Raytracing)
	{
		vk::DeviceAddress VertexAddress = Device.getBufferAddress(vk::BufferDeviceAddressInfo{.buffer = VertexBuffer});
		vk::DeviceAddress IndexAddress = Device.getBufferAddress(vk::BufferDeviceAddressInfo{.buffer = IndexBuffer});

		vk::AccelerationStructureGeometryTrianglesDataKHR Triangles{
			.vertexFormat = vk::Format::eR32G32B32Sfloat,
			.vertexData = VertexAddress,
			.vertexStride = sizeof(Vertex),
			.maxVertex = MaxIndexOffset,
			.indexType = vk::IndexType::eUint32,
			.indexData = {IndexAddress},
		};

		vk::AccelerationStructureGeometryKHR Geometry{
			.geometryType = vk::GeometryTypeKHR::eTriangles,
			.geometry = Triangles,
			.flags = vk::GeometryFlagBitsKHR::eOpaque,
		};

		vk::AccelerationStructureBuildRangeInfoKHR Offset{
			.primitiveCount = MaxIndexOffset / 3,
			.primitiveOffset = 0,
			.firstVertex = 0,
			.transformOffset = 0,
		};

		vk::AccelerationStructureBuildGeometryInfoKHR GeometryInfo{
			.type = vk::AccelerationStructureTypeKHR::eBottomLevel,
			.mode = vk::BuildAccelerationStructureModeKHR::eBuild,
			.geometryCount = 1,
			.pGeometries = &Geometry,
		};

		auto AccelBuildSize = Device.getAccelerationStructureBuildSizesKHR(vk::AccelerationStructureBuildTypeKHR::eDevice, GeometryInfo, MaxIndexOffset, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));
		vk::Buffer ScratchBuffer;
		vk::DeviceMemory ScratchBufferMemory;
		CreateBuffer(AccelBuildSize.buildScratchSize, vk::BufferUsageFlagBits::eShaderDeviceAddress | vk::BufferUsageFlagBits::eStorageBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal, ScratchBuffer, ScratchBufferMemory);

		vk::BufferDeviceAddressInfo ScratchAddressInfo{.buffer = ScratchBuffer};
		vk::DeviceAddress ScratchAddress = Device.getBufferAddress(ScratchAddressInfo);

		CreateBuffer(AccelBuildSize.accelerationStructureSize, vk::BufferUsageFlagBits::eShaderDeviceAddress | vk::BufferUsageFlagBits::eAccelerationStructureStorageKHR, vk::MemoryPropertyFlagBits::eDeviceLocal, BLASBuffer, BLASMemory);

		vk::AccelerationStructureCreateInfoKHR AccelCreateInfo{
			.buffer = BLASBuffer,
			.size = AccelBuildSize.accelerationStructureSize,
			.type = vk::AccelerationStructureTypeKHR::eBottomLevel,
		};

		BLASAccel = Device.createAccelerationStructureKHR(AccelCreateInfo, nullptr, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));

		GeometryInfo.dstAccelerationStructure = BLASAccel;
		GeometryInfo.scratchData = ScratchAddress;

		auto Command = BeginSingleTimeCommands();
		Command.buildAccelerationStructuresKHR(GeometryInfo, &Offset, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));
		EndSingleTimeCommands(Command);

		Device.freeMemory(ScratchBufferMemory);
		Device.destroyBuffer(ScratchBuffer);
	}
}

void Renderer::CreateVertexBuffer()
{
	vk::DeviceSize BufferSize = 0;
	for (auto &Model : Models)
		BufferSize += sizeof(Vertex) * Model.second.Vertices.size();

	vk::Buffer StagingBuffer;
	vk::DeviceMemory StagingBufferMemory;
	CreateBuffer(BufferSize, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, StagingBuffer, StagingBufferMemory);

	void *Data = Device.mapMemory(StagingBufferMemory, 0, BufferSize);
	size_t Offset = 0;
	for (auto &Model : Models)
	{
		Model.second.VertexOffset = Offset;
		std::memcpy(static_cast<Vertex *>(Data) + Offset, Model.second.Vertices.data(), Model.second.Vertices.size() * sizeof(Vertex));
		Offset += Model.second.Vertices.size();
	}
	Device.unmapMemory(StagingBufferMemory);

	CreateBuffer(BufferSize,
				 vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer |
					 (Raytracing ? vk::BufferUsageFlagBits::eShaderDeviceAddress | vk::BufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyKHR | vk::BufferUsageFlagBits::eStorageBuffer : vk::BufferUsageFlagBits::eTransferDst),
				 vk::MemoryPropertyFlagBits::eDeviceLocal, VertexBuffer, VertexBufferMemory);
	CopyBuffer(StagingBuffer, VertexBuffer, BufferSize);
	Device.destroyBuffer(StagingBuffer);
	Device.freeMemory(StagingBufferMemory);

	VertexBufferSize = BufferSize;
}

void Renderer::CreateIndexBuffer()
{
	vk::DeviceSize BufferSize = 0;
	for (auto &Model : Models)
		BufferSize += sizeof(uint32_t) * Model.second.Indices.size();

	vk::Buffer StagingBuffer;
	vk::DeviceMemory StagingBufferMemory;
	CreateBuffer(BufferSize, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, StagingBuffer, StagingBufferMemory);

	void *Data = Device.mapMemory(StagingBufferMemory, 0, BufferSize);
	size_t Offset = 0;
	for (auto &Model : Models)
	{
		Model.second.IndexOffset = Offset;
		std::memcpy(static_cast<uint32_t *>(Data) + Offset, Model.second.Indices.data(), Model.second.Indices.size() * sizeof(uint32_t));
		Offset += Model.second.Indices.size();
	}
	MaxIndexOffset = static_cast<uint32_t>(Offset);
	Device.unmapMemory(StagingBufferMemory);

	CreateBuffer(BufferSize,
				 vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer |
					 (Raytracing ? vk::BufferUsageFlagBits::eShaderDeviceAddress | vk::BufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyKHR | vk::BufferUsageFlagBits::eStorageBuffer : vk::BufferUsageFlagBits::eTransferDst),
				 vk::MemoryPropertyFlagBits::eDeviceLocal, IndexBuffer, IndexBufferMemory);
	CopyBuffer(StagingBuffer, IndexBuffer, BufferSize);
	Device.destroyBuffer(StagingBuffer);
	Device.freeMemory(StagingBufferMemory);

	IndexBufferSize = BufferSize;
}

void Renderer::CreateImage(uint32_t Width, uint32_t Height, vk::Format Format, vk::ImageTiling Tiling, vk::ImageUsageFlags Usage, vk::MemoryPropertyFlags Properties, vk::Image &Image, vk::DeviceMemory &Memory)
{
	vk::ImageCreateInfo ImageInfo{
		.imageType = vk::ImageType::e2D,
		.format = Format,
		.extent = {.width = Width, .height = Height, .depth = 1},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = vk::SampleCountFlagBits::e1,
		.tiling = Tiling,
		.usage = Usage,
		.sharingMode = vk::SharingMode::eExclusive,
		.initialLayout = vk::ImageLayout::eUndefined,
	};
	Image = Device.createImage(ImageInfo);

	vk::MemoryRequirements MemoryRequirements = Device.getImageMemoryRequirements(Image);
	vk::MemoryAllocateInfo AllocInfo{
		.allocationSize = MemoryRequirements.size,
		.memoryTypeIndex = FindMemoryType(MemoryRequirements.memoryTypeBits, Properties),
	};
	Memory = Device.allocateMemory(AllocInfo);
	Device.bindImageMemory(Image, Memory, 0);
}

void Renderer::TransitionImageLayout(vk::Image Image, vk::Format Format, vk::ImageLayout OldLayout, vk::ImageLayout NewLayout)
{
	auto Command = BeginSingleTimeCommands();

	vk::ImageMemoryBarrier Barrier{
		.srcAccessMask = {},
		.dstAccessMask = {},
		.oldLayout = OldLayout,
		.newLayout = NewLayout,
		.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
		.image = Image,
		.subresourceRange =
			{
				.aspectMask = vk::ImageAspectFlagBits::eColor,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
	};
	vk::PipelineStageFlags SourceStage;
	vk::PipelineStageFlags DestinationStage;

	if (OldLayout == vk::ImageLayout::eUndefined and NewLayout == vk::ImageLayout::eTransferDstOptimal)
	{
		Barrier.srcAccessMask = {};
		Barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

		SourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
		DestinationStage = vk::PipelineStageFlagBits::eTransfer;
	}
	else if (OldLayout == vk::ImageLayout::eTransferDstOptimal and NewLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
	{
		Barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		Barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

		SourceStage = vk::PipelineStageFlagBits::eTransfer;
		DestinationStage = vk::PipelineStageFlagBits::eFragmentShader;
	}
	else if (OldLayout == vk::ImageLayout::eUndefined and NewLayout == vk::ImageLayout::eGeneral)
	{
		Barrier.srcAccessMask = {};
		Barrier.dstAccessMask = {};

		SourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
		DestinationStage = vk::PipelineStageFlagBits::eBottomOfPipe;
	}
	else
	{
		throw std::invalid_argument("Unsupporded layout transition");
	}

	Command.pipelineBarrier(SourceStage, DestinationStage, {}, {}, {}, Barrier);
	EndSingleTimeCommands(Command);
}

void Renderer::CopyBufferToImage(vk::Buffer Buffer, vk::Image Image, uint32_t Width, uint32_t Height)
{
	auto Command = BeginSingleTimeCommands();
	Command.copyBufferToImage(Buffer, Image, vk::ImageLayout::eTransferDstOptimal,
							  vk::BufferImageCopy{
								  .bufferOffset = 0,
								  .bufferRowLength = 0,
								  .bufferImageHeight = 0,
								  .imageSubresource =
									  {
										  .aspectMask = vk::ImageAspectFlagBits::eColor,
										  .mipLevel = 0,
										  .baseArrayLayer = 0,
										  .layerCount = 1,
									  },
								  .imageOffset =
									  {
										  .x = 0,
										  .y = 0,
										  .z = 0,
									  },
								  .imageExtent =
									  {
										  .width = Width,
										  .height = Height,
										  .depth = 1,
									  },
							  });
	EndSingleTimeCommands(Command);
}

void Renderer::CreateOffscreenTexture()
{
	RTOffscreenImages.resize(SwapChainImages.size());
	RTOffscreenImageMemories.resize(SwapChainImages.size());
	RTOffscreenImageViews.resize(SwapChainImages.size());

	for (size_t i = 0; i < SwapChainImages.size(); i++)
	{
		CreateImage(SwapChainExtent.width, SwapChainExtent.height, vk::Format::eR32G32B32A32Sfloat, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage,
					vk::MemoryPropertyFlagBits::eDeviceLocal, RTOffscreenImages[i], RTOffscreenImageMemories[i]);
		TransitionImageLayout(RTOffscreenImages[i], vk::Format::eR32G32B32A32Sfloat, vk::ImageLayout::eUndefined, vk::ImageLayout::eGeneral);
		RTOffscreenImageViews[i] = CreateImageView(RTOffscreenImages[i], vk::Format::eR32G32B32A32Sfloat, vk::ImageAspectFlagBits::eColor);
	}
}

void Renderer::CreateUniformBuffers(Target &Target)
{
	Target.UniformBuffer.resize(SwapChainImages.size());
	Target.UniformDeviceMemory.resize(SwapChainImages.size());
	Target.UniformBufferLights.resize(SwapChainImages.size());
	Target.UniformDeviceMemoryLights.resize(SwapChainImages.size());

	for (size_t i = 0; i < SwapChainImages.size(); i++)
	{
		CreateBuffer(sizeof(UniformBufferObject), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, Target.UniformBuffer[i], Target.UniformDeviceMemory[i]);
		CreateBuffer(sizeof(UniformBufferLights), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, Target.UniformBufferLights[i], Target.UniformDeviceMemoryLights[i]);
	}
}

void Renderer::CreateDescriptorSets(Target &Target)
{
	std::vector<vk::DescriptorSetLayout> Layouts(SwapChainImages.size(), DescriptorSetLayout);

	vk::DescriptorSetAllocateInfo AllocInfo{
		.descriptorPool = DescriptorPool,
		.descriptorSetCount = static_cast<uint32_t>(SwapChainImages.size()),
		.pSetLayouts = Layouts.data(),
	};

	Target.UniformDescriptorSet = Device.allocateDescriptorSets(AllocInfo);

	for (size_t i = 0; i < SwapChainImages.size(); i++)
	{
		vk::DescriptorBufferInfo BufferInfo{
			.buffer = Target.UniformBuffer[i],
			.offset = 0,
			.range = sizeof(UniformBufferObject),
		};
		vk::DescriptorImageInfo ImageInfo{
			.sampler = TextureSampler,
			.imageView = Textures[Target.TextureID].TextureImageView,
			.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal,
		};
		vk::DescriptorBufferInfo BufferInfoLights{
			.buffer = Target.UniformBufferLights[i],
			.offset = 0,
			.range = sizeof(UniformBufferLights),
		};

		std::array<vk::WriteDescriptorSet, 3> DescriptorWrites{
			vk::WriteDescriptorSet{
				.dstSet = Target.UniformDescriptorSet[i],
				.dstBinding = 0,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eUniformBuffer,
				.pBufferInfo = &BufferInfo,
			},
			vk::WriteDescriptorSet{
				.dstSet = Target.UniformDescriptorSet[i],
				.dstBinding = 1,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eCombinedImageSampler,
				.pImageInfo = &ImageInfo,
			},
			vk::WriteDescriptorSet{
				.dstSet = Target.UniformDescriptorSet[i],
				.dstBinding = 2,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eUniformBuffer,
				.pBufferInfo = &BufferInfoLights,
			},
		};

		Device.updateDescriptorSets(DescriptorWrites, nullptr);
	}
}

void Renderer::CreateDescriptorSetsRT()
{
	std::vector<vk::DescriptorSetLayout> Layouts(SwapChainImages.size(), RTDescriptorSetLayout);
	vk::DescriptorSetAllocateInfo AllocInfo{
		.descriptorPool = RTDescriptorPool,
		.descriptorSetCount = static_cast<uint32_t>(SwapChainImages.size()),
		.pSetLayouts = Layouts.data(),
	};
	RTDescriptorSets = Device.allocateDescriptorSets(AllocInfo);

	CreateBuffer(sizeof(CameraObject), vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, CameraBuffer, CameraBufferMemory);

	for (size_t i = 0; i < SwapChainImages.size(); i++)
	{
		vk::WriteDescriptorSetAccelerationStructureKHR TLASInfo{
			.accelerationStructureCount = 1,
			.pAccelerationStructures = &TLASAccel,
		};
		vk::DescriptorImageInfo ImageInfo{
			.imageView = RTOffscreenImageViews[i],
			.imageLayout = vk::ImageLayout::eGeneral,
		};
		vk::DescriptorBufferInfo CameraInfo{
			.buffer = CameraBuffer,
			.offset = 0,
			.range = sizeof(CameraObject),
		};
		vk::DescriptorBufferInfo VertexBufferInfo{
			.buffer = VertexBuffer,
			.offset = 0,
			.range = VertexBufferSize,
		};
		vk::DescriptorBufferInfo IndexBufferInfo{
			.buffer = IndexBuffer,
			.offset = 0,
			.range = IndexBufferSize,
		};
		vk::DescriptorBufferInfo TextureMappingBufferInfo{
			.buffer = TextureMappingBuffer,
			.offset = 0,
			.range = TextureMappingBufferSize,
		};

		std::array<vk::WriteDescriptorSet, 6> DescriptorWrites{
			vk::WriteDescriptorSet{
				.pNext = &TLASInfo,
				.dstSet = RTDescriptorSets[i],
				.dstBinding = 0,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eAccelerationStructureKHR,
			},
			vk::WriteDescriptorSet{
				.dstSet = RTDescriptorSets[i],
				.dstBinding = 1,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eStorageImage,
				.pImageInfo = &ImageInfo,
			},
			vk::WriteDescriptorSet{
				.dstSet = RTDescriptorSets[i],
				.dstBinding = 2,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eUniformBuffer,
				.pBufferInfo = &CameraInfo,
			},
			vk::WriteDescriptorSet{
				.dstSet = RTDescriptorSets[i],
				.dstBinding = 3,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eStorageBuffer,
				.pBufferInfo = &VertexBufferInfo,
			},
			vk::WriteDescriptorSet{
				.dstSet = RTDescriptorSets[i],
				.dstBinding = 3,
				.dstArrayElement = 1,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eStorageBuffer,
				.pBufferInfo = &IndexBufferInfo,
			},
			vk::WriteDescriptorSet{
				.dstSet = RTDescriptorSets[i],
				.dstBinding = 3,
				.dstArrayElement = 2,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eStorageBuffer,
				.pBufferInfo = &TextureMappingBufferInfo,
			},
		};

		Device.updateDescriptorSets(DescriptorWrites, nullptr);
	}
}

vk::CommandBuffer Renderer::BeginSingleTimeCommands()
{
	auto Buffer = Device.allocateCommandBuffers(vk::CommandBufferAllocateInfo{.commandPool = CommandPool, .level = vk::CommandBufferLevel::ePrimary, .commandBufferCount = 1});
	Buffer.front().begin(vk::CommandBufferBeginInfo{.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit});
	return Buffer.front();
}

void Renderer::EndSingleTimeCommands(vk::CommandBuffer Buffer)
{
	Buffer.end();
	GraphicsQueue.submit(vk::SubmitInfo{.commandBufferCount = 1, .pCommandBuffers = &Buffer});
	GraphicsQueue.waitIdle();
	Device.freeCommandBuffers(CommandPool, Buffer);
}

void Renderer::CreateDepthResources()
{
	CreateImage(SwapChainExtent.width, SwapChainExtent.height, vk::Format::eD32Sfloat, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal, DepthImage, DepthImageMemory);
	DepthImageView = CreateImageView(DepthImage, vk::Format::eD32Sfloat, vk::ImageAspectFlagBits::eDepth);
}

void Renderer::BuildTLAS()
{
	size_t InstancesBufferSize = sizeof(vk::AccelerationStructureInstanceKHR) * TLASInstances.size();

	vk::Buffer InstancesBuffer{};
	vk::DeviceMemory InstancesBufferMemory{};
	CreateBuffer(InstancesBufferSize, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eShaderDeviceAddress | vk::BufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyKHR, vk::MemoryPropertyFlagBits::eDeviceLocal,
				 InstancesBuffer, InstancesBufferMemory);

	vk::Buffer StagingBuffer;
	vk::DeviceMemory StagingBufferMemory;
	CreateBuffer(InstancesBufferSize, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, StagingBuffer, StagingBufferMemory);

	void *Data = Device.mapMemory(StagingBufferMemory, 0, InstancesBufferSize);
	std::memcpy(Data, TLASInstances.data(), InstancesBufferSize);
	Device.unmapMemory(StagingBufferMemory);
	CopyBuffer(StagingBuffer, InstancesBuffer, InstancesBufferSize);

	Device.destroyBuffer(StagingBuffer);
	Device.freeMemory(StagingBufferMemory);

	vk::DeviceAddress InstanceBufferAddress = Device.getBufferAddress(vk::BufferDeviceAddressInfo{.buffer = InstancesBuffer});
	vk::MemoryBarrier Barrier{
		.srcAccessMask = vk::AccessFlagBits::eTransferWrite,
		.dstAccessMask = vk::AccessFlagBits::eAccelerationStructureWriteKHR,
	};

	auto Command = BeginSingleTimeCommands();
	Command.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eAccelerationStructureBuildKHR, {}, Barrier, {}, {});

	vk::AccelerationStructureGeometryInstancesDataKHR InstancesVK{};
	InstancesVK.data.deviceAddress = InstanceBufferAddress;

	vk::AccelerationStructureGeometryKHR TLASGeometry{
		.geometryType = vk::GeometryTypeKHR::eInstances,
		.geometry = InstancesVK,
	};

	vk::AccelerationStructureBuildGeometryInfoKHR BuildInfo{
		.type = vk::AccelerationStructureTypeKHR::eTopLevel,
		.flags = vk::BuildAccelerationStructureFlagBitsKHR::ePreferFastTrace,
		.mode = vk::BuildAccelerationStructureModeKHR::eBuild,
		.geometryCount = 1,
		.pGeometries = &TLASGeometry,
	};

	auto SizeInfo = Device.getAccelerationStructureBuildSizesKHR(vk::AccelerationStructureBuildTypeKHR::eDevice, BuildInfo, TLASInstances.size(), vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));
	CreateBuffer(SizeInfo.accelerationStructureSize, vk::BufferUsageFlagBits::eAccelerationStructureStorageKHR, vk::MemoryPropertyFlagBits::eDeviceLocal, TLASBuffer, TLASMemory);

	vk::AccelerationStructureCreateInfoKHR CreateInfo{
		.buffer = TLASBuffer,
		.size = SizeInfo.accelerationStructureSize,
		.type = vk::AccelerationStructureTypeKHR::eTopLevel,
	};

	TLASAccel = Device.createAccelerationStructureKHR(CreateInfo, nullptr, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));

	vk::Buffer ScratchBuffer{};
	vk::DeviceMemory ScratchBufferMemory{};
	CreateBuffer(SizeInfo.buildScratchSize, vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eShaderDeviceAddress, vk::MemoryPropertyFlagBits::eDeviceLocal, ScratchBuffer, ScratchBufferMemory);
	auto ScratchAddress = Device.getBufferAddress(vk::BufferDeviceAddressInfo{.buffer = ScratchBuffer});

	BuildInfo.dstAccelerationStructure = TLASAccel;
	BuildInfo.scratchData.deviceAddress = ScratchAddress;

	vk::AccelerationStructureBuildRangeInfoKHR RangeInfo{
		.primitiveCount = static_cast<uint32_t>(TLASInstances.size()),
		.primitiveOffset = 0,
		.firstVertex = 0,
		.transformOffset = 0,
	};
	const vk::AccelerationStructureBuildRangeInfoKHR *pRangeInfo = &RangeInfo;

	Command.buildAccelerationStructuresKHR(BuildInfo, pRangeInfo, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));
	EndSingleTimeCommands(Command);

	Device.freeMemory(ScratchBufferMemory);
	Device.destroyBuffer(ScratchBuffer);
	Device.freeMemory(InstancesBufferMemory);
	Device.destroyBuffer(InstancesBuffer);
}

void Renderer::CreateModels()
{
	if (ModelsCreate.empty())
		return;

	Device.waitIdle();
	for (auto &Pair : ModelsCreate)
	{
		Models.insert(Pair);
	}

	RebuildBuffers();
	CreateCommandBuffers();
	ModelsCreate.clear();
}

void Renderer::CreateTextures()
{
	if (TexturesCreate.empty())
		return;

	Device.waitIdle();
	for (auto &Pair : TexturesCreate)
	{
		vk::DeviceSize ImageSize = Pair.second.Pixels.size();
		vk::Buffer StagingBuffer;
		vk::DeviceMemory StagingBufferMemory;
		CreateBuffer(ImageSize, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, StagingBuffer, StagingBufferMemory);

		void *Data = Device.mapMemory(StagingBufferMemory, 0, ImageSize, {});
		std::memcpy(Data, Pair.second.Pixels.data(), Pair.second.Pixels.size());
		Device.unmapMemory(StagingBufferMemory);

		CreateImage(static_cast<uint32_t>(Pair.second.Width), static_cast<uint32_t>(Pair.second.Height), vk::Format::eR8G8B8A8Srgb, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
					vk::MemoryPropertyFlagBits::eDeviceLocal, Pair.second.TextureImage, Pair.second.TextureMemory);

		TransitionImageLayout(Pair.second.TextureImage, vk::Format::eR8G8B8A8Srgb, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);
		CopyBufferToImage(StagingBuffer, Pair.second.TextureImage, static_cast<uint32_t>(Pair.second.Width), static_cast<uint32_t>(Pair.second.Height));
		TransitionImageLayout(Pair.second.TextureImage, vk::Format::eR8G8B8A8Srgb, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);

		Device.destroyBuffer(StagingBuffer);
		Device.freeMemory(StagingBufferMemory);

		Pair.second.TextureImageView = CreateImageView(Pair.second.TextureImage, vk::Format::eR8G8B8A8Srgb, vk::ImageAspectFlagBits::eColor);
		Textures.insert(Pair);
	}

	TexturesCreate.clear();
}

void Renderer::CreateLights()
{
	if (LightsCreate.empty())
		return;

	for (auto &Pair : LightsCreate)
	{
		Lights.insert(Pair);
	}

	LightsCreate.clear();
}

void Renderer::CreateTargets()
{
	if (TargetsCreate.empty())
		return;

	Device.waitIdle();

	for (auto &[TargetID, RenderTarget] : TargetsCreate)
	{
		for (auto &LightID : RenderTarget.LightID)
		{
			LightBind[LightID] = TargetID;
		}
		Targets.insert({TargetID, RenderTarget});
	}

	if (Raytracing)
	{
		TLASInstances.reserve(Targets.size());
		for (auto &[TargetID, RenderTarget] : Targets)
		{
			vk::TransformMatrixKHR Transform;
			glm::mat4 Temp = glm::inverse(RenderTarget.Transform);
			std::memcpy(&Transform, &Temp, sizeof(vk::TransformMatrixKHR));

			vk::AccelerationStructureInstanceKHR AccelInstance{
				.transform = Transform,
				.instanceCustomIndex = TargetID,
				.mask = 0xFF,
				.instanceShaderBindingTableRecordOffset = 0,
				.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR,
				.accelerationStructureReference = Device.getAccelerationStructureAddressKHR(vk::AccelerationStructureDeviceAddressInfoKHR{.accelerationStructure = BLASAccel}, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr)),
			};

			TLASInstances.push_back(AccelInstance);
		}
		BuildTLAS();
		CreateDescriptorSetsRT();
	}
	else
	{
		for (auto &[TargetID, RenderTarget] : TargetsCreate)
		{
			CreateUniformBuffers(RenderTarget);
			CreateDescriptorSets(RenderTarget);
			Targets[TargetID] = RenderTarget;
		}
	}

	CreateCommandBuffers();
	TargetsCreate.clear();
}

void Renderer::UpdateLights()
{
	LightTemp.clear();
	for (auto &Pair : Lights)
	{
		LightTemp.push_back(Pair.second);
		if (LightBind.contains(Pair.first))
			LightTemp.back().Position += Targets[LightBind[Pair.first]].Transform[3];
	}
}

void Renderer::DeleteModels()
{
	if (ModelsDelete.empty())
		return;

	Device.waitIdle();
	for (auto ID : ModelsDelete)
	{
		Models.erase(ID);
	}

	RebuildBuffers();
	CreateCommandBuffers();
	ModelsDelete.clear();
}

void Renderer::DeleteTextures()
{
	if (TexturesDelete.empty())
		return;

	Device.waitIdle();

	for (auto ID : TexturesDelete)
	{
		Device.destroyImageView(Textures[ID].TextureImageView);
		Device.destroyImage(Textures[ID].TextureImage);
		Device.freeMemory(Textures[ID].TextureMemory);
	}

	TexturesDelete.clear();
}

void Renderer::DeleteLights()
{
	if (LightsDelete.empty())
		return;

	for (auto ID : LightsDelete)
	{
		Lights.erase(ID);
	}

	LightsDelete.clear();
}

void Renderer::DeleteTargets()
{
	if (TargetsDelete.empty())
		return;

	Device.waitIdle();
	for (auto ID : TargetsDelete)
	{
		for (auto &Buffer : Targets[ID].UniformBuffer)
			Device.destroyBuffer(Buffer);

		for (auto &Buffer : Targets[ID].UniformBufferLights)
			Device.destroyBuffer(Buffer);

		for (auto &Memory : Targets[ID].UniformDeviceMemory)
			Device.freeMemory(Memory);

		for (auto &Memory : Targets[ID].UniformDeviceMemoryLights)
			Device.freeMemory(Memory);

		Device.freeDescriptorSets(DescriptorPool, Targets[ID].UniformDescriptorSet);
		Targets.erase(ID);
		std::vector<uint16_t> toChange;
		for (auto &[I, T] : Targets)
		{
			if (I > ID)
			{
				toChange.push_back(I);
			}
		}
		for (auto I : toChange)
		{
			auto node = Targets.extract(I);
			node.key() = node.key() - 1;
			Targets.insert(std::move(node));
		}

		for (auto Iter = LightBind.begin(); Iter != LightBind.end();)
		{
			if (Iter->second == ID)
			{
				Iter = LightBind.erase(Iter);
			}
			else
			{
				if (Iter->second > ID)
				{
					Iter->second--;
				}
				Iter++;
			}
		}
	}

	CreateCommandBuffers();
	TargetsDelete.clear();
}

Renderer::Renderer(GLFWwindow *pWindow, bool Raytracing, const std::multimap<std::string, Shader> &Shaders) : Window(pWindow), Raytracing(Raytracing), Shaders(Shaders)
{
	CreateInstance();
	CreateDebugMessenger();
	CreateSurface();
	PickPhysicalDevice();
	CreateLogicalDevice();
	CreateSwapChain();
	CreateImageViews();
	CreateRenderPass();
	CreateDescriptorSetLayout();
	CreateGraphicsPipeline();
	CreateCommandPool();
	CreateDepthResources();
	CreateFramebuffers();
	CreateDescriptorPool();
	CreateCommandBuffers();
	CreateSyncObjects();
	CreateTextureSampler();
}

Renderer::~Renderer()
{
	Device.waitIdle();

	if (not Targets.empty())
	{
		for (auto &Target : Targets)
		{
			for (auto &Buffer : Target.second.UniformBuffer)
				Device.destroyBuffer(Buffer);

			for (auto &Buffer : Target.second.UniformBufferLights)
				Device.destroyBuffer(Buffer);

			for (auto &Memory : Target.second.UniformDeviceMemory)
				Device.freeMemory(Memory);

			for (auto &Memory : Target.second.UniformDeviceMemoryLights)
				Device.freeMemory(Memory);
		}
	}

	if (not Textures.empty())
	{
		for (auto &Texture : Textures)
		{
			Device.destroyImageView(Texture.second.TextureImageView);
			Device.destroyImage(Texture.second.TextureImage);
			Device.freeMemory(Texture.second.TextureMemory);
		}
	}

	if (not Models.empty())
	{
		Device.destroyBuffer(VertexBuffer);
		Device.freeMemory(VertexBufferMemory);
		Device.destroyBuffer(IndexBuffer);
		Device.freeMemory(IndexBufferMemory);
	}

	for (size_t i = 0; i < FramesInFlight; i++)
	{
		Device.destroySemaphore(ImageAvailableSem[i]);
		Device.destroySemaphore(RenderFinishedSem[i]);
		Device.destroyFence(InFlightFences[i]);
	}

	CleanSwapChain();

	Device.destroySampler(TextureSampler);
	Device.destroyDescriptorSetLayout(DescriptorSetLayout);
	Device.destroyCommandPool(CommandPool);
	Device.destroy();
	Instance.destroySurfaceKHR(Surface);

	if (VulkanDebug)
		Instance.destroyDebugUtilsMessengerEXT(DebugMessenger, nullptr, vk::DispatchLoaderDynamic(Instance, vkGetInstanceProcAddr));

	Instance.destroy();
}

void Renderer::Draw()
{
	try
	{
		if (Device.waitForFences(InFlightFences[CurrentFrame], true, UINT64_MAX) == vk::Result::eErrorDeviceLost)
			throw std::runtime_error("Critical error, device unplugged.");

		auto Result = Device.acquireNextImageKHR(SwapChain, UINT64_MAX, ImageAvailableSem[CurrentFrame]);
		if (Result.result == vk::Result::eErrorOutOfDateKHR or Result.result == vk::Result::eSuboptimalKHR or PauseDraw)
		{
			RebuildSwapChain();
			return;
		}

		if (ImageFences[Result.value] != vk::Fence{})
			if (Device.waitForFences(ImageFences[Result.value], true, UINT64_MAX) == vk::Result::eErrorDeviceLost)
				throw std::runtime_error("Critical error, device unplugged.");
		ImageFences[Result.value] = InFlightFences[CurrentFrame];

		if (TargetsDelete.size())
		{
			DeleteTargets();
		}
		if (LightsDelete.size())
		{
			DeleteLights();
		}
		if (TexturesDelete.size())
		{
			DeleteTextures();
		}
		if (ModelsDelete.size())
		{
			DeleteModels();
		}
		if (ModelsCreate.size())
		{
			CreateModels();
		}
		if (TexturesCreate.size())
		{
			CreateTextures();
		}
		if (LightsCreate.size())
		{
			CreateLights();
		}
		if (TargetsCreate.size())
		{
			CreateTargets();
		}

		UpdateLights();
		UpdateUniformBuffer(Result.value);

		vk::PipelineStageFlags WaitStages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
		vk::SubmitInfo SubmitInfo{
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &ImageAvailableSem[CurrentFrame],
			.pWaitDstStageMask = WaitStages,
			.commandBufferCount = 1,
			.pCommandBuffers = &CommandBuffers[Result.value],
			.signalSemaphoreCount = 1,
			.pSignalSemaphores = &RenderFinishedSem[CurrentFrame],
		};

		Device.resetFences(InFlightFences[CurrentFrame]);
		GraphicsQueue.submit(SubmitInfo, InFlightFences[CurrentFrame]);

		vk::PresentInfoKHR PresentInfo{
			.waitSemaphoreCount = 1,
			.pWaitSemaphores = &RenderFinishedSem[CurrentFrame],
			.swapchainCount = 1,
			.pSwapchains = &SwapChain,
			.pImageIndices = &Result.value,
		};

		auto ResultPre = PresentQueue.presentKHR(PresentInfo);
		if (ResultPre == vk::Result::eErrorOutOfDateKHR or ResultPre == vk::Result::eSuboptimalKHR or PauseDraw)
			RebuildSwapChain();

		CurrentFrame = (CurrentFrame + 1) % FramesInFlight;
	}
	catch (vk::IncompatibleDisplayKHRError Error)
	{
		RebuildSwapChain();
	}
	catch (vk::OutOfDateKHRError Error)
	{
		RebuildSwapChain();
	}
}

void Renderer::CreateModel(uint16_t ID, const Model &Model)
{
	if (Model.Vertices.empty() or Model.Indices.empty())
		throw std::runtime_error("Attempted to load invalid render model");

	ModelsCreate.push_back({ID, Model});
}

void Renderer::CreateTexture(uint16_t ID, const Texture &Texture)
{
	TexturesCreate.push_back({ID, Texture});
}

void Renderer::CreateLight(uint16_t ID, const Light &Light)
{
	LightsCreate.push_back({ID, Light});
}

void Renderer::CreateTarget(uint16_t ID, Target Target)
{
	TargetsCreate.push_back({ID, Target});
}

void Renderer::DeleteModel(uint16_t ID)
{
	if (not Models.contains(ID))
		throw std::runtime_error("Attempted to remove invalid render model");

	ModelsDelete.push_back(ID);
}

void Renderer::DeleteTexture(uint16_t ID)
{
	if (not Textures.contains(ID))
		throw std::runtime_error("Attempted to remove invalid render texture");

	TexturesDelete.push_back(ID);
}

void Renderer::DeleteLight(uint16_t ID)
{
	if (not Lights.contains(ID))
		throw std::runtime_error("Attempted to remove invalid render light");

	LightsDelete.push_back(ID);
}

void Renderer::DeleteTarget(uint16_t ID)
{
	if (not Targets.contains(ID))
		throw std::runtime_error("Attempted to remove invalid render target");

	TargetsDelete.push_back(ID);
}

Light &Renderer::UpdateLight(uint16_t ID)
{
	return Lights[ID];
}

void Renderer::UpdateTarget(uint16_t ID, const glm::mat4 &Transform)
{
	if (Targets.contains(ID))
		Targets[ID].Transform = Transform;
}

void Renderer::FlushObjects()
{
	DeleteTargets();
	DeleteTextures();
	DeleteLights();
	DeleteModels();
	CreateModels();
	CreateTextures();
	CreateLights();
	CreateTargets();
}

void Renderer::BindCamera(ICamera *Camera)
{
	this->Camera = Camera;
}