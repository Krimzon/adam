#pragma once

#include <cstdint>
#include <vector>
#include "glm/glm.hpp"
#include "rapidjson/document.h"
#include "vertex.hpp"
#include "vulkan/vulkan.hpp"

struct Model
{
	using Convertible = std::true_type;
	using ParamT = std::tuple<std::vector<Vertex>, std::vector<uint32_t>>;

	std::vector<Vertex> Vertices;
	std::vector<uint32_t> Indices;

	size_t VertexOffset;
	size_t IndexOffset;

	Model();
	Model(const ParamT &Param);
	Model(const std::vector<Vertex> &Vertices, const std::vector<uint32_t> &Indices);

	ParamT Convert() const;
};

struct Texture
{
	using Convertible = std::true_type;
	using ParamT = std::tuple<int32_t, int32_t, std::vector<uint8_t>>;

	std::vector<uint8_t> Pixels;
	int32_t Width, Height;

	vk::Image TextureImage{};
	vk::ImageView TextureImageView{};
	vk::DeviceMemory TextureMemory{};

	Texture();
	Texture(const ParamT &Param);
	Texture(int32_t Width, int32_t Height);

	ParamT Convert() const;
};

struct Light
{
	glm::vec4 Position;
	glm::vec4 Color;

	Light();
	Light(glm::vec3 Position, glm::vec4 Color);
};

struct Target
{
	using Convertible = std::true_type;
	using ParamT = std::tuple<uint16_t, uint16_t, std::vector<uint16_t>>;

	uint16_t ModelID;
	uint16_t TextureID;
	std::vector<uint16_t> LightID;

	glm::mat4 Transform;

	std::vector<vk::DescriptorSet> UniformDescriptorSet{};
	std::vector<vk::DeviceMemory> UniformDeviceMemory{};
	std::vector<vk::DeviceMemory> UniformDeviceMemoryLights{};
	std::vector<vk::Buffer> UniformBuffer{};
	std::vector<vk::Buffer> UniformBufferLights{};

	Target();
	Target(const ParamT &Params);
	Target(uint16_t ModelID, uint16_t TextureID);
	Target(uint16_t ModelID, uint16_t TextureID, std::vector<uint16_t> LightID);
	Target(uint16_t ModelID, uint16_t TextureID, std::vector<uint16_t> LightID, glm::mat4 Transform);

	ParamT Convert() const;
};