#pragma once

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS

#include <array>
#include "../net/game_packet.hpp"
#include "glm/glm.hpp"
#include "vulkan/vulkan.hpp"

struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Color;
	glm::vec3 Normal;
	glm::vec2 TexCoord;

	static vk::VertexInputBindingDescription GetBindingDescription()
	{
		return vk::VertexInputBindingDescription{
			.binding = 0,
			.stride = sizeof(Vertex),
			.inputRate = vk::VertexInputRate::eVertex,
		};
	};

	static std::array<vk::VertexInputAttributeDescription, 4> GetAttributeDescriptions()
	{
		return std::array<vk::VertexInputAttributeDescription, 4>{
			vk::VertexInputAttributeDescription{
				.location = 0,
				.binding = 0,
				.format = vk::Format::eR32G32B32Sfloat,
				.offset = offsetof(Vertex, Position),
			},
			vk::VertexInputAttributeDescription{
				.location = 1,
				.binding = 0,
				.format = vk::Format::eR32G32B32Sfloat,
				.offset = offsetof(Vertex, Color),
			},
			vk::VertexInputAttributeDescription{
				.location = 2,
				.binding = 0,
				.format = vk::Format::eR32G32B32Sfloat,
				.offset = offsetof(Vertex, Normal),
			},
			vk::VertexInputAttributeDescription{
				.location = 3,
				.binding = 0,
				.format = vk::Format::eR32G32Sfloat,
				.offset = offsetof(Vertex, TexCoord),
			},
		};
	};

	Vertex();
	Vertex(const glm::vec3 &Position, const glm::vec3 &Color);
	Vertex(const glm::vec3 &Position, const glm::vec3 &Color, const glm::vec3 &Normal, const glm::vec2 &TexCoord);
};