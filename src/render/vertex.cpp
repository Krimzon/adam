#include "glm/gtc/type_ptr.hpp"
#include "vertex.hpp"

Vertex::Vertex()
{}

Vertex::Vertex(const glm::vec3 &Position, const glm::vec3 &Color) : Position(Position), Color(Color)
{}

Vertex::Vertex(const glm::vec3 &Position, const glm::vec3 &Color, const glm::vec3 &Normal, const glm::vec2 &TexCoord) : Position(Position), Color(Color), Normal(Normal), TexCoord(TexCoord)
{}